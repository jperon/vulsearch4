using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace VulSearch4
{
	/// <summary>
	/// Summary description for AddRefForm.
	/// </summary>
	public class AddRefForm : System.Windows.Forms.Form
	{
		private System.Windows.Forms.ComboBox cmbBook;
		private System.Windows.Forms.ComboBox cmbChap;
		private System.Windows.Forms.TextBox txtVerse;
		private System.Windows.Forms.Button btnOK;
		private System.Windows.Forms.Button btnCancel;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox txtDesc;

		private string reference="x";

		public string Reference
		{
			get
			{
				return reference;
			}
		}

		public string Desc;

		public AddRefForm()
		{
			try {
			InitializeComponent();
			txtDesc.Visible=false;

			for (int i=0 ; i<73; i++)
			{
				cmbBook.Items.Add(Bible.ShortName[i]);
			}
			cmbBook.SelectedIndex=0;
		} catch {}
		}

		public AddRefForm(int book, int ch, string v, string desc) : this()
		{
			try {
			cmbBook.SelectedIndex=book-1;
			cmbChap.SelectedIndex=(ch>=cmbChap.Items.Count+1 ? 0 : ch-1);
			txtVerse.Text=v;
			txtDesc.Visible=true;
			txtDesc.Text=desc;
		} catch {}
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			try {
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		} catch {}
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(AddRefForm));
			this.cmbBook = new System.Windows.Forms.ComboBox();
			this.cmbChap = new System.Windows.Forms.ComboBox();
			this.txtVerse = new System.Windows.Forms.TextBox();
			this.btnOK = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.txtDesc = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// cmbBook
			// 
			this.cmbBook.AccessibleDescription = resources.GetString("cmbBook.AccessibleDescription");
			this.cmbBook.AccessibleName = resources.GetString("cmbBook.AccessibleName");
			this.cmbBook.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("cmbBook.Anchor")));
			this.cmbBook.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cmbBook.BackgroundImage")));
			this.cmbBook.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("cmbBook.Dock")));
			this.cmbBook.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cmbBook.Enabled = ((bool)(resources.GetObject("cmbBook.Enabled")));
			this.cmbBook.Font = ((System.Drawing.Font)(resources.GetObject("cmbBook.Font")));
			this.cmbBook.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("cmbBook.ImeMode")));
			this.cmbBook.IntegralHeight = ((bool)(resources.GetObject("cmbBook.IntegralHeight")));
			this.cmbBook.ItemHeight = ((int)(resources.GetObject("cmbBook.ItemHeight")));
			this.cmbBook.Location = ((System.Drawing.Point)(resources.GetObject("cmbBook.Location")));
			this.cmbBook.MaxDropDownItems = ((int)(resources.GetObject("cmbBook.MaxDropDownItems")));
			this.cmbBook.MaxLength = ((int)(resources.GetObject("cmbBook.MaxLength")));
			this.cmbBook.Name = "cmbBook";
			this.cmbBook.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("cmbBook.RightToLeft")));
			this.cmbBook.Size = ((System.Drawing.Size)(resources.GetObject("cmbBook.Size")));
			this.cmbBook.TabIndex = ((int)(resources.GetObject("cmbBook.TabIndex")));
			this.cmbBook.Text = resources.GetString("cmbBook.Text");
			this.cmbBook.Visible = ((bool)(resources.GetObject("cmbBook.Visible")));
			this.cmbBook.SelectedIndexChanged += new System.EventHandler(this.cmbBook_SelectedIndexChanged);
			// 
			// cmbChap
			// 
			this.cmbChap.AccessibleDescription = resources.GetString("cmbChap.AccessibleDescription");
			this.cmbChap.AccessibleName = resources.GetString("cmbChap.AccessibleName");
			this.cmbChap.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("cmbChap.Anchor")));
			this.cmbChap.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cmbChap.BackgroundImage")));
			this.cmbChap.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("cmbChap.Dock")));
			this.cmbChap.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cmbChap.Enabled = ((bool)(resources.GetObject("cmbChap.Enabled")));
			this.cmbChap.Font = ((System.Drawing.Font)(resources.GetObject("cmbChap.Font")));
			this.cmbChap.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("cmbChap.ImeMode")));
			this.cmbChap.IntegralHeight = ((bool)(resources.GetObject("cmbChap.IntegralHeight")));
			this.cmbChap.ItemHeight = ((int)(resources.GetObject("cmbChap.ItemHeight")));
			this.cmbChap.Location = ((System.Drawing.Point)(resources.GetObject("cmbChap.Location")));
			this.cmbChap.MaxDropDownItems = ((int)(resources.GetObject("cmbChap.MaxDropDownItems")));
			this.cmbChap.MaxLength = ((int)(resources.GetObject("cmbChap.MaxLength")));
			this.cmbChap.Name = "cmbChap";
			this.cmbChap.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("cmbChap.RightToLeft")));
			this.cmbChap.Size = ((System.Drawing.Size)(resources.GetObject("cmbChap.Size")));
			this.cmbChap.TabIndex = ((int)(resources.GetObject("cmbChap.TabIndex")));
			this.cmbChap.Text = resources.GetString("cmbChap.Text");
			this.cmbChap.Visible = ((bool)(resources.GetObject("cmbChap.Visible")));
			// 
			// txtVerse
			// 
			this.txtVerse.AccessibleDescription = resources.GetString("txtVerse.AccessibleDescription");
			this.txtVerse.AccessibleName = resources.GetString("txtVerse.AccessibleName");
			this.txtVerse.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("txtVerse.Anchor")));
			this.txtVerse.AutoSize = ((bool)(resources.GetObject("txtVerse.AutoSize")));
			this.txtVerse.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtVerse.BackgroundImage")));
			this.txtVerse.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("txtVerse.Dock")));
			this.txtVerse.Enabled = ((bool)(resources.GetObject("txtVerse.Enabled")));
			this.txtVerse.Font = ((System.Drawing.Font)(resources.GetObject("txtVerse.Font")));
			this.txtVerse.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("txtVerse.ImeMode")));
			this.txtVerse.Location = ((System.Drawing.Point)(resources.GetObject("txtVerse.Location")));
			this.txtVerse.MaxLength = ((int)(resources.GetObject("txtVerse.MaxLength")));
			this.txtVerse.Multiline = ((bool)(resources.GetObject("txtVerse.Multiline")));
			this.txtVerse.Name = "txtVerse";
			this.txtVerse.PasswordChar = ((char)(resources.GetObject("txtVerse.PasswordChar")));
			this.txtVerse.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("txtVerse.RightToLeft")));
			this.txtVerse.ScrollBars = ((System.Windows.Forms.ScrollBars)(resources.GetObject("txtVerse.ScrollBars")));
			this.txtVerse.Size = ((System.Drawing.Size)(resources.GetObject("txtVerse.Size")));
			this.txtVerse.TabIndex = ((int)(resources.GetObject("txtVerse.TabIndex")));
			this.txtVerse.Text = resources.GetString("txtVerse.Text");
			this.txtVerse.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("txtVerse.TextAlign")));
			this.txtVerse.Visible = ((bool)(resources.GetObject("txtVerse.Visible")));
			this.txtVerse.WordWrap = ((bool)(resources.GetObject("txtVerse.WordWrap")));
			// 
			// btnOK
			// 
			this.btnOK.AccessibleDescription = resources.GetString("btnOK.AccessibleDescription");
			this.btnOK.AccessibleName = resources.GetString("btnOK.AccessibleName");
			this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnOK.Anchor")));
			this.btnOK.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnOK.BackgroundImage")));
			this.btnOK.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnOK.Dock")));
			this.btnOK.Enabled = ((bool)(resources.GetObject("btnOK.Enabled")));
			this.btnOK.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnOK.FlatStyle")));
			this.btnOK.Font = ((System.Drawing.Font)(resources.GetObject("btnOK.Font")));
			this.btnOK.Image = ((System.Drawing.Image)(resources.GetObject("btnOK.Image")));
			this.btnOK.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnOK.ImageAlign")));
			this.btnOK.ImageIndex = ((int)(resources.GetObject("btnOK.ImageIndex")));
			this.btnOK.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnOK.ImeMode")));
			this.btnOK.Location = ((System.Drawing.Point)(resources.GetObject("btnOK.Location")));
			this.btnOK.Name = "btnOK";
			this.btnOK.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnOK.RightToLeft")));
			this.btnOK.Size = ((System.Drawing.Size)(resources.GetObject("btnOK.Size")));
			this.btnOK.TabIndex = ((int)(resources.GetObject("btnOK.TabIndex")));
			this.btnOK.Text = resources.GetString("btnOK.Text");
			this.btnOK.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnOK.TextAlign")));
			this.btnOK.Visible = ((bool)(resources.GetObject("btnOK.Visible")));
			this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.AccessibleDescription = resources.GetString("btnCancel.AccessibleDescription");
			this.btnCancel.AccessibleName = resources.GetString("btnCancel.AccessibleName");
			this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnCancel.Anchor")));
			this.btnCancel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCancel.BackgroundImage")));
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnCancel.Dock")));
			this.btnCancel.Enabled = ((bool)(resources.GetObject("btnCancel.Enabled")));
			this.btnCancel.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnCancel.FlatStyle")));
			this.btnCancel.Font = ((System.Drawing.Font)(resources.GetObject("btnCancel.Font")));
			this.btnCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnCancel.Image")));
			this.btnCancel.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnCancel.ImageAlign")));
			this.btnCancel.ImageIndex = ((int)(resources.GetObject("btnCancel.ImageIndex")));
			this.btnCancel.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnCancel.ImeMode")));
			this.btnCancel.Location = ((System.Drawing.Point)(resources.GetObject("btnCancel.Location")));
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnCancel.RightToLeft")));
			this.btnCancel.Size = ((System.Drawing.Size)(resources.GetObject("btnCancel.Size")));
			this.btnCancel.TabIndex = ((int)(resources.GetObject("btnCancel.TabIndex")));
			this.btnCancel.Text = resources.GetString("btnCancel.Text");
			this.btnCancel.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnCancel.TextAlign")));
			this.btnCancel.Visible = ((bool)(resources.GetObject("btnCancel.Visible")));
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// label1
			// 
			this.label1.AccessibleDescription = resources.GetString("label1.AccessibleDescription");
			this.label1.AccessibleName = resources.GetString("label1.AccessibleName");
			this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("label1.Anchor")));
			this.label1.AutoSize = ((bool)(resources.GetObject("label1.AutoSize")));
			this.label1.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("label1.Dock")));
			this.label1.Enabled = ((bool)(resources.GetObject("label1.Enabled")));
			this.label1.Font = ((System.Drawing.Font)(resources.GetObject("label1.Font")));
			this.label1.Image = ((System.Drawing.Image)(resources.GetObject("label1.Image")));
			this.label1.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label1.ImageAlign")));
			this.label1.ImageIndex = ((int)(resources.GetObject("label1.ImageIndex")));
			this.label1.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("label1.ImeMode")));
			this.label1.Location = ((System.Drawing.Point)(resources.GetObject("label1.Location")));
			this.label1.Name = "label1";
			this.label1.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("label1.RightToLeft")));
			this.label1.Size = ((System.Drawing.Size)(resources.GetObject("label1.Size")));
			this.label1.TabIndex = ((int)(resources.GetObject("label1.TabIndex")));
			this.label1.Text = resources.GetString("label1.Text");
			this.label1.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label1.TextAlign")));
			this.label1.Visible = ((bool)(resources.GetObject("label1.Visible")));
			// 
			// label2
			// 
			this.label2.AccessibleDescription = resources.GetString("label2.AccessibleDescription");
			this.label2.AccessibleName = resources.GetString("label2.AccessibleName");
			this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("label2.Anchor")));
			this.label2.AutoSize = ((bool)(resources.GetObject("label2.AutoSize")));
			this.label2.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("label2.Dock")));
			this.label2.Enabled = ((bool)(resources.GetObject("label2.Enabled")));
			this.label2.Font = ((System.Drawing.Font)(resources.GetObject("label2.Font")));
			this.label2.Image = ((System.Drawing.Image)(resources.GetObject("label2.Image")));
			this.label2.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label2.ImageAlign")));
			this.label2.ImageIndex = ((int)(resources.GetObject("label2.ImageIndex")));
			this.label2.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("label2.ImeMode")));
			this.label2.Location = ((System.Drawing.Point)(resources.GetObject("label2.Location")));
			this.label2.Name = "label2";
			this.label2.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("label2.RightToLeft")));
			this.label2.Size = ((System.Drawing.Size)(resources.GetObject("label2.Size")));
			this.label2.TabIndex = ((int)(resources.GetObject("label2.TabIndex")));
			this.label2.Text = resources.GetString("label2.Text");
			this.label2.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label2.TextAlign")));
			this.label2.Visible = ((bool)(resources.GetObject("label2.Visible")));
			// 
			// label3
			// 
			this.label3.AccessibleDescription = resources.GetString("label3.AccessibleDescription");
			this.label3.AccessibleName = resources.GetString("label3.AccessibleName");
			this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("label3.Anchor")));
			this.label3.AutoSize = ((bool)(resources.GetObject("label3.AutoSize")));
			this.label3.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("label3.Dock")));
			this.label3.Enabled = ((bool)(resources.GetObject("label3.Enabled")));
			this.label3.Font = ((System.Drawing.Font)(resources.GetObject("label3.Font")));
			this.label3.Image = ((System.Drawing.Image)(resources.GetObject("label3.Image")));
			this.label3.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label3.ImageAlign")));
			this.label3.ImageIndex = ((int)(resources.GetObject("label3.ImageIndex")));
			this.label3.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("label3.ImeMode")));
			this.label3.Location = ((System.Drawing.Point)(resources.GetObject("label3.Location")));
			this.label3.Name = "label3";
			this.label3.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("label3.RightToLeft")));
			this.label3.Size = ((System.Drawing.Size)(resources.GetObject("label3.Size")));
			this.label3.TabIndex = ((int)(resources.GetObject("label3.TabIndex")));
			this.label3.Text = resources.GetString("label3.Text");
			this.label3.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label3.TextAlign")));
			this.label3.Visible = ((bool)(resources.GetObject("label3.Visible")));
			// 
			// txtDesc
			// 
			this.txtDesc.AccessibleDescription = resources.GetString("txtDesc.AccessibleDescription");
			this.txtDesc.AccessibleName = resources.GetString("txtDesc.AccessibleName");
			this.txtDesc.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("txtDesc.Anchor")));
			this.txtDesc.AutoSize = ((bool)(resources.GetObject("txtDesc.AutoSize")));
			this.txtDesc.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtDesc.BackgroundImage")));
			this.txtDesc.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("txtDesc.Dock")));
			this.txtDesc.Enabled = ((bool)(resources.GetObject("txtDesc.Enabled")));
			this.txtDesc.Font = ((System.Drawing.Font)(resources.GetObject("txtDesc.Font")));
			this.txtDesc.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("txtDesc.ImeMode")));
			this.txtDesc.Location = ((System.Drawing.Point)(resources.GetObject("txtDesc.Location")));
			this.txtDesc.MaxLength = ((int)(resources.GetObject("txtDesc.MaxLength")));
			this.txtDesc.Multiline = ((bool)(resources.GetObject("txtDesc.Multiline")));
			this.txtDesc.Name = "txtDesc";
			this.txtDesc.PasswordChar = ((char)(resources.GetObject("txtDesc.PasswordChar")));
			this.txtDesc.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("txtDesc.RightToLeft")));
			this.txtDesc.ScrollBars = ((System.Windows.Forms.ScrollBars)(resources.GetObject("txtDesc.ScrollBars")));
			this.txtDesc.Size = ((System.Drawing.Size)(resources.GetObject("txtDesc.Size")));
			this.txtDesc.TabIndex = ((int)(resources.GetObject("txtDesc.TabIndex")));
			this.txtDesc.Text = resources.GetString("txtDesc.Text");
			this.txtDesc.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("txtDesc.TextAlign")));
			this.txtDesc.Visible = ((bool)(resources.GetObject("txtDesc.Visible")));
			this.txtDesc.WordWrap = ((bool)(resources.GetObject("txtDesc.WordWrap")));
			// 
			// AddRefForm
			// 
			this.AcceptButton = this.btnOK;
			this.AccessibleDescription = resources.GetString("$this.AccessibleDescription");
			this.AccessibleName = resources.GetString("$this.AccessibleName");
			this.AutoScaleBaseSize = ((System.Drawing.Size)(resources.GetObject("$this.AutoScaleBaseSize")));
			this.AutoScroll = ((bool)(resources.GetObject("$this.AutoScroll")));
			this.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("$this.AutoScrollMargin")));
			this.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("$this.AutoScrollMinSize")));
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.CancelButton = this.btnCancel;
			this.ClientSize = ((System.Drawing.Size)(resources.GetObject("$this.ClientSize")));
			this.Controls.Add(this.txtDesc);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.btnOK);
			this.Controls.Add(this.txtVerse);
			this.Controls.Add(this.cmbChap);
			this.Controls.Add(this.cmbBook);
			this.Enabled = ((bool)(resources.GetObject("$this.Enabled")));
			this.Font = ((System.Drawing.Font)(resources.GetObject("$this.Font")));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("$this.ImeMode")));
			this.Location = ((System.Drawing.Point)(resources.GetObject("$this.Location")));
			this.MaximumSize = ((System.Drawing.Size)(resources.GetObject("$this.MaximumSize")));
			this.MinimumSize = ((System.Drawing.Size)(resources.GetObject("$this.MinimumSize")));
			this.Name = "AddRefForm";
			this.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("$this.RightToLeft")));
			this.ShowInTaskbar = false;
			this.StartPosition = ((System.Windows.Forms.FormStartPosition)(resources.GetObject("$this.StartPosition")));
			this.Text = resources.GetString("$this.Text");
			this.ResumeLayout(false);

		}
		#endregion


		private void btnOK_Click(object sender, System.EventArgs e)
		{
			try {
			reference=Bible.Abbrev[cmbBook.SelectedIndex]  +" " + 
				(string) cmbChap.SelectedItem + ":" + 
				(txtVerse.Text.Length==0 ? "1" : txtVerse.Text);
			Desc=txtDesc.Text;
			this.Hide();
		} catch {}
		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			try {
			reference="x";
			this.Hide();
		} catch {}
		}

		private void cmbBook_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			try {
			cmbChap.Items.Clear();
			for (int i=0; i<Bible.NumberOfChapters[cmbBook.SelectedIndex]; i++)
			{
				cmbChap.Items.Add((i+1).ToString());
			}
			cmbChap.SelectedIndex=0;
		} catch {}
		}
	}
}
