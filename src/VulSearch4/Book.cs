using System;
using System.IO;
using VulSearch4;

namespace VulSearch4
{
	/// <summary>
	/// Reading a large number of books is a slow process, so books
	/// are only read when they need to be displayed. The private
	/// hasBeenRead flag is used by the class to keep track of
	/// whether a book's text has been read from disk.
	/// </summary>
	public class Book
	{
		private bool hasBeenRead = false;
		private Chapter[] chapters;
		private byte index;
		private Bible myBible;
		public Bible MyBible
		{
			get
			{
				return myBible;
			}
		}


		public Chapter[] Chapters
		{
			get
			{
				if (hasBeenRead==false)
				{
					hasBeenRead=true;
					ReadBook();
				}
				return chapters;
			}
		}

		public byte Index
		{
			get
			{
				return index;
			}
		}


		/// <summary>
		/// This reads the text of the book into chapters
		/// </summary>
		private void ReadBook()
		{
			try {
			byte chapter=1;
			string x, ChapterText="";
			try
			{
				StreamReader reader;
				FileStream file = new FileStream(Startup.DataPath + @"\text\"
					+ Bible.Abbrev[index] + "." + myBible.Extension,
					FileMode.Open,FileAccess.Read);
				reader = new StreamReader(file, 
					System.Text.Encoding.GetEncoding(1252));

				x=reader.ReadLine();
				while (x != null)
				{
					if (x.StartsWith(chapter.ToString()))
					{
						// still in the same chapter - add to ChapterText
						ChapterText += x.Substring(x.IndexOf(":")+1)
							+"\n";
					}
					else
					{
						// finish up the chapter
						ChapterText=ChapterText.Substring(0,
							ChapterText.Length-1); // kill last \n
						string c=ChapterText.Substring(
							ChapterText.LastIndexOf("\n")+1);
						c=c.Substring(0, c.IndexOf(" "));
						byte b=System.Convert.ToByte(c);
						Chapters[chapter-1].Read(ChapterText,b);
						ChapterText=x.Substring(x.IndexOf(":")+1) + "\n";
						chapter++;
					}

					// kludge to make sure we read the last verse of the chapter
					if (x=="null")
					{
						x=null;
					}
					else
					{
						x=reader.ReadLine();
						if (x==null)
						{
							x="null";
						}
					}
				}
				reader.Close();
			}
			catch (Exception e)
			{
				System.Windows.Forms.MessageBox.Show(Startup.rm.GetString(
					"readerror")
					 + Bible.Abbrev[index]
					+ "." + myBible.Extension + ":\n" + e.Message) ;
			}
		} catch {}
		}

		public Book(byte Index, byte NumberOfChapters, Bible ParentBible)
		{
			try {
			index=Index;
			this.chapters=new Chapter[NumberOfChapters];
			for (int i=0; i<NumberOfChapters;i++)
			{
				this.chapters[i]=new Chapter(this,i+1) ;
			}
			myBible=ParentBible;
		} catch {}
		}
	}
}
