using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace VulSearch4
{
	/// <summary>
	/// Summary description for SearchForm.
	/// </summary>
	public class SearchForm : System.Windows.Forms.Form
	{
		public System.Windows.Forms.Button btnSearch;
		private System.Windows.Forms.Label label1;
		public System.Windows.Forms.ComboBox cmbSearch;
		private System.Windows.Forms.Label label2;
		public System.Windows.Forms.ComboBox cmbBible;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;



		public SearchForm()
		{
			try {
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		} catch {}
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			try {
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		} catch {}
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SearchForm));
            this.btnSearch = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbSearch = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbBible = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // btnSearch
            // 
            resources.ApplyResources(this.btnSearch, "btnSearch");
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // cmbSearch
            // 
            resources.ApplyResources(this.cmbSearch, "cmbSearch");
            this.cmbSearch.Name = "cmbSearch";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // cmbBible
            // 
            resources.ApplyResources(this.cmbBible, "cmbBible");
            this.cmbBible.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBible.Name = "cmbBible";
            // 
            // SearchForm
            // 
            this.AcceptButton = this.btnSearch;
            resources.ApplyResources(this, "$this");
            this.Controls.Add(this.cmbBible);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cmbSearch);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnSearch);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "SearchForm";
            this.ShowInTaskbar = false;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SearchForm_FormClosing);
            this.VisibleChanged += new System.EventHandler(this.SearchForm_VisibleChanged);
            this.ResumeLayout(false);
        }

		#endregion

		public void btnSearch_Click(object sender, System.EventArgs e)
		{
			try {
			int bibleToSearch = cmbBible.SelectedIndex;
#if DEBUG
            string idxpath = MainForm.AppPath()+"\\src\\swish\\" + (Startup.Bibles[bibleToSearch] as Bible).Extension+".idx";
#else
            string idxpath = MainForm.AppPath()+"\\swish\\" + (Startup.Bibles[bibleToSearch] as Bible).Extension+".idx";
#endif
            if (System.IO.File.Exists(idxpath))
			{

				btnSearch.Enabled=false;
				Startup.MainForm.Cursor=Cursors.WaitCursor;
				this.Cursor=Cursors.WaitCursor;

				string p=cmbSearch.Text;
				if ( (Startup.Bibles[bibleToSearch] as Bible).IsLatin)
				{
					// Latin Bible - change j to i etc.
					p=p.Replace("j","i");
					p=p.Replace("J","I");
					p=p.Replace("v","u");
					p=p.Replace("V","U");
					p=p.Replace("�","ae");
					p=p.Replace("�","oe");
					p=p.Replace("�","e");
					p=p.Replace("�","Ae");
					p=p.Replace("�","Oe");
				}
				Startup.MainForm.StartSearch(bibleToSearch,p);

				btnSearch.Enabled=true ;
				Startup.MainForm.Cursor=Cursors.Default ;
				this.Cursor=Cursors.Default ;
				if (Startup.OptionsForm.chkStoreSearch.Checked)
				{
					cmbSearch.Items.Insert(0,cmbSearch.Text);
					if (cmbSearch.Items.Count > 10)
					{
						cmbSearch.Items.RemoveAt(10);
					}
				}
				cmbSearch.SelectAll();
			}
			else
			{
				MessageBox.Show(Startup.rm.GetString("notindex")
					+ (Startup.Bibles[bibleToSearch] as Bible).Extension 
					+ Startup.rm.GetString("notindex2"),"VulSearch 4",MessageBoxButtons.OK);
			}
		} catch {}
		}

        private void SearchForm_VisibleChanged(object sender, EventArgs e)
        {
            Startup.MainForm.toolSearch.Checked = this.Visible;
        }

        private void SearchForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                this.Hide();
                e.Cancel = true;
            }
        }

	}
}
