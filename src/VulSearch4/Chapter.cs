﻿using System;
using System.Drawing;
using System.Text.RegularExpressions;

namespace VulSearch4
{
	/// <summary>
	/// Summary description for Chapter.
	/// </summary>
	public class Chapter
	{
		private string text;
		private byte numberOfVerses;
		private bool HasBeenRead=false;
		private Book myBook;
		private int number;

		public int Number
		{
			get
			{
				return number;
			}
		}

		public byte NumberOfVerses
		{
			get
			{
				return numberOfVerses;
			}
		}
		public string Text
		{
			// when Book returns its Chapters array, it automatically
			// reads in the book first, so no danger of text's
			// not being initialized
			get
			{
				return text;
			}
		}

		public void CreateRTF(ref XRichTextBox rtf)
		{
			try {
			if (rtf.Text.Length>0) rtf.Clear();
			rtf.SelectionIndent=0;
			rtf.SelectionRightIndent=0;

			string text=this.text;  //local variable hides class
									//variable
			if (myBook.MyBible.IsLatin)
			{
				if (Startup.OptionsForm.chkIJ.Checked==false)
				{
					text=text.Replace("j","i");
					text=text.Replace("J","I");
				}
				if (Startup.OptionsForm.chkUV.Checked==false)
				{
					text=text.Replace("v","u");
					text=text.Replace("U","V");
				}
				if (Startup.OptionsForm.chkAE.Checked==false)
				{
					text=text.Replace("æ","ae");
					text=text.Replace("Æ","Ae");
					text=text.Replace("œ","oe");
					text=text.Replace("Œ","Oe");
					text=text.Replace("ë","e");
				}
			}

			// step 1: split into verses and do the verse number
			foreach (string t in text.Split(new char[] {'\n'}))
			{
				string s=t;
				s=s.Trim();

				//accentuate
				if (myBook.MyBible.IsLatin && Startup.OptionsForm.chkAccent.Checked)
				{
					s+=" ";
					int g=s.IndexOf(" ");
					while (g<s.Length-1)
					{
						int gg=s.IndexOf(" ",g+1);
						string ss=s.Substring(g+1,gg-g-1),brk="";
						if(ss.StartsWith("[")) {
							ss=ss.Substring(1);
							brk="[";
						}
						ss=brk+Accentuate.Accentuate.DoAccentuate(ss);
						if (myBook.Index==0 && number==1)
						{
							if (ss.StartsWith("María")) ss="Mária"+ss.Substring(5);
						}
						//if (ss.IndexOf("??")>=0)
						//{
							ss=AccentExceptions(ss, Bible.Abbrev[myBook.Index], number, byte.Parse(s.Substring(0,s.IndexOf(" "))));
						//}
						s=s.Substring(0,g+1) + ss + s.Substring(gg);
						g=s.IndexOf(" ",g+1);
					}
					s=s.Trim();
				}
				if (!Startup.OptionsForm.chkAE.Checked)
				{
					s=s.Replace("ǽ","áe");
					s=s.Replace("Ǽ","Áe");
				}

				byte verse=byte.Parse(s.Substring(0,s.IndexOf(" ")));
				s=s.Substring(s.IndexOf(" ")+1);
				// Michael's markup lets us do 1:1 [...
				// so we'd better deal with it
				if (s.Substring(0,1)=="[")
				{
					if (Startup.OptionsForm.chkPoetry.Checked & 
						Startup.OptionsForm.radParagraphs.Checked)
					{
						if (verse != 1 && rtf.Text.EndsWith("\n\n")==false)
						{
							rtf.SelectedText="\n\n";
						}
						rtf.SelectionIndent=25;
						rtf.SelectionRightIndent=25;
					}
					s=s.Substring(1);
				}

				if (Startup.OptionsForm.radVerseNewLine.Checked)
				{
					if (verse != 1)
					{
						rtf.SelectedText="\n";
					}
					rtf.SelectionFont=Startup.OptionsForm.fontNumbers.Font ;
					rtf.SelectedText=verse.ToString();
				}
				else
				{
					rtf.SelectedText=" ";
					rtf.SelectionFont=Startup.OptionsForm.fontNumbers.Font;
					if (Startup.OptionsForm.chkVerseSuper.Checked)
					{
						rtf.SelectionCharOffset=(int)
							(Startup.OptionsForm.fontMain.Font.Size
							-Startup.OptionsForm.fontNumbers.Font.Size);
					}
					rtf.SelectedText=verse.ToString();
					rtf.SelectionCharOffset=0;
				}
				
				rtf.SelectionFont=Startup.OptionsForm.fontMain.Font;
				rtf.SelectedText="\u00a0"; // non-breaking space

				Regex reg=new Regex(@"[\[\]\\/<>]");
				Match match = reg.Match(s);
				while (match.Success)
				{
					if (match.Index!=0)
					{
						rtf.SelectedText=s.Substring(0,match.Index);
					}
					switch (s.Substring(match.Index,1))
					{
						case @"[":
							if (Startup.OptionsForm.chkPoetry.Checked & 
								Startup.OptionsForm.radParagraphs.Checked)
							{
								if (rtf.Text.EndsWith("\n\n")==false)
								{
									rtf.SelectedText="\n\n";
									rtf.SelectionFont=Startup.OptionsForm.fontMain.Font;
								}
								rtf.SelectionIndent=25;
								rtf.SelectionRightIndent=25;
							}
							break;
						case @"]":
							if (Startup.OptionsForm.chkPoetry.Checked & 
								Startup.OptionsForm.radParagraphs.Checked)
							{
								//if (s.Length != match.Index+1)
								{
									rtf.SelectedText="\n\n";
									rtf.SelectionFont=Startup.OptionsForm.fontMain.Font;
									rtf.SelectionIndent=0;
									rtf.SelectionRightIndent=0;
								}
							}
							break;
						case @"\":
							if (Startup.OptionsForm.radParagraphs.Checked)
							{
								if (rtf.Text.EndsWith("\n\n")==false)
								{
									rtf.SelectedText="\n\n";
									rtf.SelectionFont=Startup.OptionsForm.fontMain.Font;
								}
							}
							break;
						case @"/":
							if (Startup.OptionsForm.chkPoetry.Checked & 
								Startup.OptionsForm.radParagraphs.Checked)
							{
								rtf.SelectedText="\n";
								rtf.SelectionFont=Startup.OptionsForm.fontMain.Font;
							}
							break;
						case @"<":
							rtf.SelectionFont=new Font(
								Startup.OptionsForm.fontMain.Font.Name,
								Startup.OptionsForm.fontMain.Font.Size,
								FontStyle.Italic);
							break;
						case @">":
							rtf.SelectedText=".";
							rtf.SelectionFont=Startup.OptionsForm.fontMain.Font;
							rtf.SelectedText=" ";
							break;
					}
					s=s.Substring(match.Index+1);
					match=reg.Match(s);
				}
				if (s != "")
				{
					rtf.SelectedText=s;
				}
			}
		} catch {}
		}

		/// <summary>
		/// called by the Book class when reading the file
		/// </summary>
		public void Read(string Text, byte NumberOfVerses)
		{
			try {
			if (HasBeenRead==false)
			{
				text=Text;
				text=text.Replace(" :","\u00a0:"); //non-breaking spaces
				text=text.Replace(" ;","\u00a0;");
				text=text.Replace(" !","\u00a0!");
				text=text.Replace(" ?","\u00a0?");
				numberOfVerses=NumberOfVerses;
				HasBeenRead=true;
			}
		} catch {}
		}


		public Chapter(Book ParentBook,int ChapNumber)
		{
			try {
			number=ChapNumber;
			myBook=ParentBook;
		} catch {}
		}

		private string AccentExceptions(string s, string Bk, int chap, byte vs)
		{
			try {
			int i=s.IndexOf("[??]");
			if (i>=0) s=s.Remove(i,4);
			i=s.IndexOf("(??)");
			if (i>=0) s=s.Remove(i,4);
			// first do exceptions that have a definite accentuation, but are just not recognized
			// by Michelet's routine
			s=ContextRpc(s,"thýina","thyína",true);
			s=ContextRpc(s,"adjúvans","ádjuvans",true);
			s=ContextRpc(s,"ággredi","aggrédi",true);
			s=ContextRpc(s,"coháeredes","cohaerédes",true);
			s=ContextRpc(s,"cohǽredes","cohaerédes",true);
			s=ContextRpc(s,"comédens","cómedens",true);
			s=ContextRpc(s,"cónfodit","confódit",true);
			s=ContextRpc(s,"cónside","consíde",true);
			s=ContextRpc(s,"cónsidunt","consídunt",true);
			s=ContextRpc(s,"íidem","iídem",true);
			s=ContextRpc(s,"impúdice","impudíce",true);
			s=ContextRpc(s,"impúdici","impudíci",true);
			s=ContextRpc(s,"jámpridem","jamprídem",true);
			s=ContextRpc(s,"iámpridem","iamprídem",true);
			s=ContextRpc(s,"matrícidis","matricídis",true);
			s=ContextRpc(s,"óbedit","obédit",true);
			s=ContextRpc(s,"parrécidis","parricídis",true);
			s=ContextRpc(s,"prætérgredi","prætergrédi",true);
			s=ContextRpc(s,"praetérgredi","praetergrédi",true);
			s=ContextRpc(s,"prógredi","progrédi",true);
			s=ContextRpc(s,"quingéntenis","quingenténis",true);
			s=ContextRpc(s,"súbridens","subrídens",true);
			s=ContextRpc(s,"súbridet","subrídet",true);
			s=ContextRpc(s,"Ísraël","Israël",true);
			s=ContextRpc(s,"Ísrael","Israel",true);

			// next genuine context sensitive replacements
			s=ContextRpc(s,"comédit","cómedit",(Bk=="Pr" && chap!=31 ) || (Bk=="Ecl") || (Bk=="Is" && chap==29));
			s=ContextRpc(s,"relíqui","réliqui", !( (Bk=="2Par" && chap==12) || (Bk=="Jr" && chap==12) || (Bk=="Rom") || (Bk=="2Tim") || (Bk=="Tit") ));
			s=ContextRpc(s,"excídit","éxcidit", ( (Bk=="Tob") || (Bk=="1Cor")));
			s=ContextRpc(s,"oblivísceris","obliviscéris", !(Bk=="Ps" && chap==43)); 
			s=ContextRpc(s,"occídit","óccidit", !(Bk=="Ecl" && chap==1)); 

			return s;
            }
            catch { return s;  }
		}

		string ContextRpc(string s, string orig, string rpc,bool b)
		{
			try {
			int k=s.IndexOf(orig);
			if (s.Length>=k+orig.Length+1 &&
				'a'<= s[k+orig.Length]
				&& 'z'>=s[k+orig.Length])
					b=false;
			
			if (b && k>=0)
			{
				return s.Substring(0,k) + rpc + s.Substring(k+orig.Length);
			}
			else
			{
				return s;
			}
            }
            catch { return s;  }
		}
	}
}
