﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace VulSearch4
{
    public partial class ResultsForm : Form
    {
        public ResultsForm()
        {
		try {
            InitializeComponent();
	} catch {}
        }

        private void ResultsForm_VisibleChanged(object sender, EventArgs e)
        {
		try {
            Startup.MainForm.toolSearchResults.Checked = this.Visible;
	} catch {}
        }

        private void ResultsForm_FormClosing(object sender, FormClosingEventArgs e)
        {
		try {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                this.Hide();
                e.Cancel = true;
            }
	} catch {}
        }

        private void ResultsBox_DoubleClick(object sender, EventArgs e)
        {
		try {
            Startup.MainForm.Box_DoubleClick(sender, e);
	} catch {}
        }

        private void ResultsBox_MouseUp(object sender, MouseEventArgs e)
        {
		try {
            Startup.MainForm.Box_MouseUp(sender, e);

	} catch {}
        }

        private void ResultsBox_MouseMove(object sender, MouseEventArgs e)
        {
		try {
            Startup.MainForm.Box_MouseMove(sender, e);

	} catch {}
        }

        private void ResultsBox_MouseDown(object sender, MouseEventArgs e)
        {
		try {
            Startup.MainForm.Box_MouseDown(sender, e);

	} catch {}
        }

    }
}
