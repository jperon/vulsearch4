using System;

namespace VulSearch4
{
	/// <summary>
	/// Each version of the Bible used by VulSearch is represented
	/// as a Bible
	/// </summary>
	public class Bible
	{
		private string name,desc;
		/// <remarks>
		/// The name is set once by the constructor, and then
		/// is read-only.
		/// </remarks>
		public string Name
		{
			get { return name; }
		}
		public string Desc
		{
			get { return desc; }
		}

		private string extension;
		/// <remarks>
		/// The extension is set once by the constructor, and then
		/// is read-only.
		/// </remarks>
		public string Extension
		{
			get { return extension; }
		}

		private bool isLatin;
		public bool IsLatin
		{
			get { return isLatin; }
		}


		public Book[] Books = new Book[73];

		public static string[] LongName = new string[73];
		public static string[] ShortName = new string[73];
		public static string[] Abbrev = new string[73];
		public static byte[] NumberOfChapters = new byte[73];

		private void SetupNameConsts()
		{
			try {
			Abbrev[0] = "Gn";
			LongName[0] = "Liber Genesis";
			ShortName[0]="Genesis";
			Abbrev[1] = "Ex";
			LongName[1] = "Liber Exodus";
			ShortName[1]="Exodus";
			Abbrev[2] = "Lv";
			LongName[2] = "Liber Leviticus";
			ShortName[2]="Leviticus";
			Abbrev[3] = "Nm";
			LongName[3] = "Liber Numeri";
			ShortName[3]="Numeri";
			Abbrev[4] = "Dt";
			LongName[4] = "Liber Deuteronomii";
			ShortName[4]="Deuteronomium";
			Abbrev[5] = "Jos";
			LongName[5] = "Liber Josue";
			ShortName[5]="Josue";
			Abbrev[6] = "Jdc";
			LongName[6] = "Liber Judicum";
			ShortName[6]="Judicum";
			Abbrev[7] = "Rt";
			LongName[7] = "Liber Ruth";
			ShortName[7]="Ruth";
			Abbrev[8] = "1Rg";
			LongName[8] = "Liber Primus Regum";
			ShortName[8]="Regum I";
			Abbrev[9] = "2Rg";
			LongName[9] = "Liber Secundus Regum";
			ShortName[9]="Regum II";
			Abbrev[10] = "3Rg";
			LongName[10] = "Liber Tertius Regum";
			ShortName[10]="Regum III";
			Abbrev[11] = "4Rg";
			LongName[11] = "Liber Quartus Regum";
			ShortName[11]="Regum IV";
			Abbrev[12] = "1Par";
			LongName[12] = "Liber Primus Paralipomenon";
			ShortName[12]="Paralipomenon I";
			Abbrev[13] = "2Par";
			LongName[13] = "Liber Secundus Paralipomenon";
			ShortName[13]="Paralipomenon II";
			Abbrev[14] = "Esr";
			LongName[14] = "Liber Esdr�";
			ShortName[14]="Esdr�";
			Abbrev[15] = "Neh";
			LongName[15] = "Liber Nehemi�";
			ShortName[15]="Nehemi�";
			Abbrev[16] = "Tob";
			LongName[16] = "Liber Tobi�";
			ShortName[16]="Tobi�";
			Abbrev[17] = "Jdt";
			LongName[17] = "Liber Judith";
			ShortName[17]="Judith";
			Abbrev[18] = "Est";
			LongName[18] = "Liber Esther";
			ShortName[18]="Esther";
			Abbrev[19] = "Job";
			LongName[19] = "Liber Job";
			ShortName[19]="Job";
			Abbrev[20] = "Ps";
			LongName[20] = "Liber Psalmorum";
			ShortName[20]="Psalmi";
			Abbrev[21] = "Pr";
			LongName[21] = "Liber Proverbiorum";
			ShortName[21]="Proverbia";
			Abbrev[22] = "Ecl";
			LongName[22] = "Liber Ecclesiastes";
			ShortName[22]="Ecclesiastes";
			Abbrev[23] = "Ct";
			LongName[23] = "Canticum Canticorum Salomonis";
			ShortName[23]="Canticum Canticorum";
			Abbrev[24] = "Sap";
			LongName[24] = "Liber Sapienti�";
			ShortName[24]="Sapientia";
			Abbrev[25] = "Sir";
			LongName[25] = "Ecclesiasticus Jesu, Filii Sirach";
			ShortName[25]="Ecclesiasticus";
			Abbrev[26] = "Is";
			LongName[26] = "Prophetia Isai�";
			ShortName[26]="Isaias";
			Abbrev[27] = "Jr";
			LongName[27] = "Prophetia Jeremi�";
			ShortName[27]="Jeremias";
			Abbrev[28] = "Lam";
			LongName[28] = "Lamentationes Jeremi� Prophet�";
			ShortName[28]="Lamentationes";
			Abbrev[29] = "Bar";
			LongName[29] = "Prophetia Baruch";
			ShortName[29]="Baruch";
			Abbrev[30] = "Ez";
			LongName[30] = "Prophetia Ezechielis";
			ShortName[30]="Ezechiel";
			Abbrev[31] = "Dn";
			LongName[31] = "Prophetia Danielis";
			ShortName[31]="Daniel";
			Abbrev[32] = "Os";
			LongName[32] = "Prophetia Osee";
			ShortName[32]="Osee";
			Abbrev[33] = "Joel";
			LongName[33] = "Prophetia Jo�l";
			ShortName[33]="Jo�l";
			Abbrev[34] = "Am";
			LongName[34] = "Prophetia Amos";
			ShortName[34]="Amos";
			Abbrev[35] = "Abd";
			LongName[35] = "Prophetia Abdi�";
			ShortName[35]="Abdias";
			Abbrev[36] = "Jon";
			LongName[36] = "Prophetia Jon�";
			ShortName[36]="Jonas";
			Abbrev[37] = "Mch";
			LongName[37] = "Prophetia Mich��";
			ShortName[37]="Mich�a";
			Abbrev[38] = "Nah";
			LongName[38] = "Prophetia Nahum";
			ShortName[38]="Nahum";
			Abbrev[39] = "Hab";
			LongName[39] = "Prophetia Habacuc";
			ShortName[39]="Habacuc";
			Abbrev[40] = "Soph";
			LongName[40] = "Prophetia Sophoni�";
			ShortName[40]="Sophonias";
			Abbrev[41] = "Agg";
			LongName[41] = "Prophetia Agg�i";
			ShortName[41]="Agg�us";
			Abbrev[42] = "Zach";
			LongName[42] = "Prophetia Zachari�";
			ShortName[42]="Zacharias";
			Abbrev[43] = "Mal";
			LongName[43] = "Prophetia Malachi�";
			ShortName[43]="Malachias";
			Abbrev[44] = "1Mcc";
			LongName[44] = "Liber I Machab�orum";
			ShortName[44]="Machab�orum I";
			Abbrev[45] = "2Mcc";
			LongName[45] = "Liber II Machab�orum";
			ShortName[45]="Machab�orum II";
			Abbrev[46] = "Mt";
			LongName[46] = "Evangelium secundum Matth�um";
			ShortName[46]="Matth�us";
			Abbrev[47] = "Mc";
			LongName[47] = "Evangelium secundum Marcum";
			ShortName[47]="Marcus";
			Abbrev[48] = "Lc";
			LongName[48] = "Evangelium secundum Lucam";
			ShortName[48]="Lucas";
			Abbrev[49] = "Jo";
			LongName[49] = "Evangelium secundum Joannem";
			ShortName[49]="Joannes";
			Abbrev[50] = "Act";
			LongName[50] = "Actus Apostolorum";
			ShortName[50]="Actus Apostolorum";
			Abbrev[51] = "Rom";
			LongName[51] = "Epistola B. Pauli Apostoli ad Romanos";
			ShortName[51]="ad Romanos";
			Abbrev[52] = "1Cor";
			LongName[52] = "Epistola B. Pauli Apostoli ad Corinthios Prima";
			ShortName[52]="ad Corinthios I";
			Abbrev[53] = "2Cor";
			LongName[53] = "Epistola B. Pauli Apostoli ad Corinthios Secunda";
			ShortName[53]="ad Corinthios II";
			Abbrev[54] = "Gal";
			LongName[54] = "Epistola B. Pauli Apostoli ad Galatas";
			ShortName[54]="ad Galatas";
			Abbrev[55] = "Eph";
			LongName[55] = "Epistola B. Pauli Apostoli ad Ephesios";
			ShortName[55]="ad Ephesios";
			Abbrev[56] = "Phlp";
			LongName[56] = "Epistola B. Pauli Apostoli ad Philippenses";
			ShortName[56]="ad Philippenses";
			Abbrev[57] = "Col";
			LongName[57] = "Epistola B. Pauli Apostoli ad Colossenses";
			ShortName[57]="ad Colossenses";
			Abbrev[58] = "1Thes";
			LongName[58] = "Epistola B. Pauli Apostoli ad Thessalonicenses Prima";
			ShortName[58]="ad Thessalonicenses I";
			Abbrev[59] = "2Thes";
			LongName[59] = "Epistola B. Pauli Apostoli ad Thessalonicenses Secunda";
			ShortName[59]="ad Thessalonicenses II";
			Abbrev[60] = "1Tim";
			LongName[60] = "Epistola B. Pauli Apostoli ad Timotheum Prima";
			ShortName[60]="ad Timotheum I";
			Abbrev[61] = "2Tim";
			LongName[61] = "Epistola B. Pauli Apostoli ad Timotheum Secunda";
			ShortName[61]="ad Timotheum II";
			Abbrev[62] = "Tit";
			LongName[62] = "Epistola B. Pauli Apostoli ad Titum";
			ShortName[62]="ad Titum";
			Abbrev[63] = "Phlm";
			LongName[63] = "Epistola B. Pauli Apostoli ad Philemonem";
			ShortName[63]="ad Philemonem";
			Abbrev[64] = "Hbr";
			LongName[64] = "Epistola B. Pauli Apostoli ad Hebr�os";
			ShortName[64]="ad Hebr�os";
			Abbrev[65] = "Jac";
			LongName[65] = "Epistola Catholica B. Jacobi Apostoli";
			ShortName[65]="Jacobi";
			Abbrev[66] = "1Ptr";
			LongName[66] = "Epistola B. Petri Apostoli Prima";
			ShortName[66]="Petri I";
			Abbrev[67] = "2Ptr";
			LongName[67] = "Epistola B. Petri Apostoli Secunda";
			ShortName[67]="Petri II";
			Abbrev[68] = "1Jo";
			LongName[68] = "Epistola B. Joannis Apostoli Prima";
			ShortName[68]="Joannis I";
			Abbrev[69] = "2Jo";
			LongName[69] = "Epistola B. Joannis Apostoli Secunda";
			ShortName[69]="Joannis II";
			Abbrev[70] = "3Jo";
			LongName[70] = "Epistola B. Joannis Apostoli Tertia";
			ShortName[70]="Joannis III";
			Abbrev[71] = "Jud";
			LongName[71] = "Epistola Catholica B. Jud� Apostoli";
			ShortName[71]="Jud�";
			Abbrev[72] = "Apc";
			LongName[72] = "Apocalypsis B. Joannis Apostoli";
			ShortName[72]="Apocalypsis";

			NumberOfChapters[0]=50;
			NumberOfChapters[1]=40;
			NumberOfChapters[2]=27;
			NumberOfChapters[3]=36;
			NumberOfChapters[4]=34;
			NumberOfChapters[5]=24;
			NumberOfChapters[6]=21;
			NumberOfChapters[7]=4;
			NumberOfChapters[8]=31;
			NumberOfChapters[9]=24;
			NumberOfChapters[10]=22;
			NumberOfChapters[11]=25;
			NumberOfChapters[12]=29;
			NumberOfChapters[13]=36;
			NumberOfChapters[14]=10;
			NumberOfChapters[15]=13;
			NumberOfChapters[16]=14;
			NumberOfChapters[17]=16;
			NumberOfChapters[18]=16; // changed 6/1/05
			NumberOfChapters[19]=42;
			NumberOfChapters[20]=150;
			NumberOfChapters[21]=31;
			NumberOfChapters[22]=12;
			NumberOfChapters[23]=8;
			NumberOfChapters[24]=19;
			NumberOfChapters[25]=51;
			NumberOfChapters[26]=66;
			NumberOfChapters[27]=52;
			NumberOfChapters[28]=5;
			NumberOfChapters[29]=6;
			NumberOfChapters[30]=48;
			NumberOfChapters[31]=14;
			NumberOfChapters[32]=14;
			NumberOfChapters[33]=3;
			NumberOfChapters[34]=9;
			NumberOfChapters[35]=1;
			NumberOfChapters[36]=4;
			NumberOfChapters[37]=7;
			NumberOfChapters[38]=3;
			NumberOfChapters[39]=3;
			NumberOfChapters[40]=3;
			NumberOfChapters[41]=2;
			NumberOfChapters[42]=14;
			NumberOfChapters[43]=4;
			NumberOfChapters[44]=16;
			NumberOfChapters[45]=15;
			NumberOfChapters[46]=28;
			NumberOfChapters[47]=16;
			NumberOfChapters[48]=24;
			NumberOfChapters[49]=21;
			NumberOfChapters[50]=28;
			NumberOfChapters[51]=16;
			NumberOfChapters[52]=16;
			NumberOfChapters[53]=13;
			NumberOfChapters[54]=6;
			NumberOfChapters[55]=6;
			NumberOfChapters[56]=4;
			NumberOfChapters[57]=4;
			NumberOfChapters[58]=5;
			NumberOfChapters[59]=3;
			NumberOfChapters[60]=6;
			NumberOfChapters[61]=4;
			NumberOfChapters[62]=3;
			NumberOfChapters[63]=1;
			NumberOfChapters[64]=13;
			NumberOfChapters[65]=5;
			NumberOfChapters[66]=5;
			NumberOfChapters[67]=3;
			NumberOfChapters[68]=5;
			NumberOfChapters[69]=1;
			NumberOfChapters[70]=1;
			NumberOfChapters[71]=1;
			NumberOfChapters[72]=22;
		} catch {}
		}



		public Bible(string Name, string Extension, string Desc, bool IsLatin)
		{
			try {
			name = Name;
			extension = Extension;
			desc=Desc;
			isLatin=IsLatin;
			SetupNameConsts();

			/* Each Book knows which number it is (0-72).
			 * This lets it know which file Abbrev.lat etc. to read */
			for (byte i=0; i<73; i++)
			{
				Books[i]=new Book(i, NumberOfChapters[i], this);
			}
		} catch {}
		}
	}
}
