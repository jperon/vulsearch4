using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace VulSearch4
{
	/// <summary>
	/// Summary description for FindInPageForm.
	/// </summary>
	public class FindInPageForm : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button btnNext;
		internal System.Windows.Forms.TextBox txtFind;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public FindInPageForm()
		{
			try {
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		} catch {}
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			try {
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		} catch {}
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(FindInPageForm));
			this.txtFind = new System.Windows.Forms.TextBox();
			this.btnNext = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// txtFind
			// 
			this.txtFind.AccessibleDescription = resources.GetString("txtFind.AccessibleDescription");
			this.txtFind.AccessibleName = resources.GetString("txtFind.AccessibleName");
			this.txtFind.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("txtFind.Anchor")));
			this.txtFind.AutoSize = ((bool)(resources.GetObject("txtFind.AutoSize")));
			this.txtFind.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtFind.BackgroundImage")));
			this.txtFind.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("txtFind.Dock")));
			this.txtFind.Enabled = ((bool)(resources.GetObject("txtFind.Enabled")));
			this.txtFind.Font = ((System.Drawing.Font)(resources.GetObject("txtFind.Font")));
			this.txtFind.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("txtFind.ImeMode")));
			this.txtFind.Location = ((System.Drawing.Point)(resources.GetObject("txtFind.Location")));
			this.txtFind.MaxLength = ((int)(resources.GetObject("txtFind.MaxLength")));
			this.txtFind.Multiline = ((bool)(resources.GetObject("txtFind.Multiline")));
			this.txtFind.Name = "txtFind";
			this.txtFind.PasswordChar = ((char)(resources.GetObject("txtFind.PasswordChar")));
			this.txtFind.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("txtFind.RightToLeft")));
			this.txtFind.ScrollBars = ((System.Windows.Forms.ScrollBars)(resources.GetObject("txtFind.ScrollBars")));
			this.txtFind.Size = ((System.Drawing.Size)(resources.GetObject("txtFind.Size")));
			this.txtFind.TabIndex = ((int)(resources.GetObject("txtFind.TabIndex")));
			this.txtFind.Text = resources.GetString("txtFind.Text");
			this.txtFind.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("txtFind.TextAlign")));
			this.txtFind.Visible = ((bool)(resources.GetObject("txtFind.Visible")));
			this.txtFind.WordWrap = ((bool)(resources.GetObject("txtFind.WordWrap")));
			this.txtFind.TextChanged += new System.EventHandler(this.txtFind_TextChanged);
			// 
			// btnNext
			// 
			this.btnNext.AccessibleDescription = resources.GetString("btnNext.AccessibleDescription");
			this.btnNext.AccessibleName = resources.GetString("btnNext.AccessibleName");
			this.btnNext.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnNext.Anchor")));
			this.btnNext.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnNext.BackgroundImage")));
			this.btnNext.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnNext.Dock")));
			this.btnNext.Enabled = ((bool)(resources.GetObject("btnNext.Enabled")));
			this.btnNext.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnNext.FlatStyle")));
			this.btnNext.Font = ((System.Drawing.Font)(resources.GetObject("btnNext.Font")));
			this.btnNext.Image = ((System.Drawing.Image)(resources.GetObject("btnNext.Image")));
			this.btnNext.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnNext.ImageAlign")));
			this.btnNext.ImageIndex = ((int)(resources.GetObject("btnNext.ImageIndex")));
			this.btnNext.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnNext.ImeMode")));
			this.btnNext.Location = ((System.Drawing.Point)(resources.GetObject("btnNext.Location")));
			this.btnNext.Name = "btnNext";
			this.btnNext.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnNext.RightToLeft")));
			this.btnNext.Size = ((System.Drawing.Size)(resources.GetObject("btnNext.Size")));
			this.btnNext.TabIndex = ((int)(resources.GetObject("btnNext.TabIndex")));
			this.btnNext.Text = resources.GetString("btnNext.Text");
			this.btnNext.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnNext.TextAlign")));
			this.btnNext.Visible = ((bool)(resources.GetObject("btnNext.Visible")));
			this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
			// 
			// FindInPageForm
			// 
			this.AcceptButton = this.btnNext;
			this.AccessibleDescription = resources.GetString("$this.AccessibleDescription");
			this.AccessibleName = resources.GetString("$this.AccessibleName");
			this.AutoScaleBaseSize = ((System.Drawing.Size)(resources.GetObject("$this.AutoScaleBaseSize")));
			this.AutoScroll = ((bool)(resources.GetObject("$this.AutoScroll")));
			this.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("$this.AutoScrollMargin")));
			this.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("$this.AutoScrollMinSize")));
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = ((System.Drawing.Size)(resources.GetObject("$this.ClientSize")));
			this.Controls.Add(this.txtFind);
			this.Controls.Add(this.btnNext);
			this.Enabled = ((bool)(resources.GetObject("$this.Enabled")));
			this.Font = ((System.Drawing.Font)(resources.GetObject("$this.Font")));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("$this.ImeMode")));
			this.Location = ((System.Drawing.Point)(resources.GetObject("$this.Location")));
			this.MaximumSize = ((System.Drawing.Size)(resources.GetObject("$this.MaximumSize")));
			this.MinimumSize = ((System.Drawing.Size)(resources.GetObject("$this.MinimumSize")));
			this.Name = "FindInPageForm";
			this.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("$this.RightToLeft")));
			this.ShowInTaskbar = false;
			this.StartPosition = ((System.Windows.Forms.FormStartPosition)(resources.GetObject("$this.StartPosition")));
			this.Text = resources.GetString("$this.Text");
			this.TopMost = true;
			this.Closing += new System.ComponentModel.CancelEventHandler(this.FindInPageForm_Closing);
			this.ResumeLayout(false);

		}
		#endregion

		private void txtFind_TextChanged(object sender, System.EventArgs e)
		{
			try {
			bool p=Startup.MainForm.FindNext(txtFind.Text,false);
			DoColours(p);
		} catch {}
		}

		private void btnNext_Click(object sender, System.EventArgs e)
		{
			try {
			bool p=Startup.MainForm.FindNext(txtFind.Text,true);
			DoColours(p);
		} catch {}
		}

		private void FindInPageForm_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			try {
			e.Cancel=true;
			this.Hide();
		} catch {}
		}

		private void DoColours(bool p)
		{
			try {
			txtFind.BackColor=(p ? Color.FromArgb(255,255,255) : Color.FromArgb(222,142,131));
			txtFind.ForeColor =(p ? Color.FromArgb(0,0,0) : Color.FromArgb(255,255,255));
		} catch {}
		}


	}
}
