using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using Microsoft.Win32;
using System.IO;

namespace VulSearch4
{
	/// <summary>
	/// Summary description for ImportOldForm.
	/// </summary>
	public class ImportOldForm : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.RadioButton radEn;
		private System.Windows.Forms.RadioButton radFr;
		private System.Windows.Forms.RadioButton radNo;
		private System.Windows.Forms.Button btnNext;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		private RegistryKey key, keyEn, keyFr;
		private string pathEn,pathFr;

		public ImportOldForm(RegistryKey Key)
		{
			try {
			key=Key;
			InitializeComponent();
		} catch {}
		}


		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			try {
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		} catch {}
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(ImportOldForm));
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.radEn = new System.Windows.Forms.RadioButton();
			this.radFr = new System.Windows.Forms.RadioButton();
			this.radNo = new System.Windows.Forms.RadioButton();
			this.btnNext = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AccessibleDescription = resources.GetString("label1.AccessibleDescription");
			this.label1.AccessibleName = resources.GetString("label1.AccessibleName");
			this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("label1.Anchor")));
			this.label1.AutoSize = ((bool)(resources.GetObject("label1.AutoSize")));
			this.label1.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("label1.Dock")));
			this.label1.Enabled = ((bool)(resources.GetObject("label1.Enabled")));
			this.label1.Font = ((System.Drawing.Font)(resources.GetObject("label1.Font")));
			this.label1.Image = ((System.Drawing.Image)(resources.GetObject("label1.Image")));
			this.label1.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label1.ImageAlign")));
			this.label1.ImageIndex = ((int)(resources.GetObject("label1.ImageIndex")));
			this.label1.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("label1.ImeMode")));
			this.label1.Location = ((System.Drawing.Point)(resources.GetObject("label1.Location")));
			this.label1.Name = "label1";
			this.label1.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("label1.RightToLeft")));
			this.label1.Size = ((System.Drawing.Size)(resources.GetObject("label1.Size")));
			this.label1.TabIndex = ((int)(resources.GetObject("label1.TabIndex")));
			this.label1.Text = resources.GetString("label1.Text");
			this.label1.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label1.TextAlign")));
			this.label1.Visible = ((bool)(resources.GetObject("label1.Visible")));
			// 
			// label2
			// 
			this.label2.AccessibleDescription = resources.GetString("label2.AccessibleDescription");
			this.label2.AccessibleName = resources.GetString("label2.AccessibleName");
			this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("label2.Anchor")));
			this.label2.AutoSize = ((bool)(resources.GetObject("label2.AutoSize")));
			this.label2.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("label2.Dock")));
			this.label2.Enabled = ((bool)(resources.GetObject("label2.Enabled")));
			this.label2.Font = ((System.Drawing.Font)(resources.GetObject("label2.Font")));
			this.label2.Image = ((System.Drawing.Image)(resources.GetObject("label2.Image")));
			this.label2.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label2.ImageAlign")));
			this.label2.ImageIndex = ((int)(resources.GetObject("label2.ImageIndex")));
			this.label2.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("label2.ImeMode")));
			this.label2.Location = ((System.Drawing.Point)(resources.GetObject("label2.Location")));
			this.label2.Name = "label2";
			this.label2.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("label2.RightToLeft")));
			this.label2.Size = ((System.Drawing.Size)(resources.GetObject("label2.Size")));
			this.label2.TabIndex = ((int)(resources.GetObject("label2.TabIndex")));
			this.label2.Text = resources.GetString("label2.Text");
			this.label2.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label2.TextAlign")));
			this.label2.Visible = ((bool)(resources.GetObject("label2.Visible")));
			// 
			// radEn
			// 
			this.radEn.AccessibleDescription = resources.GetString("radEn.AccessibleDescription");
			this.radEn.AccessibleName = resources.GetString("radEn.AccessibleName");
			this.radEn.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("radEn.Anchor")));
			this.radEn.Appearance = ((System.Windows.Forms.Appearance)(resources.GetObject("radEn.Appearance")));
			this.radEn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("radEn.BackgroundImage")));
			this.radEn.CheckAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("radEn.CheckAlign")));
			this.radEn.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("radEn.Dock")));
			this.radEn.Enabled = ((bool)(resources.GetObject("radEn.Enabled")));
			this.radEn.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("radEn.FlatStyle")));
			this.radEn.Font = ((System.Drawing.Font)(resources.GetObject("radEn.Font")));
			this.radEn.Image = ((System.Drawing.Image)(resources.GetObject("radEn.Image")));
			this.radEn.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("radEn.ImageAlign")));
			this.radEn.ImageIndex = ((int)(resources.GetObject("radEn.ImageIndex")));
			this.radEn.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("radEn.ImeMode")));
			this.radEn.Location = ((System.Drawing.Point)(resources.GetObject("radEn.Location")));
			this.radEn.Name = "radEn";
			this.radEn.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("radEn.RightToLeft")));
			this.radEn.Size = ((System.Drawing.Size)(resources.GetObject("radEn.Size")));
			this.radEn.TabIndex = ((int)(resources.GetObject("radEn.TabIndex")));
			this.radEn.Text = resources.GetString("radEn.Text");
			this.radEn.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("radEn.TextAlign")));
			this.radEn.Visible = ((bool)(resources.GetObject("radEn.Visible")));
			// 
			// radFr
			// 
			this.radFr.AccessibleDescription = resources.GetString("radFr.AccessibleDescription");
			this.radFr.AccessibleName = resources.GetString("radFr.AccessibleName");
			this.radFr.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("radFr.Anchor")));
			this.radFr.Appearance = ((System.Windows.Forms.Appearance)(resources.GetObject("radFr.Appearance")));
			this.radFr.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("radFr.BackgroundImage")));
			this.radFr.CheckAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("radFr.CheckAlign")));
			this.radFr.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("radFr.Dock")));
			this.radFr.Enabled = ((bool)(resources.GetObject("radFr.Enabled")));
			this.radFr.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("radFr.FlatStyle")));
			this.radFr.Font = ((System.Drawing.Font)(resources.GetObject("radFr.Font")));
			this.radFr.Image = ((System.Drawing.Image)(resources.GetObject("radFr.Image")));
			this.radFr.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("radFr.ImageAlign")));
			this.radFr.ImageIndex = ((int)(resources.GetObject("radFr.ImageIndex")));
			this.radFr.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("radFr.ImeMode")));
			this.radFr.Location = ((System.Drawing.Point)(resources.GetObject("radFr.Location")));
			this.radFr.Name = "radFr";
			this.radFr.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("radFr.RightToLeft")));
			this.radFr.Size = ((System.Drawing.Size)(resources.GetObject("radFr.Size")));
			this.radFr.TabIndex = ((int)(resources.GetObject("radFr.TabIndex")));
			this.radFr.Text = resources.GetString("radFr.Text");
			this.radFr.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("radFr.TextAlign")));
			this.radFr.Visible = ((bool)(resources.GetObject("radFr.Visible")));
			// 
			// radNo
			// 
			this.radNo.AccessibleDescription = resources.GetString("radNo.AccessibleDescription");
			this.radNo.AccessibleName = resources.GetString("radNo.AccessibleName");
			this.radNo.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("radNo.Anchor")));
			this.radNo.Appearance = ((System.Windows.Forms.Appearance)(resources.GetObject("radNo.Appearance")));
			this.radNo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("radNo.BackgroundImage")));
			this.radNo.CheckAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("radNo.CheckAlign")));
			this.radNo.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("radNo.Dock")));
			this.radNo.Enabled = ((bool)(resources.GetObject("radNo.Enabled")));
			this.radNo.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("radNo.FlatStyle")));
			this.radNo.Font = ((System.Drawing.Font)(resources.GetObject("radNo.Font")));
			this.radNo.Image = ((System.Drawing.Image)(resources.GetObject("radNo.Image")));
			this.radNo.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("radNo.ImageAlign")));
			this.radNo.ImageIndex = ((int)(resources.GetObject("radNo.ImageIndex")));
			this.radNo.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("radNo.ImeMode")));
			this.radNo.Location = ((System.Drawing.Point)(resources.GetObject("radNo.Location")));
			this.radNo.Name = "radNo";
			this.radNo.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("radNo.RightToLeft")));
			this.radNo.Size = ((System.Drawing.Size)(resources.GetObject("radNo.Size")));
			this.radNo.TabIndex = ((int)(resources.GetObject("radNo.TabIndex")));
			this.radNo.Text = resources.GetString("radNo.Text");
			this.radNo.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("radNo.TextAlign")));
			this.radNo.Visible = ((bool)(resources.GetObject("radNo.Visible")));
			// 
			// btnNext
			// 
			this.btnNext.AccessibleDescription = resources.GetString("btnNext.AccessibleDescription");
			this.btnNext.AccessibleName = resources.GetString("btnNext.AccessibleName");
			this.btnNext.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnNext.Anchor")));
			this.btnNext.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnNext.BackgroundImage")));
			this.btnNext.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnNext.Dock")));
			this.btnNext.Enabled = ((bool)(resources.GetObject("btnNext.Enabled")));
			this.btnNext.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnNext.FlatStyle")));
			this.btnNext.Font = ((System.Drawing.Font)(resources.GetObject("btnNext.Font")));
			this.btnNext.Image = ((System.Drawing.Image)(resources.GetObject("btnNext.Image")));
			this.btnNext.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnNext.ImageAlign")));
			this.btnNext.ImageIndex = ((int)(resources.GetObject("btnNext.ImageIndex")));
			this.btnNext.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnNext.ImeMode")));
			this.btnNext.Location = ((System.Drawing.Point)(resources.GetObject("btnNext.Location")));
			this.btnNext.Name = "btnNext";
			this.btnNext.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnNext.RightToLeft")));
			this.btnNext.Size = ((System.Drawing.Size)(resources.GetObject("btnNext.Size")));
			this.btnNext.TabIndex = ((int)(resources.GetObject("btnNext.TabIndex")));
			this.btnNext.Text = resources.GetString("btnNext.Text");
			this.btnNext.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnNext.TextAlign")));
			this.btnNext.Visible = ((bool)(resources.GetObject("btnNext.Visible")));
			this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
			// 
			// ImportOldForm
			// 
			this.AcceptButton = this.btnNext;
			this.AccessibleDescription = resources.GetString("$this.AccessibleDescription");
			this.AccessibleName = resources.GetString("$this.AccessibleName");
			this.AutoScaleBaseSize = ((System.Drawing.Size)(resources.GetObject("$this.AutoScaleBaseSize")));
			this.AutoScroll = ((bool)(resources.GetObject("$this.AutoScroll")));
			this.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("$this.AutoScrollMargin")));
			this.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("$this.AutoScrollMinSize")));
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = ((System.Drawing.Size)(resources.GetObject("$this.ClientSize")));
			this.ControlBox = false;
			this.Controls.Add(this.btnNext);
			this.Controls.Add(this.radNo);
			this.Controls.Add(this.radFr);
			this.Controls.Add(this.radEn);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Enabled = ((bool)(resources.GetObject("$this.Enabled")));
			this.Font = ((System.Drawing.Font)(resources.GetObject("$this.Font")));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("$this.ImeMode")));
			this.Location = ((System.Drawing.Point)(resources.GetObject("$this.Location")));
			this.MaximizeBox = false;
			this.MaximumSize = ((System.Drawing.Size)(resources.GetObject("$this.MaximumSize")));
			this.MinimumSize = ((System.Drawing.Size)(resources.GetObject("$this.MinimumSize")));
			this.Name = "ImportOldForm";
			this.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("$this.RightToLeft")));
			this.StartPosition = ((System.Windows.Forms.FormStartPosition)(resources.GetObject("$this.StartPosition")));
			this.Text = resources.GetString("$this.Text");
			this.Load += new System.EventHandler(this.ImportOldForm_Load);
			this.VisibleChanged += new System.EventHandler(this.ImportOldForm_VisibleChanged);
			this.ResumeLayout(false);

		}
		#endregion

		private void ImportOldForm_Load(object sender, System.EventArgs e)
		{
			try {
			// try to import old VulSearch 3 settings
			keyEn = Registry.CurrentUser.OpenSubKey(
				@"Software\VB and VBA Program Settings\VulSearch\Version");
			keyFr = Registry.CurrentUser.OpenSubKey(
					@"Software\VB and VBA Program Settings\VulSearch_fr\Version");
			radEn.Enabled=(keyEn!=null);
			radFr.Enabled=(keyFr!= null);
			if (radEn.Enabled) radEn.Checked=true; else radFr.Checked=true;
			if (keyEn!=null)
			{
				pathEn=(string) keyEn.GetValue("Path");
				keyEn.Close();
				keyEn=Registry.CurrentUser.OpenSubKey(
					@"Software\VB and VBA Program Settings\VulSearch\Options");
			}

			if (keyFr!=null)
			{
				pathFr=(string) keyFr.GetValue("Path");
				keyFr.Close();
				keyFr=Registry.CurrentUser.OpenSubKey(
					@"Software\VB and VBA Program Settings\VulSearch_fr\Options");
			}
		} catch {}
		}

		private void btnNext_Click(object sender, System.EventArgs e)
		{
			try {
			if (radEn.Checked) ImportSettings(keyEn,pathEn);
			if (radFr.Checked) ImportSettings(keyFr,pathFr);
			this.Hide();
		} catch {}
		}

		private void ImportSettings(RegistryKey k, string path)
		{
			try {
			try
			{
				ImportOldBMs(path);
				Startup.BookmarksForm.UpdateImages(Startup.BookmarksForm.tvwbm.Nodes[0]);
			}
			catch (Exception e)
			{
				MessageBox.Show(Startup.rm.GetString("importbmfail")
					+ e.Message);
			}

			try
			{
				ImportNotes(path);
			}
			catch (Exception e)
			{
				MessageBox.Show(Startup.rm.GetString("importnotesfail")
					+ e.Message);
			}

			try
			{
				ImportXRefs(path);
			}
			catch (Exception e)
			{
				MessageBox.Show(Startup.rm.GetString("importxreffail")
					+ e.Message);
			}

			try
			{
				int i=int.Parse((string) k.GetValue("LB","1"))-1;
				key.SetValue("LastBook",i.ToString() );
				i=int.Parse((string) k.GetValue("LC","1"))-1;
				key.SetValue("LastChapter",i.ToString() );
				i=int.Parse((string) k.GetValue("LV","1"))-1;
				key.SetValue("LastVerse",i.ToString() );
				i=int.Parse((string) k.GetValue("Style","1"));
				switch (i)
				{
					case 1:
					{
						key.SetValue("Missals", "true");
						key.SetValue("Printed","false");
						key.SetValue("Classical","false");
						key.SetValue("Rules","false");
						break;
					}
					case 2:
					{
						key.SetValue("Missals", "false");
						key.SetValue("Printed","true");
						key.SetValue("Classical","false");
						key.SetValue("Rules","false");
						break;
					}
					case 3:
					{
						key.SetValue("Missals", "false");
						key.SetValue("Printed","false");
						key.SetValue("Classical","true");
						key.SetValue("Rules","false");
						break;
					}
					case 4:
					{
						key.SetValue("Missals", "false");
						key.SetValue("Printed","false");
						key.SetValue("Classical","false");
						key.SetValue("Rules","true");
						break;
					}
				}
				bool b=bool.Parse(((string)k.GetValue("IJ","true")).ToLower());
				key.SetValue("IJ",b.ToString());
				b=bool.Parse(((string)k.GetValue("UV","true")).ToLower());
				key.SetValue("UV",b.ToString());
				b=bool.Parse(((string)k.GetValue("AE","true")).ToLower());
				key.SetValue("AE",b.ToString());
				b=bool.Parse(((string)k.GetValue("SaveHistory","true")).ToLower());
				key.SetValue("SaveHistory",b.ToString());
				key.SetValue("nudHistory","20");
				for (int j=1; j<=50 ; j++)
				{
					key.SetValue("History"+j.ToString(),"");
				}
				if (b)
				{
					for (int j=1; j<=20;j++)
					{
						key.SetValue("History"+j.ToString(),
							(string) k.GetValue("Bookmark"+j.ToString()));
					}
				}
			}
			catch (Exception e)
			{
				MessageBox.Show(Startup.rm.GetString("importsettingsfail")
					+ e.Message);
			}
			// add code to import Notes & Xrefs.
		} catch {}
		}

		/// <summary>
		/// This imports a collection of bookmarks created with
		/// VulSearch 3. What a mess!!! What was I thinking?!
		/// </summary>
		private static void ImportOldBMs(string path)
		{
			try {
			StreamReader sr;
			FileStream file = new FileStream(path + @"\bookmarks",
				FileMode.Open,FileAccess.Read);
			sr = new StreamReader(file, 
				System.Text.Encoding.GetEncoding(1252));
			sr.ReadLine(); // root bookmark
			string z=sr.ReadToEnd().Trim();
			sr.Close();
			TreeNode phi=new TreeNode("VulSearch 3");
			Startup.BookmarksForm.tvwbm.Nodes[0].Nodes.Add(phi);

			//z=z.Replace("\r\n","\n");
			Stack nodes = new Stack();
			TreeNode currentNode=phi, n=phi;
			RichTextBox rtb=new RichTextBox();

			foreach (string a in z.Split('\n'))
			{
				if (a.StartsWith("CHILDREN"))
				{
					nodes.Push(currentNode);
					currentNode=n;
				}
				else if (a.StartsWith("END CHILDREN"))
				{
					currentNode=nodes.Pop() as TreeNode;
				}
				else
				{
					n=new TreeNode(a.Substring(0,a.IndexOf("�")));
					currentNode.Nodes.Insert(0,n);
					string b=a.Substring(a.IndexOf("�")+1);
					Startup.BookmarksForm.hashRef[n.GetHashCode()]=b.Substring(0,b.IndexOf("�"));
					if (rtb.Text.Length>0) rtb.Clear();
					rtb.SelectionFont=Startup.MainForm.Font;
					rtb.SelectedText=b.Substring(b.IndexOf("�")+1).Replace("�","\n").Trim();
					Startup.BookmarksForm.hashText[n.GetHashCode()]=rtb.Rtf;
				}
			}
		} catch {}
		}

		private static void ImportNotes(string path)
		{
			try {
			StreamReader sr;
			FileStream file;
			
			for (int i=0;i<73;i++)
			{
				if (!File.Exists(path + @"\"+Bible.Abbrev[i]+".not")) continue;
				file= new FileStream(path + @"\"+Bible.Abbrev[i]+".not",
					FileMode.Open,FileAccess.Read);
				sr = new StreamReader(file, 
					System.Text.Encoding.GetEncoding(1252));
				while (sr.Peek()!=-1)
				{
					string y=sr.ReadLine();
					string z=sr.ReadLine();
					Startup.hashNotes[Bible.Abbrev[i]+" "+y.Trim()]
						=z.Trim().Replace("�","\n");
				}
			}
		} catch {}
		}

		private static void ImportXRefs(string path)
		{
			try {
			StreamReader sr;
			FileStream file;
			
			for (int i=0;i<73;i++)
			{
				if (!File.Exists(path + @"\"+Bible.Abbrev[i]+".ref")) continue;
				file= new FileStream(path + @"\"+Bible.Abbrev[i]+".ref",
					FileMode.Open,FileAccess.Read);
				sr = new StreamReader(file, 
					System.Text.Encoding.GetEncoding(1252));
				while (sr.Peek()!=-1)
				{
					string y=sr.ReadLine();
					string z=sr.ReadLine();
					ArrayList a=new ArrayList();
					while (z.IndexOf("�")>1)
					{
						z+="  ";
						string r=z.Substring(0,z.IndexOf("�"));
						z=z.Substring(z.IndexOf("�")+1);
						string d="";
						if (z.IndexOf("�")>0) d=z.Substring(0,z.IndexOf("�"));
						z=z.Substring(z.IndexOf("�")+1);
						z=z.Trim();
						if (r.EndsWith("*")) r=r.Substring(0,r.Length-1); // no idea what this was for!
						//a.Add(new XRef(r,d));
					}
					Startup.hashXref[Bible.Abbrev[i]+" "+y.Trim()]
						=a;
				}
			}
		} catch {}
		}

		private void ImportOldForm_VisibleChanged(object sender, System.EventArgs e)
		{
			try {
			if (!(radEn.Enabled | radFr.Enabled))
				this.Hide(); 
		} catch {}
		}

	}
}
