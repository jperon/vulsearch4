using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace VulSearch4
{
	/// <summary>
	/// Summary description for OptionsForm.
	/// </summary>
	public class OptionsForm : System.Windows.Forms.Form
	{
        private System.Windows.Forms.TabControl tabControl;
		private System.Windows.Forms.Button btnOK;
		private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.TabPage tabFormat;
        private System.Windows.Forms.TabPage tabLatin;
		private System.Windows.Forms.GroupBox groupBox1;
		public System.Windows.Forms.FontDialog fontMain;
		public System.Windows.Forms.FontDialog fontNumbers;
		private System.Windows.Forms.Label label1;
		public System.Windows.Forms.Label lblMainText;
		private System.Windows.Forms.Button btnChangeMainText;
		public System.Windows.Forms.Label lblVerseNumbers;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.GroupBox groupBox2;
		public System.Windows.Forms.RadioButton radVerseNewLine;
		public System.Windows.Forms.RadioButton radParagraphs;
		public System.Windows.Forms.CheckBox chkPoetry;
		private System.Windows.Forms.Button btnChangeVerseNumbers;
		public System.Windows.Forms.CheckBox chkVerseSuper;
		private System.Windows.Forms.Button btnApply;
        private System.Windows.Forms.TabPage tabGeneral;
		public System.Windows.Forms.CheckBox chkSaveHistory;
		public System.Windows.Forms.NumericUpDown nudHistory;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.GroupBox groupBox3;
		public System.Windows.Forms.CheckBox chkParallelScroll;
		private System.Windows.Forms.GroupBox groupBox4;
		public System.Windows.Forms.RadioButton radMissals;
		public System.Windows.Forms.RadioButton radPrinted;
		public System.Windows.Forms.RadioButton radClassical;
		public System.Windows.Forms.RadioButton radRules;
		public System.Windows.Forms.CheckBox chkIJ;
		public System.Windows.Forms.CheckBox chkUV;
		public System.Windows.Forms.CheckBox chkAE;
		public System.Windows.Forms.CheckBox chkWordsTooltip;
		public System.Windows.Forms.CheckBox chkNoWords;
		private System.Windows.Forms.GroupBox groupBox5;
		public System.Windows.Forms.CheckBox chkStoreSearch;
        public System.Windows.Forms.CheckBox chkSaveSearch;
		internal System.Windows.Forms.CheckBox chkAccent;
		public System.Windows.Forms.CheckBox chkInternet;



		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public OptionsForm()
		{
			try {
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			//tabControl.TabPages[0].Selected=true;

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		} catch {}
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			try {
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		} catch {}
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OptionsForm));
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabFormat = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.chkPoetry = new System.Windows.Forms.CheckBox();
            this.radParagraphs = new System.Windows.Forms.RadioButton();
            this.radVerseNewLine = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.chkVerseSuper = new System.Windows.Forms.CheckBox();
            this.btnChangeVerseNumbers = new System.Windows.Forms.Button();
            this.lblVerseNumbers = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnChangeMainText = new System.Windows.Forms.Button();
            this.lblMainText = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabLatin = new System.Windows.Forms.TabPage();
            this.chkNoWords = new System.Windows.Forms.CheckBox();
            this.chkWordsTooltip = new System.Windows.Forms.CheckBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.chkAccent = new System.Windows.Forms.CheckBox();
            this.chkAE = new System.Windows.Forms.CheckBox();
            this.chkUV = new System.Windows.Forms.CheckBox();
            this.chkIJ = new System.Windows.Forms.CheckBox();
            this.radRules = new System.Windows.Forms.RadioButton();
            this.radClassical = new System.Windows.Forms.RadioButton();
            this.radPrinted = new System.Windows.Forms.RadioButton();
            this.radMissals = new System.Windows.Forms.RadioButton();
            this.tabGeneral = new System.Windows.Forms.TabPage();
            this.chkInternet = new System.Windows.Forms.CheckBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.chkSaveSearch = new System.Windows.Forms.CheckBox();
            this.chkStoreSearch = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.nudHistory = new System.Windows.Forms.NumericUpDown();
            this.chkSaveHistory = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.chkParallelScroll = new System.Windows.Forms.CheckBox();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnApply = new System.Windows.Forms.Button();
            this.fontMain = new System.Windows.Forms.FontDialog();
            this.fontNumbers = new System.Windows.Forms.FontDialog();
            this.tabControl.SuspendLayout();
            this.tabFormat.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabLatin.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.tabGeneral.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudHistory)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabFormat);
            this.tabControl.Controls.Add(this.tabLatin);
            this.tabControl.Controls.Add(this.tabGeneral);
            resources.ApplyResources(this.tabControl, "tabControl");
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.MouseUp += new System.Windows.Forms.MouseEventHandler(this.tabControl_MouseUp);
            // 
            // tabFormat
            // 
            this.tabFormat.Controls.Add(this.groupBox2);
            this.tabFormat.Controls.Add(this.groupBox1);
            resources.ApplyResources(this.tabFormat, "tabFormat");
            this.tabFormat.Name = "tabFormat";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.chkPoetry);
            this.groupBox2.Controls.Add(this.radParagraphs);
            this.groupBox2.Controls.Add(this.radVerseNewLine);
            resources.ApplyResources(this.groupBox2, "groupBox2");
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.TabStop = false;
            // 
            // chkPoetry
            // 
            resources.ApplyResources(this.chkPoetry, "chkPoetry");
            this.chkPoetry.Name = "chkPoetry";
            // 
            // radParagraphs
            // 
            resources.ApplyResources(this.radParagraphs, "radParagraphs");
            this.radParagraphs.Name = "radParagraphs";
            // 
            // radVerseNewLine
            // 
            resources.ApplyResources(this.radVerseNewLine, "radVerseNewLine");
            this.radVerseNewLine.Name = "radVerseNewLine";
            this.radVerseNewLine.CheckedChanged += new System.EventHandler(this.radVerseNewLine_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.chkVerseSuper);
            this.groupBox1.Controls.Add(this.btnChangeVerseNumbers);
            this.groupBox1.Controls.Add(this.lblVerseNumbers);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.btnChangeMainText);
            this.groupBox1.Controls.Add(this.lblMainText);
            this.groupBox1.Controls.Add(this.label1);
            resources.ApplyResources(this.groupBox1, "groupBox1");
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.TabStop = false;
            // 
            // chkVerseSuper
            // 
            resources.ApplyResources(this.chkVerseSuper, "chkVerseSuper");
            this.chkVerseSuper.Name = "chkVerseSuper";
            this.chkVerseSuper.CheckedChanged += new System.EventHandler(this.chkVerseSuper_CheckedChanged);
            // 
            // btnChangeVerseNumbers
            // 
            resources.ApplyResources(this.btnChangeVerseNumbers, "btnChangeVerseNumbers");
            this.btnChangeVerseNumbers.Name = "btnChangeVerseNumbers";
            this.btnChangeVerseNumbers.Click += new System.EventHandler(this.btnChangeVerseNumbers_Click);
            // 
            // lblVerseNumbers
            // 
            resources.ApplyResources(this.lblVerseNumbers, "lblVerseNumbers");
            this.lblVerseNumbers.Name = "lblVerseNumbers";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // btnChangeMainText
            // 
            resources.ApplyResources(this.btnChangeMainText, "btnChangeMainText");
            this.btnChangeMainText.Name = "btnChangeMainText";
            this.btnChangeMainText.Click += new System.EventHandler(this.btnChangeMainText_Click);
            // 
            // lblMainText
            // 
            resources.ApplyResources(this.lblMainText, "lblMainText");
            this.lblMainText.Name = "lblMainText";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // tabLatin
            // 
            this.tabLatin.Controls.Add(this.chkNoWords);
            this.tabLatin.Controls.Add(this.chkWordsTooltip);
            this.tabLatin.Controls.Add(this.groupBox4);
            resources.ApplyResources(this.tabLatin, "tabLatin");
            this.tabLatin.Name = "tabLatin";
            // 
            // chkNoWords
            // 
            resources.ApplyResources(this.chkNoWords, "chkNoWords");
            this.chkNoWords.Name = "chkNoWords";
            this.chkNoWords.CheckedChanged += new System.EventHandler(this.chkNoWords_CheckedChanged);
            // 
            // chkWordsTooltip
            // 
            resources.ApplyResources(this.chkWordsTooltip, "chkWordsTooltip");
            this.chkWordsTooltip.Name = "chkWordsTooltip";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.chkAccent);
            this.groupBox4.Controls.Add(this.chkAE);
            this.groupBox4.Controls.Add(this.chkUV);
            this.groupBox4.Controls.Add(this.chkIJ);
            this.groupBox4.Controls.Add(this.radRules);
            this.groupBox4.Controls.Add(this.radClassical);
            this.groupBox4.Controls.Add(this.radPrinted);
            this.groupBox4.Controls.Add(this.radMissals);
            resources.ApplyResources(this.groupBox4, "groupBox4");
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.TabStop = false;
            // 
            // chkAccent
            // 
            resources.ApplyResources(this.chkAccent, "chkAccent");
            this.chkAccent.Name = "chkAccent";
            // 
            // chkAE
            // 
            resources.ApplyResources(this.chkAE, "chkAE");
            this.chkAE.Name = "chkAE";
            // 
            // chkUV
            // 
            resources.ApplyResources(this.chkUV, "chkUV");
            this.chkUV.Name = "chkUV";
            this.chkUV.CheckedChanged += new System.EventHandler(this.chkUV_CheckedChanged);
            // 
            // chkIJ
            // 
            resources.ApplyResources(this.chkIJ, "chkIJ");
            this.chkIJ.Name = "chkIJ";
            // 
            // radRules
            // 
            resources.ApplyResources(this.radRules, "radRules");
            this.radRules.Name = "radRules";
            this.radRules.CheckedChanged += new System.EventHandler(this.radRules_CheckedChanged);
            // 
            // radClassical
            // 
            resources.ApplyResources(this.radClassical, "radClassical");
            this.radClassical.Name = "radClassical";
            this.radClassical.CheckedChanged += new System.EventHandler(this.radClassical_CheckedChanged);
            // 
            // radPrinted
            // 
            resources.ApplyResources(this.radPrinted, "radPrinted");
            this.radPrinted.Name = "radPrinted";
            this.radPrinted.CheckedChanged += new System.EventHandler(this.radPrinted_CheckedChanged);
            // 
            // radMissals
            // 
            resources.ApplyResources(this.radMissals, "radMissals");
            this.radMissals.Name = "radMissals";
            this.radMissals.CheckedChanged += new System.EventHandler(this.radMissals_CheckedChanged);
            // 
            // tabGeneral
            // 
            this.tabGeneral.Controls.Add(this.chkInternet);
            this.tabGeneral.Controls.Add(this.groupBox5);
            this.tabGeneral.Controls.Add(this.groupBox3);
            this.tabGeneral.Controls.Add(this.chkParallelScroll);
            resources.ApplyResources(this.tabGeneral, "tabGeneral");
            this.tabGeneral.Name = "tabGeneral";
            // 
            // chkInternet
            // 
            resources.ApplyResources(this.chkInternet, "chkInternet");
            this.chkInternet.Name = "chkInternet";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.chkSaveSearch);
            this.groupBox5.Controls.Add(this.chkStoreSearch);
            resources.ApplyResources(this.groupBox5, "groupBox5");
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.TabStop = false;
            // 
            // chkSaveSearch
            // 
            resources.ApplyResources(this.chkSaveSearch, "chkSaveSearch");
            this.chkSaveSearch.Name = "chkSaveSearch";
            // 
            // chkStoreSearch
            // 
            this.chkStoreSearch.Checked = true;
            this.chkStoreSearch.CheckState = System.Windows.Forms.CheckState.Checked;
            resources.ApplyResources(this.chkStoreSearch, "chkStoreSearch");
            this.chkStoreSearch.Name = "chkStoreSearch";
            this.chkStoreSearch.CheckedChanged += new System.EventHandler(this.chkStoreSearch_CheckedChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.nudHistory);
            this.groupBox3.Controls.Add(this.chkSaveHistory);
            this.groupBox3.Controls.Add(this.label2);
            resources.ApplyResources(this.groupBox3, "groupBox3");
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.TabStop = false;
            // 
            // nudHistory
            // 
            this.nudHistory.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            resources.ApplyResources(this.nudHistory, "nudHistory");
            this.nudHistory.Maximum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.nudHistory.Name = "nudHistory";
            // 
            // chkSaveHistory
            // 
            resources.ApplyResources(this.chkSaveHistory, "chkSaveHistory");
            this.chkSaveHistory.Name = "chkSaveHistory";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // chkParallelScroll
            // 
            resources.ApplyResources(this.chkParallelScroll, "chkParallelScroll");
            this.chkParallelScroll.Name = "chkParallelScroll";
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            resources.ApplyResources(this.btnCancel, "btnCancel");
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnApply
            // 
            resources.ApplyResources(this.btnApply, "btnApply");
            this.btnApply.Name = "btnApply";
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // fontMain
            // 
            this.fontMain.AllowScriptChange = false;
            this.fontMain.ShowApply = true;
            this.fontMain.ShowEffects = false;
            this.fontMain.Apply += new System.EventHandler(this.FontApply);
            // 
            // fontNumbers
            // 
            this.fontNumbers.AllowScriptChange = false;
            this.fontNumbers.ShowApply = true;
            this.fontNumbers.ShowEffects = false;
            this.fontNumbers.Apply += new System.EventHandler(this.FontApply);
            // 
            // OptionsForm
            // 
            this.AcceptButton = this.btnOK;
            resources.ApplyResources(this, "$this");
            this.CancelButton = this.btnCancel;
            this.ControlBox = false;
            this.Controls.Add(this.btnApply);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.tabControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "OptionsForm";
            this.ShowInTaskbar = false;
            this.tabControl.ResumeLayout(false);
            this.tabFormat.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.tabLatin.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.tabGeneral.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.nudHistory)).EndInit();
            this.ResumeLayout(false);
        }

		#endregion

		private void btnOK_Click(object sender, System.EventArgs e)
		{
			try {
			btnApply_Click(null,null);
			this.Hide();
		} catch {}
		}

		private void btnChangeMainText_Click(object sender, System.EventArgs e)
		{
			try {
			fontMain.ShowDialog();
			lblMainText.Text=UpdateFontDesc(fontMain.Font);
		} catch {}
		}

		private void FontApply(object sender, System.EventArgs e)
		{
			try {
			Startup.MainForm.btnGo_Click();
		} catch {}
		}

		private void btnChangeVerseNumbers_Click(object sender, System.EventArgs e)
		{
			try {
			fontNumbers.ShowDialog();
			lblVerseNumbers.Text=UpdateFontDesc(fontNumbers.Font);
		} catch {}
		}

		public void UpdateFontDesc()
		{
			try {
			lblMainText.Text=UpdateFontDesc(fontMain.Font);
			lblVerseNumbers.Text=UpdateFontDesc(fontNumbers.Font);
		} catch {}
		}

		private void tabControl_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			try {
			tabControl.SelectedTab.BringToFront();
		} catch {}
		}

		private void chkUV_CheckedChanged(object sender, System.EventArgs e)
		{
			try {
		
		} catch {}
		}

		private void chkStoreSearch_CheckedChanged(object sender, System.EventArgs e)
		{
			try {
			if (!chkStoreSearch.Checked)
			{
				chkSaveSearch.Checked=false;
				chkSaveSearch.Enabled=false;
			}
			else
			{
				chkSaveSearch.Enabled=true;
			}
		
		} catch {}
		}

		private void chkNoWords_CheckedChanged(object sender, System.EventArgs e)
		{
			try {
			if (chkNoWords.Checked)
			{
				chkWordsTooltip.Checked=false;
				chkWordsTooltip.Enabled=false;
			}
			else
			{
				chkWordsTooltip.Enabled=true;
			}
		} catch {}
		}

		private void radRules_CheckedChanged(object sender, System.EventArgs e)
		{
			try {
			if (radRules.Checked)
			{

				chkAE.Enabled=true;
				chkUV.Enabled=true;
				chkIJ.Enabled=true;
				chkAccent.Enabled=true;
			}
		} catch {}
		}

		private void radClassical_CheckedChanged(object sender, System.EventArgs e)
		{
			try {
			if (radClassical.Checked)
			{
				chkAE.Checked=false;
				chkIJ.Checked=false;
				chkUV.Checked=false;
				chkAccent.Checked=false;
				chkAE.Enabled=false;
				chkUV.Enabled=false;
				chkIJ.Enabled=false;
				chkAccent.Enabled=false;
			}
		} catch {}
		}

		private void radPrinted_CheckedChanged(object sender, System.EventArgs e)
		{
			try {
			if (radPrinted.Checked)
			{
				chkAE.Checked=false;
				chkIJ.Checked=false;
				chkUV.Checked=true;
				chkAccent.Checked=false;
				chkAE.Enabled=false;
				chkUV.Enabled=false;
				chkIJ.Enabled=false;
				chkAccent.Enabled=false;
			}
		} catch {}
		}

		private void radMissals_CheckedChanged(object sender, System.EventArgs e)
		{
			try {
			if (radMissals.Checked)
			{
				chkAccent.Checked=true;
				chkAE.Checked=true;
				chkIJ.Checked=true;
				chkUV.Checked=true;
				chkAE.Enabled=false;
				chkUV.Enabled=false;
				chkIJ.Enabled=false;
				chkAccent.Enabled=false;
			}
		} catch {}
		}

		private void chkVerseSuper_CheckedChanged(object sender, System.EventArgs e)
		{
			try {
			fontNumbers.Font = new Font(fontNumbers.Font.Name,
				chkVerseSuper.Checked ? 8 :	fontMain.Font.Size,
				fontNumbers.Font.Style);
			UpdateFontDesc();
		} catch {}
		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			try {
			// to do
		} catch {}
		}

		private void btnApply_Click(object sender, System.EventArgs e)
		{
			try {
			while (Startup.HistoryForm.HistoryBox.Items.Count > (int) nudHistory.Value)
			{
				Startup.HistoryForm.HistoryBox.Items.RemoveAt(Startup.HistoryForm.HistoryBox.Items.Count-1);
			}
			if (!chkStoreSearch.Checked)
				Startup.SearchForm.cmbSearch.Items.Clear();
			Startup.MainForm.btnGo_Click();
			//if (chkFrench.Checked) System.Threading.Thread.CurrentThread.CurrentUICulture=System.Globalization.CultureInfo.CreateSpecificCulture("fr");
			//if (chkEnglish.Checked)
                System.Threading.Thread.CurrentThread.CurrentUICulture=System.Globalization.CultureInfo.CreateSpecificCulture("");

		} catch {}
		}

		private void radVerseNewLine_CheckedChanged(object sender, System.EventArgs e)
		{
			try {
			chkPoetry.Enabled = !(radVerseNewLine.Checked);
			chkVerseSuper.Checked= !(radVerseNewLine.Checked);
		} catch {}
		}


		public string UpdateFontDesc(Font f)
		{
			try {
			string s="";
			switch (f.Style)
			{
				case (FontStyle.Bold):
				{
					s=" (Bold)";
					break;
				}
				case (FontStyle.Italic):
				{
					s=" (Italic)";
					break;
				}
			}
			return f.Name + s + " " + f.SizeInPoints.ToString() + " pt";
            }
            catch { return f.Name;  }
		}
	}
}
