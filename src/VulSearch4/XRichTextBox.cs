using System;
using System.Windows.Forms;

namespace VulSearch4
{
/// <summary>
/// This class fires the VScroll event when the scroll bar in the RTF
/// box is merely dragged, rather than clicked
/// </summary>
	public class XRichTextBox : RichTextBox
	{
		public XRichTextBox() : base()
		{
		}

		protected override void WndProc(ref Message m)
		{
			try {
			base.WndProc (ref m);
			if (m.Msg== 0x115) // WM_VSCROLL
			{
				OnVScroll(null);
			}
		} catch {}
		}
	}
}
