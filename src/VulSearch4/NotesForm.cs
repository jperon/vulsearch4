﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace VulSearch4
{
    public partial class NotesForm : Form
    {
        public NotesForm()
        {
		try {
            InitializeComponent();
	} catch {}
        }

        private void NotesForm_VisibleChanged(object sender, EventArgs e)
        {
		try {
            Startup.MainForm.toolNotes.Checked = this.Visible;
	} catch {}
        }

        private void NotesForm_FormClosing(object sender, FormClosingEventArgs e)
        {
		try {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                this.Hide();
                e.Cancel = true;
            }
	} catch {}
        }

        private void NotesBox_TextChanged(object sender, EventArgs e)
        {
		try {
            Startup.MainForm.NotesBox_TextChanged(sender, e);
	} catch {}
        }

    }
}
