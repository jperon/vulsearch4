﻿namespace VulSearch4
{
    partial class ResultsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ResultsBox = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // ResultsBox
            // 
            this.ResultsBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ResultsBox.FormattingEnabled = true;
            this.ResultsBox.Location = new System.Drawing.Point(0, 0);
            this.ResultsBox.Name = "ResultsBox";
            this.ResultsBox.Size = new System.Drawing.Size(284, 261);
            this.ResultsBox.TabIndex = 0;
            this.ResultsBox.DoubleClick += new System.EventHandler(this.ResultsBox_DoubleClick);
            this.ResultsBox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ResultsBox_MouseDown);
            this.ResultsBox.MouseMove += new System.Windows.Forms.MouseEventHandler(this.ResultsBox_MouseMove);
            this.ResultsBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ResultsBox_MouseUp);
            // 
            // ResultsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.ResultsBox);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "ResultsForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Search results";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ResultsForm_FormClosing);
            this.VisibleChanged += new System.EventHandler(this.ResultsForm_VisibleChanged);
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.ListBox ResultsBox;


    }
}