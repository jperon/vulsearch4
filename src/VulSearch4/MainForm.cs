﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.IO;
using System.Text.RegularExpressions;
using System.Diagnostics;

namespace VulSearch4
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class MainForm : System.Windows.Forms.Form
	{
		private System.ComponentModel.IContainer components;
		public System.Windows.Forms.ComboBox cmbRight;
		internal System.Windows.Forms.ComboBox cmbVerse;
		internal System.Windows.Forms.ComboBox cmbChapter;
		internal System.Windows.Forms.ComboBox cmbBook;

		private string _rold="";

        private string leftBible, rightBible;
		private VulSearch4.XRichTextBox rtbRight;

		private int leftX, leftY;
		public System.Windows.Forms.ToolTip toolTipWords;
		private int prevVerse=0;
		private int lastBook, lastChapter, lastVerse;
		private System.Windows.Forms.Panel panel;

        private int timer_hack_count = 0;

		private Rectangle dragBox;
		private int indexToDrag;

		public ListView XRefBox;
		private XRichTextBox hoverCurrent;
		private string oldRtf; //last word we ran Words on

		private bool FindInRight;

		private bool suspendJump=false, suspendParallel=false;
		private System.Windows.Forms.Button btnPrev;
		private System.Windows.Forms.Timer tmrHover;
		private System.Windows.Forms.Button btnNext; // this is set to true
		//temporarily to stop repeatedly redrawing the texts
		//when book, chap & verse are set in quick succession.

		public System.Windows.Forms.ImageList imgList;
		private System.Windows.Forms.OpenFileDialog fileSel;
		public System.Windows.Forms.ComboBox cmbLeft;
        private System.Windows.Forms.OpenFileDialog importSel;
        private ToolStrip toolbarMain;
        private MenuStrip menuStrip1;
        public ToolStripButton toolWords;
        public ToolStripButton toolSearch;
        public ToolStripButton toolSearchResults;
        public ToolStripButton toolBookmarks;
        public ToolStripButton toolXrefs;
        public ToolStripButton toolNotes;
        private ToolStripSeparator toolStripSeparator1;
        public ToolStripButton toolLock;
        private ToolStripMenuItem menuFile;
        private ToolStripMenuItem menuFileUpdate;
        private ToolStripMenuItem menuFileImportXrefs;
        private ToolStripMenuItem menuView;
        public ToolStripButton toolHistory;
        private ToolStripMenuItem menuFileExportXrefs;
        private ToolStripMenuItem menuFileExit;
        private ToolStripMenuItem menuBibles;
        private ToolStripMenuItem menuHelp;
        private ToolStripSeparator toolStripSeparator2;
        private ToolStripSeparator toolStripSeparator4;
        private ToolStripMenuItem menuExit;
        private ToolStripMenuItem menuHelpHelp;
        private ToolStripSeparator toolStripSeparator5;
        private ToolStripMenuItem menuHelpAbout;
        private ToolStripMenuItem menuFindInPage;
        private ToolStripSeparator toolStripSeparator7;
        private ToolStripMenuItem menuOptions;
        private ToolStripMenuItem menuInfoBibles;
        private ToolStripSeparator toolStripSeparator6;
        private ToolStripMenuItem menuAddBible;
        private ContextMenuStrip menuRTB;
        private ToolStripMenuItem menuRTBCopy;
        private ToolStripSeparator toolStripSeparator8;
        private ToolStripMenuItem menuRTBWords;
        private ToolStripMenuItem menuTroubleshooting;
        private ToolStripMenuItem troubleshootVulSearchProblemsToolStripMenuItem;
        private ToolStripMenuItem menuFileShowConfigFiles;
        private ToolStripSeparator toolStripSeparator3;
        private ToolStripMenuItem menuMakeSensible;
        public XRichTextBox rtbLeft;
        private Timer tmrHack;

		//store positions and references in the refs box
		public ArrayList DisplayRefs;



		public void StartSearch(int BibleToSearch, string SearchText)
		{
			try
			{
				Process swish = new Process();
#if DEBUG
				swish.StartInfo.WorkingDirectory = AppPath() + "\\src\\Swish";
				swish.StartInfo.FileName = AppPath() + "\\src\\Swish\\swish-e" ;
#else
				swish.StartInfo.WorkingDirectory = AppPath() + "\\Swish";
				swish.StartInfo.FileName = AppPath() + "\\Swish\\swish-e" ;
#endif
				swish.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden ;
				swish.StartInfo.CreateNoWindow=true;
				swish.StartInfo.Arguments = "-f " + 
					(Startup.Bibles[BibleToSearch] as Bible).Extension 
					+ ".idx -s swishdocpath -P ' -w \"" + SearchText.Trim() +"\"" ;
				swish.StartInfo.UseShellExecute = false;
				swish.StartInfo.RedirectStandardOutput = true;
				swish.Start();
				string output = swish.StandardOutput.ReadToEnd();
				swish.WaitForExit();

				Startup.ResultsForm.ResultsBox.Items.Clear();

				foreach (string s in output.Split('\n'))
				{
					string t=s.Trim();
					if (t==".")
					{
						break;
					}

					if (t.Substring(0,1) != "#")
					{
						if (t.Substring(0,3)=="err")
						{
							break;
						}
						else
						{
							t=t.Substring(t.IndexOf("text")+5);
							int book=int.Parse(t.Substring(0,3));
							int chap=int.Parse(t.Substring(4,3));
							int verse=int.Parse(t.Substring(8,3));
							Startup.ResultsForm.ResultsBox.Items.Add(Bible.Abbrev[book-1] + " " +
								chap.ToString()+":"+verse.ToString());
						}
					}
				}

				Startup.ResultsForm.Text="(" + Startup.ResultsForm.ResultsBox.Items.Count.ToString()
					+ ") " + Startup.rm.GetString("results");
				if (Startup.ResultsForm.ResultsBox.Items.Count==0)
				{

					Startup.ResultsForm.ResultsBox.Items.Add("(No results found)");
				}
                Startup.ResultsForm.Show();

			}
			catch (Exception e)
			{
				System.Windows.Forms.MessageBox.Show(
					Startup.rm.GetString("searcherror") + e.Message,
					"VulSearch 4");
			}

		}



		public int LastBook
		{
			get
			{
				return cmbBook.SelectedIndex;
			}
			set
			{
				lastBook=value;
			}
		}

		public int LastChapter
		{
			get
			{
				return cmbChapter.SelectedIndex;
			}
			set
			{
				lastChapter=value;
			}
		}
			
		public int LastVerse
		{
			get
			{
				return cmbVerse.SelectedIndex;
			}
			set
			{
				lastVerse=value;
			}
		}
			


		public string LeftBible
		{
			get
			{
				return leftBible;
			}
			set
			{
				leftBible=value;
				try
				{
					cmbLeft.SelectedIndex=Startup.IndexFromExt(value);
				}
				catch
				{
					// haven't filled cmbLeft yet - no prob!
				}

			}
		}

		public string RightBible
		{
			get
			{
				return rightBible;
			}
			set
			{
				rightBible=value;
				try
				{
					cmbRight.SelectedIndex=Startup.IndexFromExt(value);
				}
				catch
				{
					// haven't filled cmbRight yet - no prob!
				}

			}
		}

		public MainForm()
		{
			try {
			oldRtf="";
			DisplayRefs=new ArrayList();
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

		} catch {}
		}

		private void OnExitClick(object sender, EventArgs e)
		{
			try {
			this.Close();
		} catch {}
		}

		private int verseFromLine(string q)
		{
			try {
			int vq=q.IndexOf(":");
			return int.Parse(q.Substring(vq+1,q.IndexOf(" ",vq)-vq-1));
            }
            catch { return 1;  }
		}

		private int chapFromLine(string q)
		{
			try {
			return int.Parse(q.Substring(0,q.IndexOf(":")));
		} catch { return 1; }
		}

		private void OnImportClick(object sender, EventArgs e)
		{
			try {
			importSel.Title=Startup.rm.GetString("importrefs");
			importSel.CheckFileExists=true;
			importSel.ShowDialog();
			if (importSel.FileName=="") return;
			Startup.ImportRefs(importSel.FileName);
			RedisplayRefs();
		} catch {}
		}

		private void OnExportClick(object sender, EventArgs e)
		{
			try {
			importSel.Title=Startup.rm.GetString("exportrefs");
			importSel.CheckFileExists=false;
			importSel.ShowDialog();
			if (importSel.FileName=="") return;
			Startup.ExportRefs(importSel.FileName);
		} catch {}
		}

		private void OnShowVSFilesClick(object sender, EventArgs e)
		{
			try {
			Process.Start(Startup.DataPath);
		} catch {}
		}

		private void OnUpdateClick(object sender, EventArgs e)
		{
			try {
			UpdateTexts(false);
		} catch {}
		}

		public void UpdateTexts(bool quiet)
			{
			Stream dummy=new MemoryStream(); // compiler worries variables might be uninitialized - this will never be the case as long as update file is in correct format. If it isn't, we'll catch the error anyway.
			StreamReader i=new StreamReader(dummy),j=new StreamReader(dummy);
			StreamWriter k=new StreamWriter(dummy);
			UpdateForm f=new UpdateForm();
			FileStream file;
			try
			{
				string p=null,q=null,ext=null,book=null;
				bool jopen=false;
				int cp=0,cq=0,vp=0,vq=0;
				bool goon=false;
				if(Startup.OptionsForm.chkInternet.Checked)
				{
					f.label1.Text=Startup.rm.GetString("downloading");
					f.Show();
					Application.DoEvents();
					System.Net.WebClient myClient = new System.Net.WebClient();
					i=new StreamReader(myClient.OpenRead("http://vulsearch.sourceforge.net/update.txt"), System.Text.Encoding.GetEncoding(1252));
				}
				else
				{
					fileSel.ShowDialog();
					if (fileSel.FileName=="") return;
					file = new FileStream(fileSel.FileName, FileMode.Open, FileAccess.Read);
					i = new StreamReader(file, System.Text.Encoding.GetEncoding(1252));
					f.Show();
				}
				Application.DoEvents();

				do
				{
					p=i.ReadLine();
					if (p.Substring(0,1)!="#")
					{
						if (p.Substring(0,1)=="@" || p.Substring(0,1)=="*")
						{
							if (jopen)
							{
								while (q!=null)
								{
									k.WriteLine(q);
									q=j.ReadLine();
								}
								k.Close();
								j.Close();
								jopen=false;
							}
							if (p=="@") break;
							if (p.Substring(0,1)=="*")
							{
								ext=p.Substring(1);
							}
							else
							{
								book=p.Substring(1);
								if (File.Exists(Startup.DataPath+"\\Text\\"+book+"."+ext))
								{
									f.label1.Text="(" + ext + ") " + book;
									Application.DoEvents();
									File.Copy(Startup.DataPath+"\\Text\\"+book+"."+ext,Startup.DataPath+"\\tmp",true);
									file = new FileStream(Startup.DataPath+"\\tmp", FileMode.Open, FileAccess.Read);
									j = new StreamReader(file, System.Text.Encoding.GetEncoding(1252));
									file = new FileStream(Startup.DataPath+"\\Text\\"+book+"."+ext, FileMode.Truncate, FileAccess.Write);
									k = new StreamWriter(file, System.Text.Encoding.GetEncoding(1252));
									jopen=true;
									q=j.ReadLine();
									vq=verseFromLine(q);
									cq=chapFromLine(q);
								}
							}
						}
						else if (jopen)
						{
							vp=verseFromLine(p);
							cp=chapFromLine(p);
							/* ok, I've got my verse to apply, and I'm going to apply it before
							reading anything else from p
								cases:
								(1) p<q. Then apply p and read in more from p
								(2) p=q. Then apply p, read next q, then go on to next p
								(3) p>q. Then we're not at the place where p goes yet -
									write q, read more q, loop back
									*/
					
							do
							{
								if (cp<cq || (cp==cq && vp<vq))
								{
									//case (1)
									if (p.Substring(p.IndexOf(" ")) != " X")
										k.WriteLine(p);
									break;
								}
								else
								{
									goon=false;
									if (cp==cq && vp==vq)
									{
										//case (2)
										if (p.Substring(p.IndexOf(" ")) != " X")
											k.WriteLine(p);
										goon=true;
									}
									else
									{
										//case (3)
										k.WriteLine(q);
									}
									//cases (2) and (3)
									if (q!=null)
									{
										q=j.ReadLine();
										if (q!=null)
										{
											vq=verseFromLine(q);
											cq=chapFromLine(q);
										}
										else
										{
											cq=1000;
										}
										if (goon) break;
									}
									else
									{
										cq=1000;
									}
								}
							} while (true);
						}
					}
				} while (true);
				i.Close();
				j.Close();
				k.Close();
				//MessageBox.Show(Startup.rm.GetString("updateok"),"VulSearch");
				try {
					System.Net.WebClient myClient = new System.Net.WebClient();
					StreamReader ii=new StreamReader(myClient.OpenRead("http://vulsearch.sourceforge.net/latest.txt"), System.Text.Encoding.ASCII);
					Startup.CurTxtVer = int.Parse(ii.ReadLine().Trim());
					ii.Close();
				}
				catch {
				}

				f.label1.Text=Startup.rm.GetString("updateok");
				f.button1.Enabled=true;
				if(quiet)
					f.Hide();
			}

			catch (Exception ee)
			{
				MessageBox.Show(Startup.rm.GetString("updateerr")+"\n"+ee.Message,"VulSearch");
				try
				{
					f.Hide();
					i.Close();
					j.Close();
					k.Close();
				}
				catch
				{
					//
				}
			}
		}


		private void OnHelpClick(object sender, EventArgs e)
		{
			try
			{
#if DEBUG
				Help.ShowHelp(this,AppPath() + "\\misc\\VulSearch4.chm",HelpNavigator.TableOfContents);
#else
				Help.ShowHelp(this,AppPath() + "\\VulSearch4.chm",HelpNavigator.TableOfContents);
#endif
		
			}
			catch
			{
				// missing help file?
			}
		}

		private void OnBibleInfoClick(object sender, EventArgs e)
		{
			try {
			BibleInfoForm f=new BibleInfoForm();
			f.ShowDialog();
			f=null;
		} catch {}
		}

		private void OnAddBibleClick(object sender, EventArgs e)
		{
			try {
			AddBibleForm f=new AddBibleForm();
			f.ShowDialog();
			if (!f.WasCancelled)
			{
				MakeIndexForm g=new MakeIndexForm(f.txtPath.Text,
					(string) f.cmbExt.SelectedItem,f.radLat.Checked,f.txtDesc.Text,f.txtName.Text);
				g.ShowDialog();
				g=null;
			}
			f=null;
		} catch {}
		}

		private void OnOptionsClick(object sender, EventArgs e)
		{
			try {
			Startup.OptionsForm.ShowDialog();
			btnGo_Click();
		} catch {}
		}

		private void OnAboutClick(object sender, EventArgs e)
		{
			try {
			AboutForm f=new AboutForm();
			f.ShowDialog();
			f=null;
		} catch {}
		}


		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			try {
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		} catch {}
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.panel = new System.Windows.Forms.Panel();
            this.toolbarMain = new System.Windows.Forms.ToolStrip();
            this.toolWords = new System.Windows.Forms.ToolStripButton();
            this.toolSearch = new System.Windows.Forms.ToolStripButton();
            this.toolSearchResults = new System.Windows.Forms.ToolStripButton();
            this.toolHistory = new System.Windows.Forms.ToolStripButton();
            this.toolBookmarks = new System.Windows.Forms.ToolStripButton();
            this.toolXrefs = new System.Windows.Forms.ToolStripButton();
            this.toolNotes = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolLock = new System.Windows.Forms.ToolStripButton();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menuFile = new System.Windows.Forms.ToolStripMenuItem();
            this.menuFileUpdate = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.menuFileImportXrefs = new System.Windows.Forms.ToolStripMenuItem();
            this.menuFileExportXrefs = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.menuExit = new System.Windows.Forms.ToolStripMenuItem();
            this.menuView = new System.Windows.Forms.ToolStripMenuItem();
            this.menuFindInPage = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.menuOptions = new System.Windows.Forms.ToolStripMenuItem();
            this.menuBibles = new System.Windows.Forms.ToolStripMenuItem();
            this.menuInfoBibles = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.menuAddBible = new System.Windows.Forms.ToolStripMenuItem();
            this.menuTroubleshooting = new System.Windows.Forms.ToolStripMenuItem();
            this.menuFileShowConfigFiles = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.menuMakeSensible = new System.Windows.Forms.ToolStripMenuItem();
            this.troubleshootVulSearchProblemsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuHelp = new System.Windows.Forms.ToolStripMenuItem();
            this.menuHelpHelp = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.menuHelpAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.cmbVerse = new System.Windows.Forms.ComboBox();
            this.cmbChapter = new System.Windows.Forms.ComboBox();
            this.cmbBook = new System.Windows.Forms.ComboBox();
            this.cmbRight = new System.Windows.Forms.ComboBox();
            this.btnNext = new System.Windows.Forms.Button();
            this.btnPrev = new System.Windows.Forms.Button();
            this.cmbLeft = new System.Windows.Forms.ComboBox();
            this.menuRTB = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.menuRTBCopy = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.menuRTBWords = new System.Windows.Forms.ToolStripMenuItem();
            this.toolTipWords = new System.Windows.Forms.ToolTip(this.components);
            this.tmrHover = new System.Windows.Forms.Timer(this.components);
            this.imgList = new System.Windows.Forms.ImageList(this.components);
            this.fileSel = new System.Windows.Forms.OpenFileDialog();
            this.importSel = new System.Windows.Forms.OpenFileDialog();
            this.menuFileExit = new System.Windows.Forms.ToolStripMenuItem();
            this.tmrHack = new System.Windows.Forms.Timer(this.components);
            this.rtbRight = new VulSearch4.XRichTextBox();
            this.rtbLeft = new VulSearch4.XRichTextBox();
            this.panel.SuspendLayout();
            this.toolbarMain.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.menuRTB.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel
            // 
            resources.ApplyResources(this.panel, "panel");
            this.panel.Controls.Add(this.toolbarMain);
            this.panel.Controls.Add(this.menuStrip1);
            this.panel.Controls.Add(this.cmbVerse);
            this.panel.Controls.Add(this.cmbChapter);
            this.panel.Controls.Add(this.cmbBook);
            this.panel.Controls.Add(this.cmbRight);
            this.panel.Controls.Add(this.btnNext);
            this.panel.Controls.Add(this.btnPrev);
            this.panel.Controls.Add(this.cmbLeft);
            this.panel.Name = "panel";
            this.panel.Layout += new System.Windows.Forms.LayoutEventHandler(this.panel_Layout);
            // 
            // toolbarMain
            // 
            this.toolbarMain.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolbarMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolWords,
            this.toolSearch,
            this.toolSearchResults,
            this.toolHistory,
            this.toolBookmarks,
            this.toolXrefs,
            this.toolNotes,
            this.toolStripSeparator1,
            this.toolLock});
            resources.ApplyResources(this.toolbarMain, "toolbarMain");
            this.toolbarMain.Name = "toolbarMain";
            // 
            // toolWords
            // 
            this.toolWords.CheckOnClick = true;
            this.toolWords.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.toolWords, "toolWords");
            this.toolWords.Name = "toolWords";
            this.toolWords.CheckStateChanged += new System.EventHandler(this.toolWords_CheckStateChanged);
            // 
            // toolSearch
            // 
            this.toolSearch.CheckOnClick = true;
            this.toolSearch.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.toolSearch, "toolSearch");
            this.toolSearch.Name = "toolSearch";
            this.toolSearch.CheckStateChanged += new System.EventHandler(this.toolSearch_CheckStateChanged);
            // 
            // toolSearchResults
            // 
            this.toolSearchResults.CheckOnClick = true;
            this.toolSearchResults.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.toolSearchResults, "toolSearchResults");
            this.toolSearchResults.Name = "toolSearchResults";
            this.toolSearchResults.CheckStateChanged += new System.EventHandler(this.toolSearchResults_CheckStateChanged);
            // 
            // toolHistory
            // 
            this.toolHistory.CheckOnClick = true;
            this.toolHistory.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.toolHistory, "toolHistory");
            this.toolHistory.Name = "toolHistory";
            this.toolHistory.CheckStateChanged += new System.EventHandler(this.toolHistory_CheckStateChanged);
            // 
            // toolBookmarks
            // 
            this.toolBookmarks.CheckOnClick = true;
            this.toolBookmarks.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.toolBookmarks, "toolBookmarks");
            this.toolBookmarks.Name = "toolBookmarks";
            this.toolBookmarks.CheckedChanged += new System.EventHandler(this.toolBookmarks_CheckedChanged);
            // 
            // toolXrefs
            // 
            this.toolXrefs.CheckOnClick = true;
            this.toolXrefs.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.toolXrefs, "toolXrefs");
            this.toolXrefs.Name = "toolXrefs";
            this.toolXrefs.CheckStateChanged += new System.EventHandler(this.toolXrefs_CheckStateChanged);
            // 
            // toolNotes
            // 
            this.toolNotes.CheckOnClick = true;
            this.toolNotes.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.toolNotes, "toolNotes");
            this.toolNotes.Name = "toolNotes";
            this.toolNotes.CheckStateChanged += new System.EventHandler(this.toolNotes_CheckStateChanged);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            resources.ApplyResources(this.toolStripSeparator1, "toolStripSeparator1");
            // 
            // toolLock
            // 
            this.toolLock.CheckOnClick = true;
            this.toolLock.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.toolLock, "toolLock");
            this.toolLock.Name = "toolLock";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuFile,
            this.menuView,
            this.menuBibles,
            this.menuTroubleshooting,
            this.menuHelp});
            resources.ApplyResources(this.menuStrip1, "menuStrip1");
            this.menuStrip1.Name = "menuStrip1";
            // 
            // menuFile
            // 
            this.menuFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuFileUpdate,
            this.toolStripSeparator2,
            this.menuFileImportXrefs,
            this.menuFileExportXrefs,
            this.toolStripSeparator4,
            this.menuExit});
            this.menuFile.Name = "menuFile";
            resources.ApplyResources(this.menuFile, "menuFile");
            // 
            // menuFileUpdate
            // 
            this.menuFileUpdate.Name = "menuFileUpdate";
            resources.ApplyResources(this.menuFileUpdate, "menuFileUpdate");
            this.menuFileUpdate.Click += new System.EventHandler(this.OnUpdateClick);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            resources.ApplyResources(this.toolStripSeparator2, "toolStripSeparator2");
            // 
            // menuFileImportXrefs
            // 
            this.menuFileImportXrefs.Name = "menuFileImportXrefs";
            resources.ApplyResources(this.menuFileImportXrefs, "menuFileImportXrefs");
            this.menuFileImportXrefs.Click += new System.EventHandler(this.OnImportClick);
            // 
            // menuFileExportXrefs
            // 
            this.menuFileExportXrefs.Name = "menuFileExportXrefs";
            resources.ApplyResources(this.menuFileExportXrefs, "menuFileExportXrefs");
            this.menuFileExportXrefs.Click += new System.EventHandler(this.OnExportClick);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            resources.ApplyResources(this.toolStripSeparator4, "toolStripSeparator4");
            // 
            // menuExit
            // 
            this.menuExit.Name = "menuExit";
            resources.ApplyResources(this.menuExit, "menuExit");
            this.menuExit.Click += new System.EventHandler(this.OnExitClick);
            // 
            // menuView
            // 
            this.menuView.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuFindInPage,
            this.toolStripSeparator7,
            this.menuOptions});
            this.menuView.Name = "menuView";
            resources.ApplyResources(this.menuView, "menuView");
            // 
            // menuFindInPage
            // 
            this.menuFindInPage.Name = "menuFindInPage";
            resources.ApplyResources(this.menuFindInPage, "menuFindInPage");
            this.menuFindInPage.Click += new System.EventHandler(this.OnFindInPageClick);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            resources.ApplyResources(this.toolStripSeparator7, "toolStripSeparator7");
            // 
            // menuOptions
            // 
            this.menuOptions.Name = "menuOptions";
            resources.ApplyResources(this.menuOptions, "menuOptions");
            this.menuOptions.Click += new System.EventHandler(this.OnOptionsClick);
            // 
            // menuBibles
            // 
            this.menuBibles.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuInfoBibles,
            this.toolStripSeparator6,
            this.menuAddBible});
            this.menuBibles.Name = "menuBibles";
            resources.ApplyResources(this.menuBibles, "menuBibles");
            // 
            // menuInfoBibles
            // 
            this.menuInfoBibles.Name = "menuInfoBibles";
            resources.ApplyResources(this.menuInfoBibles, "menuInfoBibles");
            this.menuInfoBibles.Click += new System.EventHandler(this.OnBibleInfoClick);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            resources.ApplyResources(this.toolStripSeparator6, "toolStripSeparator6");
            // 
            // menuAddBible
            // 
            this.menuAddBible.Name = "menuAddBible";
            resources.ApplyResources(this.menuAddBible, "menuAddBible");
            this.menuAddBible.Click += new System.EventHandler(this.OnAddBibleClick);
            // 
            // menuTroubleshooting
            // 
            this.menuTroubleshooting.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuFileShowConfigFiles,
            this.toolStripSeparator3,
            this.menuMakeSensible,
            this.troubleshootVulSearchProblemsToolStripMenuItem});
            this.menuTroubleshooting.Name = "menuTroubleshooting";
            resources.ApplyResources(this.menuTroubleshooting, "menuTroubleshooting");
            // 
            // menuFileShowConfigFiles
            // 
            this.menuFileShowConfigFiles.Name = "menuFileShowConfigFiles";
            resources.ApplyResources(this.menuFileShowConfigFiles, "menuFileShowConfigFiles");
            this.menuFileShowConfigFiles.Click += new System.EventHandler(this.OnShowVSFilesClick);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            resources.ApplyResources(this.toolStripSeparator3, "toolStripSeparator3");
            // 
            // menuMakeSensible
            // 
            this.menuMakeSensible.Name = "menuMakeSensible";
            resources.ApplyResources(this.menuMakeSensible, "menuMakeSensible");
            this.menuMakeSensible.Click += new System.EventHandler(this.menuMakeSensible_Click);
            // 
            // troubleshootVulSearchProblemsToolStripMenuItem
            // 
            this.troubleshootVulSearchProblemsToolStripMenuItem.Name = "troubleshootVulSearchProblemsToolStripMenuItem";
            resources.ApplyResources(this.troubleshootVulSearchProblemsToolStripMenuItem, "troubleshootVulSearchProblemsToolStripMenuItem");
            this.troubleshootVulSearchProblemsToolStripMenuItem.Click += new System.EventHandler(this.troubleshootVulSearchProblemsToolStripMenuItem_Click);
            // 
            // menuHelp
            // 
            this.menuHelp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuHelpHelp,
            this.toolStripSeparator5,
            this.menuHelpAbout});
            this.menuHelp.Name = "menuHelp";
            resources.ApplyResources(this.menuHelp, "menuHelp");
            // 
            // menuHelpHelp
            // 
            this.menuHelpHelp.Name = "menuHelpHelp";
            resources.ApplyResources(this.menuHelpHelp, "menuHelpHelp");
            this.menuHelpHelp.Click += new System.EventHandler(this.OnHelpClick);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            resources.ApplyResources(this.toolStripSeparator5, "toolStripSeparator5");
            // 
            // menuHelpAbout
            // 
            this.menuHelpAbout.Name = "menuHelpAbout";
            resources.ApplyResources(this.menuHelpAbout, "menuHelpAbout");
            this.menuHelpAbout.Click += new System.EventHandler(this.OnAboutClick);
            // 
            // cmbVerse
            // 
            this.cmbVerse.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            resources.ApplyResources(this.cmbVerse, "cmbVerse");
            this.cmbVerse.Name = "cmbVerse";
            this.cmbVerse.SelectedIndexChanged += new System.EventHandler(this.cmbVerse_SelectedIndexChanged);
            // 
            // cmbChapter
            // 
            this.cmbChapter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            resources.ApplyResources(this.cmbChapter, "cmbChapter");
            this.cmbChapter.Name = "cmbChapter";
            this.cmbChapter.SelectedIndexChanged += new System.EventHandler(this.cmbChapter_SelectedIndexChanged);
            // 
            // cmbBook
            // 
            this.cmbBook.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            resources.ApplyResources(this.cmbBook, "cmbBook");
            this.cmbBook.Name = "cmbBook";
            this.cmbBook.SelectedIndexChanged += new System.EventHandler(this.cmbBook_SelectedIndexChanged);
            // 
            // cmbRight
            // 
            resources.ApplyResources(this.cmbRight, "cmbRight");
            this.cmbRight.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.cmbRight.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbRight.Name = "cmbRight";
            this.cmbRight.SelectedIndexChanged += new System.EventHandler(this.cmbRight_SelectedIndexChanged);
            // 
            // btnNext
            // 
            resources.ApplyResources(this.btnNext, "btnNext");
            this.btnNext.Name = "btnNext";
            this.toolTipWords.SetToolTip(this.btnNext, resources.GetString("btnNext.ToolTip"));
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // btnPrev
            // 
            resources.ApplyResources(this.btnPrev, "btnPrev");
            this.btnPrev.BackColor = System.Drawing.SystemColors.Control;
            this.btnPrev.Name = "btnPrev";
            this.toolTipWords.SetToolTip(this.btnPrev, resources.GetString("btnPrev.ToolTip"));
            this.btnPrev.UseVisualStyleBackColor = false;
            this.btnPrev.Click += new System.EventHandler(this.btnPrev_Click);
            // 
            // cmbLeft
            // 
            this.cmbLeft.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.cmbLeft.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            resources.ApplyResources(this.cmbLeft, "cmbLeft");
            this.cmbLeft.Name = "cmbLeft";
            this.cmbLeft.SelectedIndexChanged += new System.EventHandler(this.cmbLeft_SelectedIndexChanged);
            // 
            // menuRTB
            // 
            this.menuRTB.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuRTBCopy,
            this.toolStripSeparator8,
            this.menuRTBWords});
            this.menuRTB.Name = "menuRTB";
            resources.ApplyResources(this.menuRTB, "menuRTB");
            // 
            // menuRTBCopy
            // 
            this.menuRTBCopy.Name = "menuRTBCopy";
            resources.ApplyResources(this.menuRTBCopy, "menuRTBCopy");
            this.menuRTBCopy.Click += new System.EventHandler(this.menuRTBCopy_Click);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            resources.ApplyResources(this.toolStripSeparator8, "toolStripSeparator8");
            // 
            // menuRTBWords
            // 
            this.menuRTBWords.Name = "menuRTBWords";
            resources.ApplyResources(this.menuRTBWords, "menuRTBWords");
            this.menuRTBWords.Click += new System.EventHandler(this.menuRTBWords_Click);
            // 
            // toolTipWords
            // 
            this.toolTipWords.AutomaticDelay = 0;
            this.toolTipWords.ShowAlways = true;
            // 
            // tmrHover
            // 
            this.tmrHover.Interval = 500;
            this.tmrHover.Tick += new System.EventHandler(this.tmrHover_Tick);
            // 
            // imgList
            // 
            this.imgList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imgList.ImageStream")));
            this.imgList.TransparentColor = System.Drawing.Color.Transparent;
            this.imgList.Images.SetKeyName(0, "");
            this.imgList.Images.SetKeyName(1, "");
            this.imgList.Images.SetKeyName(2, "");
            this.imgList.Images.SetKeyName(3, "");
            this.imgList.Images.SetKeyName(4, "");
            this.imgList.Images.SetKeyName(5, "");
            // 
            // fileSel
            // 
            this.fileSel.DefaultExt = "txt";
            resources.ApplyResources(this.fileSel, "fileSel");
            // 
            // importSel
            // 
            this.importSel.DefaultExt = "xml";
            resources.ApplyResources(this.importSel, "importSel");
            // 
            // menuFileExit
            // 
            this.menuFileExit.Name = "menuFileExit";
            resources.ApplyResources(this.menuFileExit, "menuFileExit");
            // 
            // tmrHack
            // 
            this.tmrHack.Tick += new System.EventHandler(this.tmrHack_Tick);
            // 
            // rtbRight
            // 
            resources.ApplyResources(this.rtbRight, "rtbRight");
            this.rtbRight.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.rtbRight.ContextMenuStrip = this.menuRTB;
            this.rtbRight.DetectUrls = false;
            this.rtbRight.HideSelection = false;
            this.rtbRight.Name = "rtbRight";
            this.rtbRight.ReadOnly = true;
            this.rtbRight.VScroll += new System.EventHandler(this.rtb_VScroll);
            this.rtbRight.MouseLeave += new System.EventHandler(this.rtb_MouseLeave);
            this.rtbRight.MouseMove += new System.Windows.Forms.MouseEventHandler(this.rtb_MouseMove);
            this.rtbRight.MouseUp += new System.Windows.Forms.MouseEventHandler(this.rtb_MouseUp);
            // 
            // rtbLeft
            // 
            resources.ApplyResources(this.rtbLeft, "rtbLeft");
            this.rtbLeft.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.rtbLeft.ContextMenuStrip = this.menuRTB;
            this.rtbLeft.DetectUrls = false;
            this.rtbLeft.HideSelection = false;
            this.rtbLeft.Name = "rtbLeft";
            this.rtbLeft.ReadOnly = true;
            this.rtbLeft.SizeChanged += new System.EventHandler(this.rtbLeft_SizeChanged);
            this.rtbLeft.MouseLeave += new System.EventHandler(this.rtb_MouseLeave);
            this.rtbLeft.MouseMove += new System.Windows.Forms.MouseEventHandler(this.rtb_MouseMove);
            this.rtbLeft.MouseUp += new System.Windows.Forms.MouseEventHandler(this.rtb_MouseUp);
            // 
            // MainForm
            // 
            resources.ApplyResources(this, "$this");
            this.Controls.Add(this.rtbRight);
            this.Controls.Add(this.rtbLeft);
            this.Controls.Add(this.panel);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Closing += new System.ComponentModel.CancelEventHandler(this.MainForm_Closing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.Move += new System.EventHandler(this.MainForm_Move);
            this.Resize += new System.EventHandler(this.MainForm_Resize);
            this.panel.ResumeLayout(false);
            this.panel.PerformLayout();
            this.toolbarMain.ResumeLayout(false);
            this.toolbarMain.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.menuRTB.ResumeLayout(false);
            this.ResumeLayout(false);

		}
		#endregion


		static public string AppPath()
		{
#if DEBUG
			return @"d:\projects\vulsearch4";
#else
			return Application.StartupPath;
#endif
		}

		private void cmbBook_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			try {
			bool jump=suspendJump;
			suspendJump=true;
			cmbChapter.Items.Clear();
			for (int i=0; i<Bible.NumberOfChapters[cmbBook.SelectedIndex]; i++)
			{
				cmbChapter.Items.Add((i+1).ToString());
			}
			cmbChapter.SelectedIndex=0;
			cmbChapter_SelectedIndexChanged();
			suspendJump=jump;
			btnGo_Click();
		} catch {}
		}

        public void minimize_all_forms()
        {
                Startup.SearchForm.WindowState = FormWindowState.Minimized;
                Startup.BookmarksForm.WindowState = FormWindowState.Minimized;
                Startup.FindInPageForm.WindowState = FormWindowState.Minimized;
                Startup.HistoryForm.WindowState = FormWindowState.Minimized;
                Startup.NotesForm.WindowState = FormWindowState.Minimized;
                Startup.OptionsForm.WindowState = FormWindowState.Minimized;
                Startup.RefsForm.WindowState = FormWindowState.Minimized;
                Startup.ResultsForm.WindowState = FormWindowState.Minimized;
                Startup.SearchBMForm.WindowState = FormWindowState.Minimized;
                Startup.WordsForm.WindowState = FormWindowState.Minimized;

        }

        public void restore_all_forms()
        {
            Startup.SearchForm.WindowState = FormWindowState.Normal;
            Startup.BookmarksForm.WindowState = FormWindowState.Normal;
            Startup.FindInPageForm.WindowState = FormWindowState.Normal;
            Startup.HistoryForm.WindowState = FormWindowState.Normal;
            Startup.NotesForm.WindowState = FormWindowState.Normal;
            Startup.OptionsForm.WindowState = FormWindowState.Normal;
            Startup.RefsForm.WindowState = FormWindowState.Normal;
            Startup.ResultsForm.WindowState = FormWindowState.Normal;
            Startup.SearchBMForm.WindowState = FormWindowState.Normal;
            Startup.WordsForm.WindowState = FormWindowState.Normal;
            //Startup.MainForm.WindowState = FormWindowState.Normal;

            resize_rtb();
        }

        private static void resize_rtb()
        {
            Size s = new Size((Startup.MainForm.panel.Width - 30) / 2, Startup.MainForm.Height - Startup.MainForm.panel.Height - 50);
            Startup.MainForm.ResumeLayout();
            if (Startup.MainForm.rtbLeft.Size != s)
            {
                Startup.MainForm.rtbRight.Size = Startup.MainForm.rtbLeft.Size = s;
                Startup.MainForm.rtbLeft.Location = new Point(10, Startup.MainForm.panel.Bottom);
                Startup.MainForm.rtbRight.Location = new Point(20 + Startup.MainForm.rtbLeft.Width, Startup.MainForm.rtbLeft.Location.Y);
            }
        }

		private void MainForm_Resize(object sender, System.EventArgs e)
		{
			try {
			if (this.WindowState==FormWindowState.Minimized)
			{
                minimize_all_forms();
            }
            else {
                restore_all_forms();
            }
            /*
			rtbLeft.Width=(panel.Width-30)/2;
			rtbRight.Width=rtbLeft.Width;
			rtbRight.Left=panel.Width-10-rtbRight.Width;
             */

		} catch {}
		}

		/// <summary>
		/// When the mouse moves to a new location in one of the
		/// rich text boxes, we start a timer; when this triggers,
		/// we run Words.
		/// </summary>
		/// <remarks>
		/// Kludge because MouseHover only fires
		/// once if the user doesn't go out of the control &
		/// back in.
		/// </remarks>
		private void rtb_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			try {
			if (!(leftX==e.X && leftY==e.Y))
			{
				leftX=e.X;
				leftY=e.Y;
				hoverCurrent=(sender as XRichTextBox);
				tmrHover.Stop();
				tmrHover.Start();
			}
		} catch {}
		}

		private string rtbToBible(ref XRichTextBox rtb)
		{
			if (rtb==rtbLeft)
			{
				return leftBible;
			}
			else
			{
				return rightBible;
			}

		}


		private string CleanUpAe(string s)
		{
            try
            {
                string rtf = s.Replace("æ", "ae");
                rtf = rtf.Replace("Æ", "Ae");
                rtf = rtf.Replace("œ", "oe");
                rtf = rtf.Replace("Œ", "Oe");
                rtf = rtf.Replace("ë", "e");
                rtf = rtf.Replace("á", "a");
                rtf = rtf.Replace("é", "e");
                rtf = rtf.Replace("í", "i");
                rtf = rtf.Replace("ó", "o");
                rtf = rtf.Replace("ú", "u");
                rtf = rtf.Replace("ý", "y");
                rtf = rtf.Replace("Á", "A");
                rtf = rtf.Replace("É", "E");
                rtf = rtf.Replace("Í", "I");
                rtf = rtf.Replace("Ó", "O");
                rtf = rtf.Replace("Ú", "U");
                rtf = rtf.Replace("ǽ", "ae");
                rtf = rtf.Replace("Ǽ", "Ae");
                return rtf;
            }
            catch
            {
                return s;
            }
		}


		private void RunWords(ref XRichTextBox rtb, int x, int y)
		{
			try {
			try
			{
				// extract the word currently being hovered over
				string rtf=rtb.Text;
				int i=rtb.GetCharIndexFromPosition(new System.Drawing.Point(x,y));
				string regstr=@"[ ,.\<\>!;:'\?\\/\[\]\{\}\(\)\""]";
				Regex reg=new Regex(regstr,RegexOptions.RightToLeft);
				Match before = reg.Match(rtf,0,i);
				reg=new Regex(regstr);
				Match after = reg.Match(rtf,i);
				rtf=rtf.Substring(before.Index+1,after.Index-before.Index-1);
				rtf=CleanUpAe(rtf);

				if (oldRtf != rtf && rtf.Trim()!="" )
				{
					// run Words!
					oldRtf=rtf;
					Process words = new Process();
#if DEBUG
					words.StartInfo.WorkingDirectory = MainForm.AppPath() + "\\src\\words" ;
#else
                    words.StartInfo.WorkingDirectory = MainForm.AppPath() + "\\words" ;
#endif
					words.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Normal ;
					words.StartInfo.FileName = words.StartInfo.WorkingDirectory + "\\words_vul" ;
					words.StartInfo.Arguments = rtf ;
					words.StartInfo.CreateNoWindow=true;
					words.StartInfo.UseShellExecute = false;
					words.StartInfo.RedirectStandardOutput = true;
					words.Start();
					rtf=words.StandardOutput.ReadToEnd();
					words.WaitForExit();
					rtf=CleanupWords(rtf);
					if (Startup.OptionsForm.chkWordsTooltip.Checked)
					{
						toolTipWords.SetToolTip(rtb,rtf.TrimEnd().Replace("$n","").Replace("$i","").Replace("$b",""));
					}
					if (Startup.WordsForm.rtbWords.Text.Length>0) Startup.WordsForm.rtbWords.Clear();
					Startup.WordsForm.rtbWords.SelectionFont=new Font(Startup.WordsForm.rtbWords.Font.Name,8, FontStyle.Regular);
					while (rtf.IndexOf("$") != -1)
					{
						if (rtf.Substring(0,rtf.IndexOf("$"))!="")
						{
							Startup.WordsForm.rtbWords.SelectedText=rtf.Substring(0,rtf.IndexOf("$"));
						}
						string style=rtf.Substring(rtf.IndexOf("$")+1,1);
						rtf=rtf.Substring(rtf.IndexOf("$")+2);
						switch (style)
						{
							case "n":
							{
								Startup.WordsForm.rtbWords.SelectionFont=new Font(Startup.WordsForm.rtbWords.Font.Name,8,FontStyle.Regular);
								break;
							}
							case "i":
							{
								Startup.WordsForm.rtbWords.SelectionFont=new Font(Startup.WordsForm.rtbWords.Font.Name,8,FontStyle.Italic);
								break;
							}
							case "b":
							{
								Startup.WordsForm.rtbWords.SelectionFont=new Font(Startup.WordsForm.rtbWords.Font.Name,8,FontStyle.Bold);
								break;
							}
						}
					}
					if (rtf.Length>0) Startup.WordsForm.rtbWords.SelectedText=rtf;
				}
				else
				{
					toolTipWords.SetToolTip(rtb,"");
				}
                Startup.WordsForm.Show();
			}
			catch
			{
				// not much we can do!
			}
		} catch {}
		}


		private void cmbRight_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			try {
			rightBible=(Startup.Bibles[cmbRight.SelectedIndex] as Bible).Extension;
			btnGo_Click();
		} catch {}
		}

		private void cmbLeft_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			try {
			leftBible=(Startup.Bibles[cmbLeft.SelectedIndex] as Bible).Extension;
			btnGo_Click();
		} catch {}
		}

		private void cmbVerse_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			try {
			btnGo_Click();
		} catch {}
		}


		private void MainForm_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			try {
			Startup.WriteSettings();
		} catch {}
		}

		private void MainForm_Load(object sender, System.EventArgs e)
		{
			try {
			suspendJump=true;
			for (int i=0 ; i<73; i++)
			{
				cmbBook.Items.Add(Bible.ShortName[i]);
			}
			for (int i=1; i<=Startup.Bibles.Count; i++)
			{
				cmbLeft.Items.Add((Startup.Bibles[i-1] as Bible).Name);
				cmbRight.Items.Add((Startup.Bibles[i-1] as Bible).Name);
			}
			LeftBible=LeftBible;
			RightBible=RightBible;
			cmbBook.SelectedIndex=lastBook;
			cmbChapter.SelectedIndex=lastChapter;
			cmbVerse.SelectedIndex=lastVerse;

			this.Show();

			suspendJump=false;
			btnGo_Click();
			/*
			 * old xref code
			XRefBox.Columns[0].Width=-1;
			XRefBox.Columns[1].Width=-1;
			if (XRefBox.Columns[0].Width<46) XRefBox.Columns[0].Width=46;
			if (XRefBox.Columns[1].Width<120) XRefBox.Columns[1].Width=120;
			*/
			if (Startup.CLargs != "")
			{
				DoCLargs(Startup.CLargs);
			}

            tmrHack.Enabled = true;
		} catch {}
		}

		private void JumpToVerse(ref XRichTextBox rtb,int Verse)
		{
			try {
			//rtb.HideSelection=true;
			rtb.Focus();
			//rtb.SelectionStart=rtb.Text.Length-1;
			//rtb.SelectionLength=0;
			rtb.SelectionStart=rtb.Text.Length-1;
			rtb.SelectionLength=0;
			//rtb.ScrollToCaret();
			rtb.SelectionStart=rtb.Text.IndexOf((Verse).ToString()+"\u00a0");
			rtb.SelectionLength=0;
			rtb.ScrollToCaret();
			//rtb.HideSelection=false;
			//Startup.SearchForm.Focus();
		} catch {}
		}

		public void btnGo_Click()
		{
			try {
			btnGo_Click(null,null);
		} catch {}
		}

		private void btnGo_Click(object sender, System.EventArgs e)
		{
			try {
			if (suspendJump==false)
			{
				try
				{
					suspendParallel=true;
					toolTipWords.SetToolTip(rtbLeft,"");
					toolTipWords.SetToolTip(rtbRight,"");

					(Startup.Bibles[Startup.IndexFromExt(leftBible)] as Bible)
						.Books[cmbBook.SelectedIndex]
						.Chapters[cmbChapter.SelectedIndex].CreateRTF(
						ref rtbLeft);
					(Startup.Bibles[Startup.IndexFromExt(rightBible)] as Bible)
						.Books[cmbBook.SelectedIndex]
						.Chapters[cmbChapter.SelectedIndex].CreateRTF(
						ref rtbRight);
					JumpToVerse(ref rtbLeft,cmbVerse.SelectedIndex+1);
					JumpToVerse(ref rtbRight,cmbVerse.SelectedIndex+1);
					string toInsert=Bible.Abbrev[cmbBook.SelectedIndex]
						+ " " + (cmbChapter.SelectedIndex+1).ToString()
						+ ":" + (cmbVerse.SelectedIndex+1).ToString();
					Startup.HistoryForm.HistoryBox.Items.Remove(toInsert);
					Startup.HistoryForm.HistoryBox.Items.Insert(0,toInsert);
					suspendParallel=false;
				}
				catch
				{
					// at startup, ignore
				}
			}

            resize_rtb();
		} catch {}
		}

		private void cmbChapter_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			try {
			bool jump=suspendJump;
			suspendJump=true;
			cmbVerse.Items.Clear() ;
			for (int i=0; 
				i<(Startup.Bibles[0] as Bible).Books[cmbBook.SelectedIndex].
				Chapters[cmbChapter.SelectedIndex].NumberOfVerses; i++)
			{
				cmbVerse.Items.Add((i+1).ToString());
			}
			cmbVerse.SelectedIndex=0;
			suspendJump=jump;
			btnGo_Click();
			btnPrev.Enabled=(cmbChapter.SelectedIndex!=0);
			btnNext.Enabled=(cmbChapter.SelectedIndex!= cmbChapter.Items.Count-1);
			Startup.NotesForm.NotesBox.Text=(string) Startup.hashNotes[(Bible.Abbrev[cmbBook.SelectedIndex] +
				" " + (cmbChapter.SelectedIndex+1).ToString())];

			/*
			 * old Xref code
			XRefBox.Items.Clear();
			if (Startup.hashXref[(Bible.Abbrev[cmbBook.SelectedIndex] +
				" " + (cmbChapter.SelectedIndex+1).ToString())] !=null)
			{
				ArrayList a=Startup.hashXref[(Bible.Abbrev[cmbBook.SelectedIndex] +
					" " + (cmbChapter.SelectedIndex+1).ToString())] as
					ArrayList;
				foreach (object o in a)
				{
					XRef x=(XRef) o;
					ListViewItem i=new ListViewItem(x.Ref);
					i.SubItems.Add(x.Desc);
					XRefBox.Items.Add(i);
				}
			}
			*/
			RedisplayRefs();
		} catch {}
		}

		public void RemoveRef(NewRef r)
		{
			try {
			string k=Bible.Abbrev[Startup.MainForm.cmbBook.SelectedIndex] + " " + (Startup.MainForm.cmbChapter.SelectedIndex+1).ToString();
			ArrayList a=new ArrayList();
			if (Startup.hashXref[k] != null)
			{
				a=Startup.hashXref[k] as ArrayList;
				a.Remove(r);
				Startup.hashXref[k]=a;
			}
			RedisplayRefs();
		} catch {}
		}

		public void ChangeRef(NewRef r,string _ref,string desc)
		{
			try {
			string k=Bible.Abbrev[Startup.MainForm.cmbBook.SelectedIndex] + " " + (Startup.MainForm.cmbChapter.SelectedIndex+1).ToString();
			ArrayList a=new ArrayList();
			if (Startup.hashXref[k] != null)
			{
				a=Startup.hashXref[k] as ArrayList;
				a.Remove(r);
				r.Ref=_ref;
				r.Desc=desc;
				a.Add(r);
				Startup.hashXref[k]=a;
			}
			RedisplayRefs();
		} catch {}
		}

		public void RedisplayRefs()
        {
            try { 
			DisplayRefs.Clear();
			bool separate=false;
			if (Startup.MainForm.cmbBook.SelectedIndex==-1) return; // ignore pre-initialization calls
			Regex integer=new Regex("^[0-9]*");
			Match m;
			if (Startup.RefsForm.rtfRefs.Text.Length>0) Startup.RefsForm.rtfRefs.Clear();
			string k=Bible.Abbrev[Startup.MainForm.cmbBook.SelectedIndex] + " " + (Startup.MainForm.cmbChapter.SelectedIndex+1).ToString();
			ArrayList a=new ArrayList();
            if (Startup.hashXref[k] != null)
            {
                separate = true;
                a = Startup.hashXref[k] as ArrayList;
                int[] sortKeys = new int[a.Count];
                string[] rtf = new string[a.Count];
                int[] vv = new int[a.Count];
                NewRef[] tt = new NewRef[a.Count];
                int p = 0;
                RichTextBox rtb = new RichTextBox();
                foreach (Object o in a)
                {
                    NewRef n = ((NewRef)o);
                    if (rtb.Text.Length > 0) rtb.Clear();
                    try
                    {
                        m = integer.Match(n.Verses);

                        vv[p] = int.Parse(m.Value);
                    }
                    catch { vv[p] = 1; }
                    rtb.SelectionFont = new Font("Tahoma", 8, FontStyle.Bold);
                    rtb.SelectionColor = Color.FromArgb(0, 0, 0);
                    rtb.SelectedText = n.Verses + " ";
                    rtb.SelectionFont = new Font("Tahoma", 8, FontStyle.Regular);
                    rtb.SelectionColor = Startup.ColourofType(n.Type);
                    rtb.SelectedText = n.Ref;
                    if (n.Desc.Length > 0)
                        rtb.SelectedText += " " + n.Desc;
                    if (Startup.RefsForm.radVerse.Checked)
                    {
                        sortKeys[p] = 10000 * vv[p] + Startup.IndexofType(n.Type);
                    }
                    else
                    {
                        sortKeys[p] = vv[p] + 10000 * Startup.IndexofType(n.Type);
                    }
                    rtb.SelectAll();
                    rtf[p] = rtb.SelectedRtf;
                    tt[p] = n;
                    p++;
                }
                int[] copyKeys = (int[])sortKeys.Clone();
                Array.Sort(sortKeys, rtf);
                sortKeys = (int[])copyKeys.Clone();
                Array.Sort(sortKeys, tt);
                Array.Sort(copyKeys, vv);
                int prev = -1, ss = 0;
                string prevtype = "";
                for (int i = 0; i < rtf.Length; i++)
                {
                    if (i != 0)
                    {
                        if (Startup.RefsForm.radType.Checked && prevtype != tt[i].Type)
                        {
                            Startup.RefsForm.rtfRefs.SelectedText = "\r\n\r\n";
                            prev = -1;
                        }
                        else
                            Startup.RefsForm.rtfRefs.SelectedText = "  ";
                    }
                    ss = Startup.RefsForm.rtfRefs.Text.Length;
                    Startup.RefsForm.rtfRefs.SelectedRtf = rtf[i];
                    if (prev == vv[i])
                    {
                        String y = Startup.RefsForm.rtfRefs.Text.Substring(ss);
                        Startup.RefsForm.rtfRefs.SelectionStart = ss;
                        Startup.RefsForm.rtfRefs.SelectionLength = y.IndexOf(" ");
                        IDataObject tmp = null;
                        try
                        {
                            tmp = Clipboard.GetDataObject();
                        }
                        catch { }
                        if (Startup.RefsForm.rtfRefs.SelectedText != "")
                        {
                            Startup.RefsForm.rtfRefs.ReadOnly = false; // need to do this to stop the bell
                            // sounding when we cut!
                            Startup.RefsForm.rtfRefs.Cut();
                            Startup.RefsForm.rtfRefs.ReadOnly = true;
                        }
                        try
                        {
                            Clipboard.SetDataObject(tmp, true);
                        }
                        catch { }
                        Startup.RefsForm.rtfRefs.SelectionFont = new Font("Tahoma", 8, FontStyle.Regular);
                        Startup.RefsForm.rtfRefs.SelectedText = "; ";
                        Startup.RefsForm.rtfRefs.SelectionStart = Startup.RefsForm.rtfRefs.Text.Length;
                    }
                    prev = vv[i];
                    prevtype = tt[i].Type;
                    DisplayRefs.Add(new DisplayRef(Startup.RefsForm.rtfRefs.Text.Length, tt[i]));
                }
 
			}

			//now display back references
			string rev=Startup.hashReverseRef[k] as String;
			if (rev != null)
			{
				rev=rev.Substring(1);
				Startup.RefsForm.rtfRefs.SelectionColor=Color.FromArgb(192,192,192);
				if (separate) Startup.RefsForm.rtfRefs.SelectedText="\r\n";
				Startup.RefsForm.rtfRefs.SelectedText="-- \r\n";

				ArrayList SeenRefs=new ArrayList(); // keep track of back-refs so we don't duplicate them
				// when a chapter is targeted more than once by a single source chapter
				a=new ArrayList();
				int total=0;
				foreach(string g in rev.Split(';'))
				{
					if (SeenRefs.Contains(g))
						continue;
					SeenRefs.Add(g);
					if (Startup.hashXref[g] != null)
					{
						foreach (object o in (ArrayList) Startup.hashXref[g])
						{
							NewRef n=((NewRef) o);
							if (n.Ref.StartsWith(k)) //&& n.Ref.IndexOf(":")>0)
								total++;
						}
					}
				}
				int[] sortKeys=new int[total];
				string[] rtf=new string[total];
				int[] vv=new int[total];
				NewRef[] tt=new NewRef[total];
				int p=0;
				RichTextBox rtb=new RichTextBox();

				SeenRefs.Clear();
				foreach(string g in rev.Split(';'))
				{
					if (SeenRefs.Contains(g))
						continue;
					SeenRefs.Add(g);
					if (Startup.hashXref[g] != null)
					{
						a=Startup.hashXref[g] as ArrayList;

						foreach (object o in a)
						{
							NewRef n=((NewRef) o);
							if (n.Ref.StartsWith(k) ) //&& n.Ref.IndexOf(":")>0)
							{
								if (rtb.Text.Length>0) rtb.Clear();
								m=integer.Match(n.Verses);
								if (n.Ref.IndexOf(":")>0 && n.Ref.IndexOf(":")<n.Ref.Length-1)
								{
									string u=n.Ref.Substring(n.Ref.IndexOf(":")+1);
									u="0"+(new Regex("^[0-9]+")).Match(u).Value;
									vv[p]=(int.Parse(u)==0 ? 1 : int.Parse(u));
								}
								else
								{
									vv[p]=1;
								}
								//vv[p]=int.Parse(n.Ref.Substring(n.Ref.IndexOf(":")+1));
								rtb.SelectionFont=new Font("Tahoma",8,FontStyle.Bold);
								rtb.SelectedText=vv[p].ToString()+" ";
								rtb.SelectionFont=new Font("Tahoma",8,FontStyle.Regular);
								rtb.SelectedText=g+":"+int.Parse(m.Value).ToString();
								sortKeys[p]=vv[p];
								rtb.SelectAll();
								rtf[p]=rtb.SelectedRtf;
								tt[p]=new NewRef("BACKREF",g+":"+int.Parse(m.Value).ToString(),"","");
								p++;
							}
						}
					}
				}

				int[] copyKeys=(int[]) sortKeys.Clone();
				Array.Sort(sortKeys,rtf);
				sortKeys=(int[]) copyKeys.Clone();
				Array.Sort(sortKeys,tt);
				Array.Sort(copyKeys,vv);
				int prev=-1, ss=0;
				for (int i=0; i<rtf.Length; i++)
				{
					if (i!=0)
						Startup.RefsForm.rtfRefs.SelectedText="  ";
					ss=Startup.RefsForm.rtfRefs.Text.Length;
					Startup.RefsForm.rtfRefs.SelectedRtf=rtf[i];
					if (prev==vv[i])
					{
						String y=Startup.RefsForm.rtfRefs.Text.Substring(ss);
						Startup.RefsForm.rtfRefs.SelectionStart=ss;
						Startup.RefsForm.rtfRefs.SelectionLength=y.IndexOf(" ");
						IDataObject tmp=null;
						try
						{
							tmp=Clipboard.GetDataObject();
						}
						catch {}
						if (Startup.RefsForm.rtfRefs.SelectedText!="")
						{
							Startup.RefsForm.rtfRefs.ReadOnly=false; // need to do this to stop the bell
							// sounding when we cut!
							Startup.RefsForm.rtfRefs.Cut();
							Startup.RefsForm.rtfRefs.ReadOnly=true;
						}
						try
						{
							Clipboard.SetDataObject(tmp,true);
						}
						catch {}
						Startup.RefsForm.rtfRefs.SelectionFont=new Font("Tahoma",8,FontStyle.Regular);
						Startup.RefsForm.rtfRefs.SelectedText="; ";
						Startup.RefsForm.rtfRefs.SelectionStart=Startup.RefsForm.rtfRefs.Text.Length;
					}
					prev=vv[i];
					DisplayRefs.Add(new DisplayRef(Startup.RefsForm.rtfRefs.Text.Length,tt[i]));
				}
			}


            }
            catch { }

			
		}

		private void cmbChapter_SelectedIndexChanged()
		{
			try {
			cmbChapter_SelectedIndexChanged(null,null);
		} catch {}
		}


		private void cmbBook_SelectedIndexChanged()
		{
			try {
			cmbBook_SelectedIndexChanged(null,null);
		} catch {}
		}

		public void Box_DoubleClick(object sender, EventArgs e)
		{
			try {
			try
			{
				ListBox box=sender as ListBox;
				string p=box.SelectedItem.ToString();
				GoToRef(p);
				if (box==Startup.HistoryForm.HistoryBox)
				{
					for (int i=0; i<Startup.HistoryForm.HistoryBox.Items.Count;i++)
					{
						if (((string) Startup.HistoryForm.HistoryBox.Items[i])==p)
						{
							Startup.HistoryForm.HistoryBox.SelectedIndex=i;
							break;
						}
					}
				}
			}
			catch
			{
				//no item selected
			}
		} catch {}
		}

		public void Box_MouseUp(object sender, MouseEventArgs e)
		{
			try {
			ListBox box=sender as ListBox;
			dragBox=Rectangle.Empty;
			if (box.IndexFromPoint(e.X,e.Y) != ListBox.NoMatches)
			{
				box.SelectedIndex=box.IndexFromPoint(e.X,e.Y);
			}
		} catch {}
		}

		public void Box_MouseDown(object sender, MouseEventArgs e)
		{
			try {
			ListBox box=sender as ListBox;
			indexToDrag = box.IndexFromPoint(e.X, e.Y);
			if (indexToDrag != ListBox.NoMatches) 
			{
				Size dragSize = SystemInformation.DragSize;
				dragBox = new Rectangle(new Point(e.X - (dragSize.Width /2),
					e.Y - (dragSize.Height /2)), dragSize);
			} 
			else
				dragBox = Rectangle.Empty;
		} catch {}
		}

		public void Box_MouseMove(object sender, MouseEventArgs e)
		{
			try {
			ListBox box=sender as ListBox;
			if ((e.Button & MouseButtons.Left) == MouseButtons.Left) 
			{
				if (dragBox != Rectangle.Empty && 
					!dragBox.Contains(e.X, e.Y)) 
					box.DoDragDrop(box.Items[indexToDrag],DragDropEffects.Copy);
			}
			
		} catch {}
		}

		public void GoToRef(string s)
		{
			try {
			bool jump=suspendJump;
			suspendJump=true;
			try
			{
				int chap=int.Parse(s.Substring(s.IndexOf(" ")+1,
					s.IndexOf(":")-1-s.IndexOf(" ")));
				int verse=int.Parse(s.Substring(s.IndexOf(":")+1));
				int book=0;
				s=s.Substring(0,s.IndexOf(" "));
				for (int i=0; i<73;i++)
				{
					if (s==Bible.Abbrev[i])
					{
						book=i;
						break;
					}
				}
				cmbBook.SelectedIndex=book;
				cmbChapter.SelectedIndex=chap-1;
				cmbVerse.SelectedIndex=verse-1;
				suspendJump=jump;
				btnGo_Click();
			}
			catch
			{
				MessageBox.Show(Startup.rm.GetString("invalidref"),"VulSearch 4",MessageBoxButtons.OK);
				suspendJump=jump;
			}
		} catch {}
		}

		private void ParallelScroll(XRichTextBox source,XRichTextBox target)
		{
			try {
			bool parallel=suspendParallel, jump=suspendJump;
            //source.Size = new Size(source.Width, Startup.MainForm.Height - source.Top - 20);
			if (suspendParallel==false && 
				Startup.OptionsForm.chkParallelScroll.Checked==true )
			{
				suspendParallel=true;
				try
				{
					int i=source.GetCharIndexFromPosition(
						new Point(3,3));
					Regex reg=new Regex(@"[ \n]\d+\u00a0");
					string t=source.Text.Substring(i);
					Match match=reg.Match(source.Text.Substring(i));
					if (match.Success)
					{
						string s=source.Text.Substring(
							i+match.Index+1, match.Length-2);
						int verse=int.Parse(source.Text.Substring(
							i+match.Index+1, match.Length-2));
						if ((source.GetLineFromCharIndex(i)
							- (Startup.OptionsForm.radVerseNewLine
							.Checked ? 1 : 0)) !=
							source.GetLineFromCharIndex(i+match.Index))
						{
							verse--;
						}
						if (verse != prevVerse)
						{
							JumpToVerse(ref target,verse);
						}
						prevVerse=verse;
						suspendJump=true;
						cmbVerse.SelectedIndex=verse-1;
						suspendJump=jump;
					}
				}
				catch
				{
				}
			}
			suspendParallel=parallel;
		} catch {}
		}


		private void rtb_VScroll(object sender, System.EventArgs e)
		{
			try {
			XRichTextBox rtb=sender as XRichTextBox;
			bool b=rtb.Focused;
			if (rtb!=rtbRight)
			{
				ParallelScroll(rtb,rtbRight);
			}
			if (rtb!=rtbLeft)
			{
				ParallelScroll(rtb,rtbLeft);
			}
            if (b) rtb.Focus();
		} catch {}
		}

		private void rtb_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			try {

			if (e.Button==MouseButtons.Left)
				FindInRight=(sender==rtbRight);
		} catch {}
		}

		private void btnPrev_Click(object sender, System.EventArgs e)
		{
			try {
			cmbChapter.SelectedIndex--;
		} catch {}
		}

		private void btnNext_Click(object sender, System.EventArgs e)
		{
			try {
			cmbChapter.SelectedIndex++;
		} catch {}
		}

		/// <summary>
		/// This is a big mess - loads of cases to take into account
		/// </summary>
		private string CleanupWords(string s)
		{
			try {
			string ret, tot="", t ,currentCase="X";
			bool firstMeaning=false;
			string[] lines=s.Split('\n');
			for (int i=0; i<lines.Length; i++)
			{
				t=lines[i];
				ret="";
				if (t.StartsWith("    common") || t.StartsWith("    veryfreq")) continue;
				if (t.StartsWith("%unknown"))
				{
					return "$iunknown$n";
				}
				if (t.StartsWith("%Syncope") || t.StartsWith("%Word mod") ||
					t.StartsWith("%A doubled") || t.StartsWith("%A Terminal") ||
					t.StartsWith("%An internal") || t.StartsWith("%An initial") ||
					t.StartsWith("%Two words") || t.StartsWith("%It is") ||
					t.StartsWith("%May be") || t.StartsWith("%Some forms") ||
					t.StartsWith("Some forms") || t.StartsWith("%Slur"))
				{
					i++;
					continue;
				}


				if (t.StartsWith("*"))
				{
					continue;
				}
				if (t.StartsWith("%"))
				{
					if (firstMeaning)
					{
						tot+="\n";
						firstMeaning=false;
					}
					string u=t.Substring(1)+"  ";
					ret+=u.Substring(0,u.IndexOf(" ")+1);
					u=u.Substring(u.IndexOf(" ")+1).TrimStart();
					currentCase=u.Substring(0,u.IndexOf(" "));
					u=u.Substring(u.IndexOf(" ")+1).TrimStart();
					switch (currentCase)
					{
						case "X":
						{
							ret+=u.Trim();
							continue;
						}
						case "N":
						case "PRON":
						{
							u=u.Substring(u.IndexOf(" ")+1);
							u=u.Substring(u.IndexOf(" ")+1);
							if (!u.StartsWith("X"))
							{
								ret+= "("+u.Substring(0,3).ToLower();
								u=u.Substring(u.IndexOf(" ")+1);
								if (!u.StartsWith("X"))
								{
									ret+= u.StartsWith("S") ? " sing)" : " pl)";
								}
								else
								{
									ret += ")";
								}
							}
							break;
						}

						case "ADJ":
						{
							u=u.Substring(u.IndexOf(" ")+1);
							u=u.Substring(u.IndexOf(" ")+1);
							if (!u.StartsWith("X"))
							{
								ret+= "("+u.Substring(0,3).ToLower();
								u=u.Substring(u.IndexOf(" ")+1);
								string gen="";
								if (!u.StartsWith("X"))
								{
									gen= u.StartsWith("S") ? " sing" : " pl";
								}
								u=u.Substring(u.IndexOf(" ")+1);
								switch (u.Substring(0,1))
								{
									case "X":
									{
										gen=@" m/f/nt" + gen;
										break;
									}
									case "M":
									{
										gen=" m" + gen;
										break;
									}
									case "F":
									{
										gen=" f"+gen;
										break;
									}
									case "N":
									{
										gen=" nt"+gen;
										break;
									}
									case "C":
									{
										gen=@" m/f"+gen;
										break;
									}
								}
								u=u.Substring(u.IndexOf(" ")+1);
								if (u.StartsWith("COMP"))
								{
									gen+=" compar";
								}
								if (u.StartsWith("SUPER"))
								{
									gen+=" superl";
								}
								ret += gen + ")";
							}
							break;

						}

						case "V":
						{
							u=u.Substring(u.IndexOf(" ")+1);
							u=u.Substring(u.IndexOf(" ")+1);
							string vb="";
							switch (u.Substring(0,u.IndexOf(" ")))
							{
								case "X":
									break;
								case "PRES":
								{
									vb="pres";
									break;
								}
								case "IMPF":
								{
									vb="imp";
									break;
								}
								case "FUT":
								{
									vb="fut";
									break;
								}
								case "PERF":
								{
									vb="perf";
									break;
								}
								case "PLUP":
								{
									vb="pluperf";
									break;
								}
								case "FUTP":
								{
									vb="fut perf";
									break;
								}
							}
							u=u.Substring(u.IndexOf(" ")+1).Trim();
							if (u.StartsWith("PASSIVE"))
							{
								vb+=" pass";
							}
							if (u.StartsWith("ACTIVE") || u.StartsWith("PASSIVE")) u=u.Substring(u.IndexOf(" ")+1).Trim();

							switch (u.Substring(0,u.IndexOf(" ")))
							{
								case "X":
									break;
								case "IND":
								{
									vb+=" ind";
									break;
								}
								case "SUB":
								{
									vb +=" subj";
									break;
								}
								case "IMP":
								{
									vb+=" imper";
									break;
								}
								case "INF":
								{
									vb+=" inf";
									break;
								}
								case "PPL":
								{
									vb+=" part";
									break;
								}
							}
							u=u.Substring(u.IndexOf(" ")+1);
							if (u.Substring(0,1)!="0")
							{
								switch (u.Substring(0,1))
								{
									case "1":
									{
										ret+="(1st ";
										break;
									}
									case "2":
									{
										ret+="(2nd ";
										break;
									}
									case "3":
									{
										ret+="(3rd ";
										break;
									}
								}
								u=u.Substring(u.IndexOf(" ")+1);
								ret+= (u.Substring(0,1)=="S" ? "sing, " : "pl, ");
							}
							else
							{
								ret+="(";
							}
							ret+=vb+")";
							break;
						}

						case "VPAR":
						{
							u=u.Substring(u.IndexOf(" ")+1);
							u=u.Substring(u.IndexOf(" ")+1);
							string gen="",cas="";
							if (!u.StartsWith("X"))
							{
								cas= u.Substring(0,3).ToLower();
								u=u.Substring(u.IndexOf(" ")+1);
								if (!u.StartsWith("X"))
								{
									gen= u.StartsWith("S") ? " sing" : " pl";
								}
								u=u.Substring(u.IndexOf(" ")+1);
								switch (u.Substring(0,1))
								{
									case "X":
									{
										gen=@" m/f/nt" + gen;
										break;
									}
									case "M":
									{
										gen=" m" + gen;
										break;
									}
									case "F":
									{
										gen=" f"+gen;
										break;
									}
									case "N":
									{
										gen=" nt"+gen;
										break;
									}
									case "C":
									{
										gen=@" m/f"+gen;
										break;
									}
								}
							}

							u=u.Substring(u.IndexOf(" ")+1);
							string vb="";
							switch (u.Substring(0,u.IndexOf(" ")))
							{
								case "X":
									break;
								case "PRES":
								{
									vb="pres";
									break;
								}
								case "IMPF":
								{
									vb="imp";
									break;
								}
								case "FUT":
								{
									vb="fut";
									break;
								}
								case "PERF":
								{
									vb="perf";
									break;
								}
								case "PLUP":
								{
									vb="pluperf";
									break;
								}
								case "FUTP":
								{
									vb="fut perf";
									break;
								}
							}
							u=u.Substring(u.IndexOf(" ")+1).Trim();
							if (u.StartsWith("PASSIVE"))
							{
								vb+=" pass";
							}
							if (u.StartsWith("ACTIVE") || u.StartsWith("PASSIVE")) u=u.Substring(u.IndexOf(" ")+1).Trim();

							switch (u.Substring(0,u.IndexOf(" ")))
							{
								case "X":
									break;
								case "IND":
								{
									vb+=" ind";
									break;
								}
								case "SUB":
								{
									vb +=" subj";
									break;
								}
								case "IMP":
								{
									vb+=" imper";
									break;
								}
								case "INF":
								{
									vb+=" inf";
									break;
								}
								case "PPL":
								{
									vb+=" part";
									break;
								}
							}
							ret+= "(" + vb + ", " + cas + gen + ")";
							currentCase="V";
							break;
						}

						case "ADV":
						{
							u=u.Trim();
							if (u.StartsWith("COMP"))
							{
								ret+="(compar)";
							}
							if (u.StartsWith("SUPER"))
							{
								ret+="(superl)";
							}
							break;
						}

						case "SUPINE":
						{
							ret+="(supine)";
							currentCase="V";
							break;
						}
						
						case "PREP":
						case "CONJ":
						case "INTERJ":
						{
							break;
						}

						case "NUM":
						{
							if (u.IndexOf("CARD")!=-1) ret+="(card)";
							if (u.IndexOf("ORD")!=-1) ret+="(ord)";
							if (u.IndexOf("DIST")!=-1) ret+="(distrib)";
							if (u.IndexOf("ADVERB")!=-1) ret+="(adv)";
							break;
						}
					
						case "PREFIX":
						{
							ret="$b"+ret+"$n $iprefix$n";
							break;
						}
						case "SUFFIX":
						{
							ret="$b"+ret+"$n $isuffix$n";
							break;
						}

						case "TACKON":
						{
							ret="$b"+ret+"$n $iadd-on$n";
							break;
						}
						default:
						{
							ret=""; // packons
							break;
						}
					}
				}
				else
				{
					firstMeaning=true;
					if ((" "+t+" ").IndexOf(" "+ currentCase +" ") != -1)
					{
						switch (currentCase)
						{
							case "X":
							{
								ret+=t.Trim();
								break;
							}
							case "N":
							{
								ret+="$b" + t.Substring(0,t.IndexOf(" N ")).Trim()+"$n ";
								t=t.Substring(t.IndexOf(" N ")+4,1);
								switch (t)
								{
									case "X":
									{
										ret+="$in$n";
										break;
									}
									case "M":
									case "F":
									{
										ret+="$in " + t.ToLower()+ "$n";
										break;
									}
									case "N":
									{
										ret+="$in nt$n";
										break;
									}
									case "C":
									{
										ret+="$in m/f$n";
										break;
									}
								}
								break;
							} // end case N
						
							case "PRON":
							{
								if (t.IndexOf(" PRON ") != -1)
								{
									ret+="$b" + t.Substring(0,t.IndexOf(" PRON ")).Trim()+"$n $ipron$n\n" +
										t.Substring(t.IndexOf(" PRON ")+7,1);
								}
								break;
							}

							case "ADJ":
							{
								ret+="$b" + t.Substring(0,t.IndexOf(" ADJ ")).Trim()+"$n $iadj$n";
								break;
							}

							case "V":
							{
								ret+="$b" + t.Substring(0,t.IndexOf(" V ")).Trim()+"$n $ivb$n ";
								t=t.Substring(t.IndexOf(" V ")+4)+" ";
								t=t.Substring(0,t.IndexOf(" "));
								switch (t)

								{
									case "DAT":
									case "GEN":
									case "ABL":
									{
										ret+="(+ "+t.ToLower()+")";
										break;
									}
									case "IMPERS":
									{
										ret+="$iimpers$n";
										break;
									}
									case "DEP":
									{
										ret+="$idep$n";
										break;
									}
									case "SEMIDEP":
									{
										ret+="$isemi-dep$n";
										break;
									}
									case "PERFDEF":
									{
										ret+="$iperf def$n";
										break;
									}
								}
								break;
							}

							case "ADV":
							{
								ret+="$b" + t.Substring(0,t.IndexOf(" ADV ")).Trim()+"$n $iadv$n";
								break;
							}

							case "PREP":
							{
								ret+="$b" + t.Substring(0,t.IndexOf(" PREP ")).Trim()+"$n $iprep$n";
								t=t.Substring(t.IndexOf(" PREP ")+7)+" ";
								t=t.Substring(0,t.IndexOf(" ")).Trim();
								ret+=" (+ " + t.ToLower() +")";
								break;
							}

							case "CONJ":
							{
								ret+="$b" + t.Substring(0,t.IndexOf(" CONJ ")).Trim()+"$n $iconj$n";
								break;
							}

							case "INTERJ":
							{
								ret+="$b" + t.Substring(0,t.IndexOf(" INTERJ ")).Trim()+"$n $iinterj$n";
								break;
							}

							case "NUM":
							{
								ret+="$b" + t.Substring(0,t.IndexOf(" NUM ")).Trim()+"$n $inum$n";
								break;
							}

							case "PACK":
							case "TACKON":
							{
								ret+="$b" + t.Substring(7).Trim()+"$n $isuffix$n";
								break;
							}

						} // end switch
					}
					else
					{
						ret+=t.Trim();
					}

					//currentCase="X";
				} // end else
				if (ret!="") tot+=ret.Trim()+"\n";
			} //end for
			return tot.Trim();
            }
            catch { return "Words error"; }
		}

		private void tmrHover_Tick(object sender, System.EventArgs e)
		{
			try {
			tmrHover.Stop();
			if ((!(Startup.OptionsForm.chkNoWords.Checked)) && (Startup.WordsForm.Visible || Startup.OptionsForm.chkWordsTooltip.Checked))
			{
				string whichBible=rtbToBible(ref hoverCurrent);
			
				if ( (Startup.Bibles[Startup.IndexFromExt(whichBible)] as Bible).IsLatin)
				{
					RunWords(ref hoverCurrent,leftX,leftY);
				}
			}
		} catch {}
		}

		private void rtb_MouseLeave(object sender, System.EventArgs e)
		{
			try {
			tmrHover.Stop();
		} catch {}
		}

		public void NotesBox_TextChanged(object sender, EventArgs e)
		{
			try {
			Startup.hashNotes[(Bible.Abbrev[cmbBook.SelectedIndex] +
				" " + (cmbChapter.SelectedIndex+1).ToString())]
				=Startup.NotesForm.NotesBox.Text;
		} catch {}
		}
/*
		private void XRefBox_AfterLabelEdit(object sender, LabelEditEventArgs e)
		{
		try {
			XRefBox_AfterLabelEdit(sender,e,_rold,e.Item);
			} catch {}
		}

		private void XRefBox_AfterLabelEdit(object sender, LabelEditEventArgs e, string rold, int item)
		{
		try {
			if (e!=null) XRefBox.Items[e.Item].Text=e.Label;
			ArrayList a=new ArrayList();

			the idea: to the current book & chapter is assigned an arraylist
			   of the cross-refs associated to it. We're going to rebuild this
			   list by reading in the items in the list-view, into a.
			  
			   If we encounter a reference to a valid book & chapter, we'll try
			   to see if there's already a reference there back here; if so,
			   we'll update it, if not create one. 

			foreach (ListViewItem i in XRefBox.Items)
			{
				XRef r=new XRef(i.SubItems[0].Text,i.SubItems[1].Text);
				a.Add(r);
				Regex reg=new Regex(@"(?:Gn|Ex|Lv|Nm|Dt|Jos|Jdc|Rt|1Rg|2Rg|3Rg|4Rg|1Par|2Par|Esr|Neh|Tob|Jdt|Est|Job|Ps|Pr|Ecl|Ct|Sap|Sir|Is|Jr|Lam|Bar|Ez|Dn|Os|Joel|Am|Abd|Jon|Mch|Nah|Hab|Soph|Agg|Zach|Mal|1Mcc|2Mcc|Mt|Mc|Lc|Jo|Act|Rom|1Cor|2Cor|Gal|Eph|Phlp|Col|1Thes|2Thes|1Tim|2Tim|Tit|Phlm|Hbr|Jac|1Ptr|2Ptr|1Jo|2Jo|3Jo|Jud|Apc) \d{1,3}");
				Match m=reg.Match(i.SubItems[0].Text);
				if (!m.Success) continue;
					
				//should really do more work here, check chapter
				//is valid etc. - but RAM is cheap, who cares if
				//the hash table's bigger than necessary?!

				bool existsRef=false;
				ArrayList b=new ArrayList();
				string t=m.Value.Trim();
				if (Startup.hashXref[t] != null)
				{
					b=Startup.hashXref[t] as ArrayList;
					foreach (object y in b)
					{
						string rr=((XRef) y).Ref;
						m=reg.Match(rr);
						if (!m.Success) continue;
						rr=m.Value.Trim();

						if (rr==(Bible.Abbrev[
							cmbBook.SelectedIndex] + " " + 
							(cmbChapter.SelectedIndex+1).ToString())
							&& ((XRef) y).Desc == rold)
						{
							existsRef=true;
							int h=b.IndexOf(y);
							b.RemoveAt(h);
							b.Insert(h, new XRef(((XRef) y).Ref,
								XRefBox.Items[item].SubItems[1].Text));
							break;
						}
					}
				}
				if (!existsRef)
				{
					b.Add(new XRef((Bible.Abbrev[
						cmbBook.SelectedIndex] + " " + 
						(cmbChapter.SelectedIndex+1).ToString()),
						r.Desc));
				}
				Startup.hashXref[t]=b;

			}

			Startup.hashXref[(Bible.Abbrev[cmbBook.SelectedIndex] +
				" " + (cmbChapter.SelectedIndex+1).ToString())]
				=a;
	} catch {}
		}

		private void XRefBox_MouseUp(object sender, MouseEventArgs e)
		{
			try {
			dragBox=Rectangle.Empty;
			if (e.Button==MouseButtons.Right)
			{
				optEditDesc.Enabled=(XRefBox.SelectedIndices.Count>0);
				optDelRef.Enabled=(XRefBox.SelectedIndices.Count>0);
				MenuCommand selected = popupXRef.TrackPopup(
					XRefBox.PointToScreen(new Point(e.X,e.Y)));
				if (selected==optAddRef)
				{
					ListViewItem i=new ListViewItem(Startup.rm.GetString("newref"));
					i.SubItems.Add("");
					XRefBox.Items.Add(i);
					i.BeginEdit();
				}

				if (selected==optAddRefDialog)
				{
					AddRefForm f=new AddRefForm();
					f.ShowDialog();
					if (f.Reference!="x")
					{
						ListViewItem i=new ListViewItem(f.Reference);
						i.SubItems.Add("");
						XRefBox.Items.Add(i);
					}
					f=null;
					XRefBox_AfterLabelEdit(null,null);
				}
				if (selected==optEditDesc)
				{
					EditDescForm f=new EditDescForm(XRefBox.SelectedItems[0].Text);
					string rold=XRefBox.SelectedItems[0].SubItems[1].Text;
					f.txtDesc.Text=rold;
					f.ShowDialog();
					XRefBox.SelectedItems[0].SubItems[1].Text=f.txtDesc.Text;
					XRefBox_AfterLabelEdit(null,null,rold,XRefBox.SelectedItems[0].Index);
				}

				if (selected==optDelRef)
				{
					string r=XRefBox.SelectedItems[0].SubItems[0].Text;
					string d=XRefBox.SelectedItems[0].SubItems[1].Text;
					XRefBox.SelectedItems[0].Remove();
					Startup.hashXref[Bible.Abbrev[
								cmbBook.SelectedIndex]
								+ " " + (cmbChapter.SelectedIndex+1).
								ToString()]=null;

					foreach (DictionaryEntry de in Startup.hashXref)
					{
						ArrayList b=de.Value as ArrayList;
						if (b==null) continue;
						foreach (object o in b)
						{
							XRef x=(XRef) o;
							if (x.Desc==d && x.Ref.StartsWith((Bible.Abbrev[
								cmbBook.SelectedIndex]
								+ " " + (cmbChapter.SelectedIndex+1).
								ToString())))
							{
								b.Remove(o);
								break;
							}
						}
					}
				}
			}

		} catch {}
		}
*/
		public void JumpToRef(string s)
		{
			try {
			Regex reg=new Regex(@"(?:Gn|Ex|Lv|Nm|Dt|Jos|Jdc|Rt|1Rg|2Rg|3Rg|4Rg|1Par|2Par|Esr|Neh|Tob|Jdt|Est|Job|Ps|Pr|Ecl|Ct|Sap|Sir|Is|Jr|Lam|Bar|Ez|Dn|Os|Joel|Am|Abd|Jon|Mch|Nah|Hab|Soph|Agg|Zach|Mal|1Mcc|2Mcc|Mt|Mc|Lc|Jo|Act|Rom|1Cor|2Cor|Gal|Eph|Phlp|Col|1Thes|2Thes|1Tim|2Tim|Tit|Phlm|Hbr|Jac|1Ptr|2Ptr|1Jo|2Jo|3Jo|Jud|Apc) \d{1,3}:\d{1,3}");
			Match m=reg.Match(s);
			if (!m.Success)
			{
				MessageBox.Show(Startup.rm.GetString("invalidref"),"VulSearch 4",MessageBoxButtons.OK);
			}
			else
			{
				GoToRef(m.Value);
			}
		} catch {}
		}

		private void XRefBox_DoubleClick(object sender, EventArgs e)
		{
			try {
			string s=XRefBox.SelectedItems[0].Text;
			if (s.IndexOf(":")==-1) s+=":1";
			JumpToRef(s);
		} catch {}
		}

		private void XRefBox_MouseDown(object sender, MouseEventArgs e)
		{
			try {
			try
			{
				indexToDrag = XRefBox.GetItemAt(e.X, e.Y).Index;
			}
			catch
			{
				indexToDrag=-1;
			}
			if (indexToDrag >=0 ) 
			{
				Size dragSize = SystemInformation.DragSize;
				dragBox = new Rectangle(new Point(e.X - (dragSize.Width /2),
					e.Y - (dragSize.Height /2)), dragSize);
			} 
			else
				dragBox = Rectangle.Empty;

		} catch {}
		}

		private void XRefBox_MouseMove(object sender, MouseEventArgs e)
		{
			try {
			if ((e.Button & MouseButtons.Left) == MouseButtons.Left) 
			{
				if (dragBox != Rectangle.Empty && 
					!dragBox.Contains(e.X, e.Y)) 
					XRefBox.DoDragDrop(XRefBox.Items[indexToDrag].Text+"\n" 
						+ XRefBox.Items[indexToDrag].SubItems[1].Text,
						DragDropEffects.Copy);
			}
		} catch {}
		}

		private void XRefBox_DragEnter(object sender, DragEventArgs e)
		{
			try {
			if (e.Data.GetDataPresent("System.Windows.Forms.TreeNode", false))
				e.Effect = DragDropEffects.Copy;
			else if (e.Data.GetDataPresent("System.String",false))
				e.Effect=DragDropEffects.Copy;
			else
				e.Effect=DragDropEffects.None;

		} catch {}
		}
/*
		private void XRefBox_DragDrop(object sender, DragEventArgs e)
		{
		try {
			string _ref="",desc="";
			if (e.Data.GetDataPresent("System.Windows.Forms.TreeNode", false))
			{
				TreeNode newNode = (TreeNode) e.Data.GetData(
					typeof(System.Windows.Forms.TreeNode));
				_ref=(string) Startup.BookmarksForm.hashRef[newNode.GetHashCode()];
				desc=(string) Startup.BookmarksForm.hashText[newNode.GetHashCode()];
				RichTextBox rt=new RichTextBox();
				rt.Rtf=desc;
				desc=rt.Text;
				rt=null;
			}

			if (e.Data.GetDataPresent("System.String", false))
			{
				string s=(string) e.Data.GetData("System.String");
				if (s.IndexOf("\n")>=0)
				{
					_ref=s.Substring(0,s.IndexOf("\n"));
					desc=s.Substring(s.IndexOf("\n")+1);
				}
				else
				{
					_ref=s;
					desc="";
				}
			}

			if (_ref!="")
			{
				ListViewItem i=new ListViewItem(_ref);
				i.SubItems.Add(desc);
				XRefBox.Items.Add(i);
				XRefBox_AfterLabelEdit(null,null);
			}
			
			} catch {}
		}
*/


		public void DoCLargs(string command)
		{
			try {
			if (this.WindowState== FormWindowState.Minimized)
				this.WindowState=FormWindowState.Normal;

			string i;
			if (command.IndexOf("-i")>0)
			{
				i=command.Substring(command.IndexOf("-i")+3);
				command=command.Substring(0,command.IndexOf("-i"));
				for (int k=0; k<Startup.Bibles.Count; k++)
				{
					if (((Bible) Startup.Bibles[k]).Extension==i)
						Startup.SearchForm.cmbBible.SelectedIndex=k;
				}
			}

			Startup.SearchForm.cmbSearch.Text=command;
			Startup.SearchForm.btnSearch_Click(null,null);
			if (Startup.ResultsForm.ResultsBox.Items.Count==1 && !(Startup.ResultsForm.ResultsBox.Items[0].ToString().StartsWith("(")))
			{
				Startup.ResultsForm.ResultsBox.SelectedIndex=0;
				Box_DoubleClick((object) Startup.ResultsForm.ResultsBox, null);
			}


		} catch {}
		}

		
		protected override void WndProc(ref Message m)
		{
			try {
			if (m.Msg == AppMessager.WM_COPYDATA)
			{
				string command = AppMessager.ProcessWM_COPYDATA(m);
				if (command != null)
				{
					ProcessCommandLine(command);
					return;
				}
			}
			base.WndProc (ref m);
		} catch {}
		}

		private void ProcessCommandLine(string command)
		{
			try {
			if (command.IndexOf("-s") > 0)
			{
				command=command.Substring(command.IndexOf("-s")+3);
				DoCLargs(command);
			}
		} catch {}
		}

		private void panel_Layout(object sender, System.Windows.Forms.LayoutEventArgs e)
		{
			try {
			int h=panel.Height, w=panel.Width;
			rtbLeft.Height=h-54;
			rtbRight.Height=h-54;
			rtbLeft.Width= (w-28) / 2;
			rtbRight.Width=(w-28)/2;
			rtbRight.Left=(w-28)/2+16;

		} catch {}
		}

		private void XRefBox_BeforeLabelEdit(object sender, LabelEditEventArgs e)
		{
			try {
			_rold=XRefBox.Items[e.Item].SubItems[1].Text ;
		} catch {}
		}

		private int IndexWithLigs(string t,int k, int length,bool LongToShort)
		{
			Regex re=new Regex("[æÆœǽǼ]");
			MatchCollection m=re.Matches(t);
			if (m.Count>0)
			{
				int a=0;
				while (a+1<=m.Count &&  m[a].Index<=k+length)
				{
					if (LongToShort) k--; else k++;
					a++;
				}
			}
			return k;

		}

		public bool FindNext(string s, bool NextOccur)
		{
			bool ret=false;
			try {
			XRichTextBox b=rtbLeft;
			if (FindInRight) b=rtbRight;
			//if (b.Text!="Thæ is a isæ a tæst") b.Text="Thæ is a isæ a tæst";
			int k=-1;
			try
			{
				k=CleanUpAe(b.Text).IndexOf(s,IndexWithLigs(b.Text,b.SelectionStart,0,false)+(NextOccur ? 1 : 0));
			}
			catch {}
			if (k<0) k=CleanUpAe(b.Text).IndexOf(s);
			if (k>=0)
			{
				k=IndexWithLigs(b.Text,k,s.Length ,true);
				b.Focus();
				b.SelectionStart=b.Text.Length-1;
				b.SelectionLength=0;
				int l=-new Regex("(ae|oe|Ae)").Matches(s).Count;
				k-=l;
				l+=s.Length;
				b.SelectionStart=k;
					
				if (Startup.OptionsForm.chkAE.Checked)
					l-=
						b.SelectionLength=l;
				Startup.FindInPageForm.Focus();
				ret=true;
			}
		} catch { }
			return ret;
		}


		private void OnFindInPageClick(object sender, EventArgs e)
		{
			try {
			FindInRight=rtbRight.Focused;
			Startup.FindInPageForm.txtFind.SelectAll();
			Startup.FindInPageForm.Show();
		} catch {}
		}

        private void toolBookmarks_CheckedChanged(object sender, EventArgs e)
        {
            Startup.BookmarksForm.Visible = toolBookmarks.Checked;
        }

        private void toolXrefs_CheckStateChanged(object sender, EventArgs e)
        {
            Startup.RefsForm.Visible = toolXrefs.Checked;
        }

        private void toolSearch_CheckStateChanged(object sender, EventArgs e)
        {
            Startup.SearchForm.Visible = toolSearch.Checked;
        }

        private void toolWords_CheckStateChanged(object sender, EventArgs e)
        {
            Startup.WordsForm.Visible = toolWords.Checked;
        }

        private void toolSearchResults_CheckStateChanged(object sender, EventArgs e)
        {
            Startup.ResultsForm.Visible = toolSearchResults.Checked;
        }

        private void toolHistory_CheckStateChanged(object sender, EventArgs e)
        {
            Startup.HistoryForm.Visible = toolHistory.Checked;
        }

        private void toolNotes_CheckStateChanged(object sender, EventArgs e)
        {
            Startup.NotesForm.Visible = toolNotes.Checked;
        }

        private void menuRTBCopy_Click(object sender, EventArgs e)
        {
            try
            {
                ToolStripMenuItem x = sender as ToolStripMenuItem;
                ContextMenuStrip y = x.Owner as ContextMenuStrip;
                XRichTextBox rtb = y.SourceControl as XRichTextBox;
                DataObject copy = new DataObject();
                // Place text version in DataObject as text and unicode
                copy.SetData(DataFormats.Text, rtb.SelectedText);
                copy.SetData(DataFormats.UnicodeText, rtb.SelectedText);
                copy.SetData(DataFormats.Rtf, rtb.SelectedRtf);
                Clipboard.SetDataObject(copy, true);
            }
            catch ( Exception ex)
            {
                MessageBox.Show("Failed to copy to clipboard!\n" +  ex.ToString(), "Copy error",  MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void menuRTBWords_Click(object sender, EventArgs e)
        {
			try {

			string whichBible = rtbToBible(ref hoverCurrent);

			if ((Startup.Bibles[Startup.IndexFromExt(whichBible)] as Bible).IsLatin)
			{
				RunWords(ref hoverCurrent, leftX, leftY);
			}
			}

			catch
			{
                MessageBox.Show("Error", "Failed to run Words!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}

        }

        private int xold = -1, yold = -1;

        private void MainForm_Move(object sender, EventArgs e)
        {
             int xnew = this.Location.X;
            int ynew = this.Location.Y;
            if (xold >= 0 && this.toolLock.Checked) {
                Startup.SearchForm.Location = new Point(Startup.SearchForm.Location.X + (xnew - xold), Startup.SearchForm.Location.Y + (ynew - yold));
                Startup.BookmarksForm.Location = new Point(Startup.BookmarksForm.Location.X + (xnew - xold), Startup.BookmarksForm.Location.Y + (ynew - yold));
                Startup.HistoryForm.Location = new Point(Startup.HistoryForm.Location.X + (xnew - xold), Startup.HistoryForm.Location.Y + (ynew - yold));
                Startup.NotesForm.Location = new Point(Startup.NotesForm.Location.X + (xnew - xold), Startup.NotesForm.Location.Y + (ynew - yold));
                Startup.RefsForm.Location = new Point(Startup.RefsForm.Location.X + (xnew - xold), Startup.RefsForm.Location.Y + (ynew - yold));
                Startup.ResultsForm.Location = new Point(Startup.ResultsForm.Location.X + (xnew - xold), Startup.ResultsForm.Location.Y + (ynew - yold));
                Startup.WordsForm.Location = new Point(Startup.WordsForm.Location.X + (xnew - xold), Startup.WordsForm.Location.Y + (ynew - yold));
            }
            Startup.CheckFormPositionsAreSensible();
            xold = xnew;
            yold = ynew;
        }

        private void troubleshootVulSearchProblemsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Startup.TroubleForm.ShowDialog();
            }
            catch { }
        }

        private void menuMakeSensible_Click(object sender, EventArgs e)
        {
            try { Startup.CheckFormPositionsAreSensible(); }
            catch { }
        }

        private void rtbLeft_SizeChanged(object sender, EventArgs e)
        {
            //resize_rtb();
        }

        private void tmrHack_Tick(object sender, EventArgs e)
        {
            if (++this.timer_hack_count == 100)
                this.tmrHack.Enabled = false;
            try
            {
                resize_rtb();
            }
            catch { }


        }



	}
}
