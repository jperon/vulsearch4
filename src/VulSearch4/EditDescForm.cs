using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace VulSearch4
{
	/// <summary>
	/// Summary description for EditDescForm.
	/// </summary>
	public class EditDescForm : System.Windows.Forms.Form
	{
		public System.Windows.Forms.TextBox txtDesc;
		private System.Windows.Forms.Label lblRef;
		private System.Windows.Forms.Button btnOK;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public EditDescForm(string _ref)
		{
			try {
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			lblRef.Text=_ref;
		} catch {}
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			try {
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		} catch {}
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(EditDescForm));
			this.txtDesc = new System.Windows.Forms.TextBox();
			this.lblRef = new System.Windows.Forms.Label();
			this.btnOK = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// txtDesc
			// 
			this.txtDesc.AccessibleDescription = resources.GetString("txtDesc.AccessibleDescription");
			this.txtDesc.AccessibleName = resources.GetString("txtDesc.AccessibleName");
			this.txtDesc.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("txtDesc.Anchor")));
			this.txtDesc.AutoSize = ((bool)(resources.GetObject("txtDesc.AutoSize")));
			this.txtDesc.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtDesc.BackgroundImage")));
			this.txtDesc.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("txtDesc.Dock")));
			this.txtDesc.Enabled = ((bool)(resources.GetObject("txtDesc.Enabled")));
			this.txtDesc.Font = ((System.Drawing.Font)(resources.GetObject("txtDesc.Font")));
			this.txtDesc.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("txtDesc.ImeMode")));
			this.txtDesc.Location = ((System.Drawing.Point)(resources.GetObject("txtDesc.Location")));
			this.txtDesc.MaxLength = ((int)(resources.GetObject("txtDesc.MaxLength")));
			this.txtDesc.Multiline = ((bool)(resources.GetObject("txtDesc.Multiline")));
			this.txtDesc.Name = "txtDesc";
			this.txtDesc.PasswordChar = ((char)(resources.GetObject("txtDesc.PasswordChar")));
			this.txtDesc.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("txtDesc.RightToLeft")));
			this.txtDesc.ScrollBars = ((System.Windows.Forms.ScrollBars)(resources.GetObject("txtDesc.ScrollBars")));
			this.txtDesc.Size = ((System.Drawing.Size)(resources.GetObject("txtDesc.Size")));
			this.txtDesc.TabIndex = ((int)(resources.GetObject("txtDesc.TabIndex")));
			this.txtDesc.Text = resources.GetString("txtDesc.Text");
			this.txtDesc.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("txtDesc.TextAlign")));
			this.txtDesc.Visible = ((bool)(resources.GetObject("txtDesc.Visible")));
			this.txtDesc.WordWrap = ((bool)(resources.GetObject("txtDesc.WordWrap")));
			// 
			// lblRef
			// 
			this.lblRef.AccessibleDescription = resources.GetString("lblRef.AccessibleDescription");
			this.lblRef.AccessibleName = resources.GetString("lblRef.AccessibleName");
			this.lblRef.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lblRef.Anchor")));
			this.lblRef.AutoSize = ((bool)(resources.GetObject("lblRef.AutoSize")));
			this.lblRef.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lblRef.Dock")));
			this.lblRef.Enabled = ((bool)(resources.GetObject("lblRef.Enabled")));
			this.lblRef.Font = ((System.Drawing.Font)(resources.GetObject("lblRef.Font")));
			this.lblRef.Image = ((System.Drawing.Image)(resources.GetObject("lblRef.Image")));
			this.lblRef.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblRef.ImageAlign")));
			this.lblRef.ImageIndex = ((int)(resources.GetObject("lblRef.ImageIndex")));
			this.lblRef.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lblRef.ImeMode")));
			this.lblRef.Location = ((System.Drawing.Point)(resources.GetObject("lblRef.Location")));
			this.lblRef.Name = "lblRef";
			this.lblRef.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lblRef.RightToLeft")));
			this.lblRef.Size = ((System.Drawing.Size)(resources.GetObject("lblRef.Size")));
			this.lblRef.TabIndex = ((int)(resources.GetObject("lblRef.TabIndex")));
			this.lblRef.Text = resources.GetString("lblRef.Text");
			this.lblRef.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblRef.TextAlign")));
			this.lblRef.Visible = ((bool)(resources.GetObject("lblRef.Visible")));
			// 
			// btnOK
			// 
			this.btnOK.AccessibleDescription = resources.GetString("btnOK.AccessibleDescription");
			this.btnOK.AccessibleName = resources.GetString("btnOK.AccessibleName");
			this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnOK.Anchor")));
			this.btnOK.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnOK.BackgroundImage")));
			this.btnOK.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnOK.Dock")));
			this.btnOK.Enabled = ((bool)(resources.GetObject("btnOK.Enabled")));
			this.btnOK.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnOK.FlatStyle")));
			this.btnOK.Font = ((System.Drawing.Font)(resources.GetObject("btnOK.Font")));
			this.btnOK.Image = ((System.Drawing.Image)(resources.GetObject("btnOK.Image")));
			this.btnOK.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnOK.ImageAlign")));
			this.btnOK.ImageIndex = ((int)(resources.GetObject("btnOK.ImageIndex")));
			this.btnOK.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnOK.ImeMode")));
			this.btnOK.Location = ((System.Drawing.Point)(resources.GetObject("btnOK.Location")));
			this.btnOK.Name = "btnOK";
			this.btnOK.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnOK.RightToLeft")));
			this.btnOK.Size = ((System.Drawing.Size)(resources.GetObject("btnOK.Size")));
			this.btnOK.TabIndex = ((int)(resources.GetObject("btnOK.TabIndex")));
			this.btnOK.Text = resources.GetString("btnOK.Text");
			this.btnOK.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnOK.TextAlign")));
			this.btnOK.Visible = ((bool)(resources.GetObject("btnOK.Visible")));
			this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
			// 
			// EditDescForm
			// 
			this.AcceptButton = this.btnOK;
			this.AccessibleDescription = resources.GetString("$this.AccessibleDescription");
			this.AccessibleName = resources.GetString("$this.AccessibleName");
			this.AutoScaleBaseSize = ((System.Drawing.Size)(resources.GetObject("$this.AutoScaleBaseSize")));
			this.AutoScroll = ((bool)(resources.GetObject("$this.AutoScroll")));
			this.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("$this.AutoScrollMargin")));
			this.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("$this.AutoScrollMinSize")));
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = ((System.Drawing.Size)(resources.GetObject("$this.ClientSize")));
			this.ControlBox = false;
			this.Controls.Add(this.btnOK);
			this.Controls.Add(this.lblRef);
			this.Controls.Add(this.txtDesc);
			this.Enabled = ((bool)(resources.GetObject("$this.Enabled")));
			this.Font = ((System.Drawing.Font)(resources.GetObject("$this.Font")));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("$this.ImeMode")));
			this.Location = ((System.Drawing.Point)(resources.GetObject("$this.Location")));
			this.MaximumSize = ((System.Drawing.Size)(resources.GetObject("$this.MaximumSize")));
			this.MinimumSize = ((System.Drawing.Size)(resources.GetObject("$this.MinimumSize")));
			this.Name = "EditDescForm";
			this.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("$this.RightToLeft")));
			this.ShowInTaskbar = false;
			this.StartPosition = ((System.Windows.Forms.FormStartPosition)(resources.GetObject("$this.StartPosition")));
			this.Text = resources.GetString("$this.Text");
			this.ResumeLayout(false);

		}
		#endregion

		private void btnOK_Click(object sender, System.EventArgs e)
		{
			try {
			this.Hide();
		} catch {}
		}
	}
}
