using System;
using System.Collections;
using System.Windows.Forms;
using System.Xml;
using System.IO;
using Microsoft.Win32;
using System.Resources;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Drawing;

namespace VulSearch4
{
	public struct RefType
	{
		public string Name;
		public Color Colour;
        public RefType(string name, int red, int green, int blue)
        {
            Name = name;
            Colour = Color.FromArgb(red, green, blue);
        }
	}

	public struct NewRef
	{
		public string Verses;
		public string Ref;
		public string Desc;
		public string Type;
		public NewRef(string verses, string _ref, string desc, string type)
		{
			Verses=verses;
			Ref=_ref;
			Desc=desc;
			Type=type;
		}
	}

	public struct DisplayRef
	{
		public int Position;
		public NewRef Ref;
        public DisplayRef(int pos, NewRef _ref)
        {
            Ref = _ref;
            Position = pos;
        }
	}


	/// <summary>
	/// This class reads in all options at startup and supplies
	/// them to other classes as needed.
	/// </summary>
	public class Startup
	{
		public static SearchForm SearchForm;// = new SearchForm();
		public static BookmarksForm BookmarksForm;// = new BookmarksForm();
		public static MainForm MainForm ;//= new MainForm();
		public static RefsForm RefsForm;
        public static NotesForm NotesForm;
        public static ResultsForm ResultsForm;
        public static HistoryForm HistoryForm;
        public static WordsForm WordsForm;
		public static OptionsForm OptionsForm;// = new OptionsForm();
		public static SearchBMForm SearchBMForm;//=new SearchBMForm();
        public static TroubleForm TroubleForm;
        public static FindInPageForm FindInPageForm;
		public static ArrayList Bibles = new ArrayList(2);
		public static string CLargs="";
		public static string DataPath=System.Environment.GetFolderPath(System.Environment.SpecialFolder.LocalApplicationData)+"\\Vulsearch 4";

		public static int CurTxtVer=0;
		private static bool haveReadRootBookmark=false;

        public static bool troubleshootingModeDontWriteSettings = false;
        
		public static Hashtable hashNotes=new Hashtable();
		public static Hashtable hashXref=new Hashtable();
		public static Hashtable hashReverseRef=new Hashtable();
		public static ArrayList RefTypes=new ArrayList(2);

		public static ResourceManager rm;

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main(string [] args) 
		{
			try {
#if !DEBUG
			try
			{
#endif
				SplashScreen splash;
				//Application.EnableVisualStyles();

				//if a previous instance is found then
				//send commandline to this instance and exit
	#if !DEBUG
				if (AppMessager.CheckPrevInstance()) return;
	#endif

				//Accentuate.Accentuate.DoAccentuate("oblivisceris");
                if (!Directory.Exists(DataPath)) Directory.CreateDirectory(DataPath);

				// setup command line args
				if (args.Length>1)
				{
					if (args[0] == "-s")
					{
						for (int i=1; i<args.Length; i++)
						{
							CLargs+=(" "+args[i]);
						}
					CLargs=CLargs.Trim();
					}
				}

				// read options
				RegistryKey key = Registry.CurrentUser.OpenSubKey(
					"Software\\VulSearch4\\VulSearch4.2",true); 

				if (key == null)
				{
					CreateDefaultKey(ref key);
				}

            /*
				if
					(bool.Parse((string) key.GetValue("French","false")))
					System.Threading.Thread.CurrentThread.CurrentUICulture = new CultureInfo("fr");
				if
					(bool.Parse((string) key.GetValue("English","false")))
                    */
					System.Threading.Thread.CurrentThread.CurrentUICulture = new CultureInfo("");
				
				splash=new SplashScreen();
				splash.Show();
				Application.DoEvents();
				SearchForm = new SearchForm();
				rm = new ResourceManager("VulSearch4.MyStrings", SearchForm.GetType().Assembly);
				BookmarksForm = new BookmarksForm();
				RefsForm=new RefsForm();
                NotesForm = new NotesForm();
                ResultsForm = new ResultsForm();
                HistoryForm = new HistoryForm();
                WordsForm = new WordsForm();
				MainForm = new MainForm();
				OptionsForm = new OptionsForm();
                TroubleForm = new TroubleForm();
				SearchBMForm=new SearchBMForm();
				FindInPageForm=new FindInPageForm();

				// read in the installed Bibles. The Clementine Vulgate
				// gets special priority
				FileInfo[] fi;
				DirectoryInfo di;

                di = new DirectoryInfo(MainForm.AppPath() + "\\Text");
                fi = di.GetFiles("*");
                if (!Directory.Exists(DataPath)) Directory.CreateDirectory(DataPath);
                if (!Directory.Exists(DataPath + "\\Text")) Directory.CreateDirectory(DataPath + "\\Text");
                foreach (FileInfo f in fi)
                {
                    CheckAndCopy(MainForm.AppPath(), "\\Text\\" + f, "");
                }

#if DEBUG
                /* in the DEBUG case, we want to copy the .lat files from
                 * Text AND everything else (including lat.xml) from
                 * TextOther */
                di = new DirectoryInfo(MainForm.AppPath() + "\\TextOther");
                fi = di.GetFiles("*");
                foreach (FileInfo f in fi)
                {
                    CheckAndCopy(MainForm.AppPath(), "\\TextOther\\" + f,
                    "\\Text\\" + f);
                }
#endif

                di=new DirectoryInfo(DataPath+"\\Text");
				fi=di.GetFiles("*.xml");

				for (int i=0;i<=1;i++)
				{
					foreach (FileInfo f in fi)
					{
						if ((f.Name=="lat.xml" && i==0) || (f.Name!="lat.xml" &&
							i==1))
						{
							string ext=f.Name;
							if (ext.IndexOf(".")!=-1)
								ext=ext.Substring(0,ext.LastIndexOf("."));
							XmlTextReader xr=new XmlTextReader(f.FullName);
							xr.Read();
							xr.ReadStartElement("Bible");
							string name=xr.ReadElementString("Name");
							bool isLat=bool.Parse(xr.ReadElementString("IsLatin"));
							string desc=xr.ReadElementString("Description");
							xr.Close();

							Bibles.Add(new Bible(name,ext,desc,isLat));
							SearchForm.cmbBible.Items.Add(name);
						}
					}
				}
            ReadKey(key);

			if(OptionsForm.chkInternet.Checked) {
				try {
					System.Net.WebClient myClient = new System.Net.WebClient();
					StreamReader i=new StreamReader(myClient.OpenRead("http://vulsearch.sourceforge.net/latest.txt"), System.Text.Encoding.ASCII);
					if(CurTxtVer < int.Parse(i.ReadLine().Trim()))
            MainForm.UpdateTexts(true);
					i.Close();
				}
				catch {
				}
			}

				splash.Hide();
				splash=null;
                RestoreForms(key);
                key.Close();
                CheckFormPositionsAreSensible();
				Application.Run(MainForm);
#if !DEBUG
			}
			catch (Exception exc)
			{
				System.Windows.Forms.MessageBox.Show("An internal error has occured.\n"
					+ "Please send an email to clementinetextproject@mail.com explaining what you were doing "
					+"when the error occurred, and including the text below, so that I can try to "
					+"fix any bug.\n\n"
					+ exc.Message + "\n" +
					"Stack trace: " + exc.StackTrace+"\n"+
					"Site: " + exc.TargetSite +"\n"+
					"Source: "+ exc.Source+"\n");
			}
#endif
            } catch {}
		}

		public static Color ColourofType(string name)
		{
	
			foreach(RefType r in RefTypes)
			{
				if (r.Name==name)
					return r.Colour;
			}
			return Color.FromArgb(0,0,0);
		
		}

		public static int IndexofType(string name)
		{
			try {
			for (int i=0; i<RefsForm.cmbType.Items.Count; i++)
			{
				if (((string) RefsForm.cmbType.Items[i])==name)
					return i;
			}
			return 0;
            }
            catch { return 0; }
		}


		public static int IndexFromExt(string ext)
		{
			try {
			for (int i=0; i<Bibles.Count;i++)
			{
				if ((Bibles[i] as Bible).Extension==ext)
					return i;
			}
			return 0;
            }
            catch { return 0; }
		}

		private static void CreateDefaultKey(ref RegistryKey key)
		{
			try {
			key = Registry.CurrentUser.CreateSubKey(
			"Software\\VulSearch4\\VulSearch4.2");
		} catch {}
		}

		private static void CheckAndCopy(string pth, string src_fname, string dst_fname)
		{
           if (dst_fname == "")
                dst_fname = src_fname;
			try {
			if (!Directory.Exists(DataPath)) Directory.CreateDirectory(DataPath);
			try
			{
				File.Copy(pth+"\\"+src_fname,DataPath+"\\"+dst_fname,false);
			}
			catch
			{
				// if file already exists, no prob
			}
		} catch {}
		}

        private static int ReadRegValue(RegistryKey key, string name, int default_val)
        {
            try
            {
                return int.Parse((string)key.GetValue(name));
            }
            catch
            {
                return default_val;
            }
        }

        private static bool ReadRegValue(RegistryKey key, string name, bool default_val)
        {
            try
            {
                return bool.Parse((string)key.GetValue(name));
            }
            catch
            {
                return default_val;
            }
        }

        private static float ReadRegValue(RegistryKey key, string name, float default_val)
        {
            try
            {
                return float.Parse((string)key.GetValue(name));
            }
            catch
            {
                return default_val;
            }
        }

        private static void RestoreForms(RegistryKey key)
        {
            MainForm.Top = ReadRegValue(key, "MainNewTop", 0);
            MainForm.Left = ReadRegValue(key, "MainNewLeft", 163);
            MainForm.Height = ReadRegValue(key, "MainNewHeight", 713);
            MainForm.Width = ReadRegValue(key, "MainNewWidth", 733);

            BookmarksForm.Top = ReadRegValue(key, "BookmarksTop", 0);
            BookmarksForm.Left = ReadRegValue(key, "BookmarksLeft", 893);
            BookmarksForm.Height = ReadRegValue(key, "BookmarksHeight", 456);
            BookmarksForm.Width = ReadRegValue(key, "BookmarksWidth", 172);
            Startup.MainForm.toolBookmarks.Checked = BookmarksForm.Visible = ReadRegValue(key, "BookmarksVisible", true);

            SearchForm.Top = ReadRegValue(key, "SearchTop", 713);
            SearchForm.Left = ReadRegValue(key, "SearchLeft", 535);
            SearchForm.Height = ReadRegValue(key, "SearchHeight", 117);
            SearchForm.Width = ReadRegValue(key, "SearchWidth", 360);
            Startup.MainForm.toolSearch.Checked = SearchForm.Visible = ReadRegValue(key, "SearchVisible", true);

            RefsForm.Top = ReadRegValue(key, "RefsTop", 456);
            RefsForm.Left = ReadRegValue(key, "RefsLeft", 893);
            RefsForm.Height = ReadRegValue(key, "RefsHeight", 374);
            RefsForm.Width = ReadRegValue(key, "RefsWidth", 172);
            Startup.MainForm.toolXrefs.Checked = RefsForm.Visible = ReadRegValue(key, "RefsVisible", true);

            NotesForm.Top = ReadRegValue(key, "NotesTop", 586);
            NotesForm.Left = ReadRegValue(key, "NotesLeft", 0);
            NotesForm.Height = ReadRegValue(key, "NotesHeight", 244);
            NotesForm.Width = ReadRegValue(key, "NotesWidth", 163);
            Startup.MainForm.toolNotes.Checked = NotesForm.Visible = ReadRegValue(key, "NotesVisible", true);

            ResultsForm.Top = ReadRegValue(key, "ResultsTop", 0);
            ResultsForm.Left = ReadRegValue(key, "ResultsLeft", 0);
            ResultsForm.Height = ReadRegValue(key, "ResultsHeight", 244);
            ResultsForm.Width = ReadRegValue(key, "ResultsWidth", 163);
            Startup.MainForm.toolSearchResults.Checked = ResultsForm.Visible = ReadRegValue(key, "ResultsVisible", false);

            HistoryForm.Top = ReadRegValue(key, "HistoryTop", 244);
            HistoryForm.Left = ReadRegValue(key, "HistoryLeft", 0);
            HistoryForm.Height = ReadRegValue(key, "HistoryHeight", 341);
            HistoryForm.Width = ReadRegValue(key, "HistoryWidth", 163);
            Startup.MainForm.toolHistory.Checked = HistoryForm.Visible = ReadRegValue(key, "HistoryVisible", true);

            WordsForm.Top = ReadRegValue(key, "WordsTop", 713);
            WordsForm.Left = ReadRegValue(key, "WordsLeft", 163);
            WordsForm.Height = ReadRegValue(key, "WordsHeight", 117);
            WordsForm.Width = ReadRegValue(key, "WordsWidth", 373);
            Startup.MainForm.toolWords.Checked = WordsForm.Visible = ReadRegValue(key, "WordsVisible", true);

        }

		private static void ReadKey(RegistryKey key)
		{
			try {
			if ( ReadRegValue(key, "FirstRun",true))
			{
				string oldPath=(string) key.GetValue("Path","x");
				if (oldPath=="x")
				{
					//no VulSearch 4 beta installed
					ImportOldForm f=new ImportOldForm(key);
					f.ShowDialog();
					f=null;
				}
				else
				{
					CheckAndCopy(oldPath,"bookmarks.xml", "");
					CheckAndCopy(oldPath,"notes.xml", "");
					CheckAndCopy(oldPath,"xrefs.xml", "");
					// key.SetValue("Path",key.GetValue("Path41"));
					// ^ not needed?
				}
			}

			// if no user data for bookmarks etc. exists, then move the default files from the VS folder
#if DEBUG
			CheckAndCopy(MainForm.AppPath()+"\\misc","bookmarks.xml", "");
			CheckAndCopy(MainForm.AppPath()+"\\misc","notes.xml", "");
			CheckAndCopy(MainForm.AppPath()+"\\misc","refs.xml", "");
#else
			CheckAndCopy(MainForm.AppPath(),"bookmarks.xml", "");
			CheckAndCopy(MainForm.AppPath(),"notes.xml", "");
			CheckAndCopy(MainForm.AppPath(),"refs.xml", "");
#endif

            MainForm.toolLock.Checked = ReadRegValue(key, "WindowsLocked", false);

			MainForm.LeftBible=(string) key.GetValue("LeftBible","lat");
			MainForm.RightBible=(string) key.GetValue("RightBible","eng");
			MainForm.LastBook=ReadRegValue(key,"LastBook",0);
			MainForm.LastChapter=ReadRegValue(key,"LastChapter",0);
			MainForm.LastVerse=ReadRegValue(key,"LastVerse",0);

			SearchForm.cmbBible.SelectedIndex =IndexFromExt((string) key.GetValue("BibleSearching","lat"));

			string fontName=(string) key.GetValue("MainFontName","Verdana");
			System.Drawing.FontStyle fontStyle=(System.Drawing.FontStyle) ( ReadRegValue(key, "MainFontStyle",0));
			float fontSize= ReadRegValue(key, "MainFontSize",13f);
			OptionsForm.fontMain.Font=new System.Drawing.Font(fontName,fontSize,fontStyle);

			CurTxtVer=ReadRegValue(key,"CurTxtVer",22);
			fontName=(string) key.GetValue("VerseFontName","Verdana");
			fontStyle=(System.Drawing.FontStyle) ReadRegValue(key,"VerseFontStyle",1);
			fontSize=ReadRegValue(key,"VerseFontSize",8f);
			OptionsForm.fontNumbers.Font=new System.Drawing.Font(fontName,fontSize,fontStyle);
			OptionsForm.UpdateFontDesc();


			SearchBMForm.Top= ReadRegValue(key,"SearchBMTop",50);
			SearchBMForm.Left= ReadRegValue(key,"SearchBMLeft",50);
			SearchBMForm.Height= ReadRegValue(key,"SearchBMHeight",336);
			SearchBMForm.Width= ReadRegValue(key,"SearchBMWidth",304);
			SearchBMForm.lvres.Columns[0].Width=ReadRegValue(key,"SearchBMCol0",85);
			SearchBMForm.lvres.Columns[1].Width=ReadRegValue(key,"SearchBMCol1",84);
			SearchBMForm.lvres.Columns[2].Width=ReadRegValue(key,"SearchBMCol2",163);

			RefsForm.radVerse.Checked =ReadRegValue(key,"XRefsByVerse",true);
			RefsForm.radType.Checked =!RefsForm.radVerse.Checked ;

			OptionsForm.chkVerseSuper.Checked=ReadRegValue(key,"VerseSuper",true);
			OptionsForm.chkPoetry.Checked=ReadRegValue(key,"Poetry",true);
			OptionsForm.radParagraphs.Checked=ReadRegValue(key,"Paragraphs",true);
			OptionsForm.radVerseNewLine.Checked = !(OptionsForm.radParagraphs.Checked);

			OptionsForm.radMissals.Checked=ReadRegValue(key,"Missals",true);
			OptionsForm.radPrinted.Checked=ReadRegValue(key,"Printed",false);
			OptionsForm.radClassical.Checked=ReadRegValue(key,"Classical",false);
			OptionsForm.radRules.Checked=ReadRegValue(key,"Rules",false);
			OptionsForm.chkIJ.Checked=ReadRegValue(key,"IJ",true);
			OptionsForm.chkUV.Checked=ReadRegValue(key,"UV",true);
			OptionsForm.chkAE.Checked=ReadRegValue(key,"AE",true);
			OptionsForm.chkAccent.Checked=ReadRegValue(key,"Accent",true);
			OptionsForm.chkWordsTooltip.Checked=ReadRegValue(key,"WordsTooltip",false);
			OptionsForm.chkNoWords.Checked=ReadRegValue(key,"NoWords",false);

			OptionsForm.chkStoreSearch.Checked=ReadRegValue(key,"StoreSearch",true);
			OptionsForm.chkSaveSearch.Checked=ReadRegValue(key,"SaveSearch",true);
			OptionsForm.chkSaveHistory.Checked = ReadRegValue(key,"SaveHistory",true);
			OptionsForm.chkParallelScroll.Checked=ReadRegValue(key,"ParallelScroll",true);
			OptionsForm.chkInternet.Checked=ReadRegValue(key,"Internet",true);
			OptionsForm.nudHistory.Value=ReadRegValue(key,"nudHistory",20);
			for (int j=1;j<=(int) OptionsForm.nudHistory.Value;j++)
			{
				string s=(string) key.GetValue("History" + j.ToString(),"");
				if (s != "")
				{
					HistoryForm.HistoryBox.Items.Add(s);
				}
			}
			for (int j=1;j<=10;j++)
			{
				string s=(string) key.GetValue("Search" + j.ToString(),"");
				if (s!="")
				{
					SearchForm.cmbSearch.Items.Add(s);
				}
			}

			// read bookmarks
			XmlTextReader xr=new XmlTextReader(Startup.DataPath+"\\bookmarks.xml");
			try {
				xr.Read();
				TreeNode n=BookmarksForm.tvwbm.Nodes[0];
				ReadBookmarkXML(ref xr,n);
				xr.Close();
			}
			catch {
				xr.Close();
				File.Copy(Startup.DataPath+"\\bookmarks.xml",Startup.DataPath+"\\corrupt-bookmarks.xml",true);
				File.Copy(Startup.DataPath+"\\bookmarks.xml.bak",Startup.DataPath+"\\bookmarks.xml",true);
				xr=new XmlTextReader(Startup.DataPath+"\\bookmarks.xml");
				xr.Read();
				TreeNode n=BookmarksForm.tvwbm.Nodes[0];
				ReadBookmarkXML(ref xr,n);
				xr.Close();
			}
			BookmarksForm.UpdateImages(BookmarksForm.tvwbm.Nodes[0]);
			BookmarksForm.tvwbm.SelectedNode=BookmarksForm.tvwbm.Nodes[0];
			
			if (File.Exists(Startup.DataPath+"\\notes.xml")) {
				try {
					xr=new XmlTextReader(Startup.DataPath+"\\notes.xml");
					xr.Read();
					xr.ReadStartElement("Notes");
					while (xr.IsStartElement("Note")) {
						xr.ReadStartElement("Note");
						string k= xr.ReadElementString("Key");
						string v=xr.ReadElementString("Value");
						hashNotes[k]=v;
						xr.ReadEndElement();
					}
					xr.Close();
				}
				catch  {
					xr.Close();
					File.Copy(Startup.DataPath+"\\notes.xml",Startup.DataPath+"\\corrupt-notes.xml",true);
					File.Copy(Startup.DataPath+"\\notes.xml.bak",Startup.DataPath+"\\notes.xml",true);
					xr=new XmlTextReader(Startup.DataPath+"\\notes.xml");
					xr.Read();
					xr.ReadStartElement("Notes");
					while (xr.IsStartElement("Note")) {
						xr.ReadStartElement("Note");
						string k= xr.ReadElementString("Key");
						string v=xr.ReadElementString("Value");
						hashNotes[k]=v;
						xr.ReadEndElement();
					}
					xr.Close();
				}
			}
			//read new refs
			// always have special types
			RefTypes.Add(new RefType(rm.GetString("crossreference"),0,0,0));
			RefTypes.Add(new RefType(rm.GetString("footnote"),255,0,0));

			if (File.Exists(Startup.DataPath+"\\refs.xml"))
			{
				try {
					ImportRefs(Startup.DataPath+"\\refs.xml");
				}
				catch  {
					File.Copy(Startup.DataPath+"\\refs.xml",Startup.DataPath+"\\corrupt-refs.xml",true);
					File.Copy(Startup.DataPath+"\\refs.xml.bak",Startup.DataPath+"\\refs.xml",true);
					ImportRefs(Startup.DataPath+"\\refs.xml");
				}
			}

			if (! ReadRegValue(key, "ImportedOldRefs",false))
                ReadOldRefs();

			foreach (RefType r in RefTypes)
			{
				RefsForm.cmbType.Items.Add(r.Name);
			}
			RefsForm.cmbType.SelectedIndex= ReadRegValue(key, "LastRefType",0);
			
			BookmarksForm.UpdateImages(BookmarksForm.tvwbm.Nodes[0]);
			BookmarksForm.tvwbm.SelectedNode=BookmarksForm.tvwbm.Nodes[0];
			
			try
			{
				TreeNode m=BookmarksForm.tvwbm.Nodes[0];
				string path=(string) key.GetValue("LastBM");
				foreach (string y in path.Split('\\'))
				{
					if (y.Length>0)
					{
						m=m.Nodes[int.Parse(y)];
					}
				}
				m.EnsureVisible();
				BookmarksForm.tvwbm.SelectedNode=m;
			}
			catch
			{
			}
		} catch {}
		}

		public static void ImportRefs(string filename) {
			XmlTextReader xr=new XmlTextReader(filename);
			try {
				xr.Read();
				xr.ReadStartElement("References");

				//first process the different reference types
				xr.ReadStartElement("Types");
				while (xr.IsStartElement("Type")) {
					xr.ReadStartElement("Type");
					string k= xr.ReadElementString("Name");
					string r1=xr.ReadElementString("Red");
					string r2=xr.ReadElementString("Green");
					string r3=xr.ReadElementString("Blue");
					RefTypes.Add(new RefType(k,int.Parse(r1),int.Parse(r2),int.Parse(r3)));
					xr.ReadEndElement();
				}

				//TODO: worry about reading types



				//next fill in the cross-refs
				Regex reg=new Regex(@"(?:Gn|Ex|Lv|Nm|Dt|Jos|Jdc|Rt|1Rg|2Rg|3Rg|4Rg|1Par|2Par|Esr|Neh|Tob|Jdt|Est|Job|Ps|Pr|Ecl|Ct|Sap|Sir|Is|Jr|Lam|Bar|Ez|Dn|Os|Joel|Am|Abd|Jon|Mch|Nah|Hab|Soph|Agg|Zach|Mal|1Mcc|2Mcc|Mt|Mc|Lc|Jo|Act|Rom|1Cor|2Cor|Gal|Eph|Phlp|Col|1Thes|2Thes|1Tim|2Tim|Tit|Phlm|Hbr|Jac|1Ptr|2Ptr|1Jo|2Jo|3Jo|Jud|Apc) \d{1,3}:{0,1}");
				while (xr.IsStartElement("ChapterRefs")) {
					xr.ReadStartElement("ChapterRefs");
					string k= xr.ReadElementString("BaseBookChap");
					ArrayList a=new ArrayList();
					if (hashXref[k]!=null) a=(ArrayList) hashXref[k];
					while (xr.IsStartElement("Crossref")) {
						xr.ReadStartElement("Crossref");
						string r=xr.ReadElementString("Verses");
						string q=xr.ReadElementString("Ref");
						string d=xr.ReadElementString("Desc");
						string p=xr.ReadElementString("Type");
						if (p=="Cross reference") p=rm.GetString("crossreference");
						if (p=="Footnote") p=rm.GetString("footnote");
						NewRef t=ParseNewRef(r,q,d,p,k);
						if (!a.Contains(t)) a.Add(t);
						xr.ReadEndElement();
					}
					hashXref[k]=a;
					xr.ReadEndElement();
				}
			}
			catch (Exception e) {
				MessageBox.Show(rm.GetString("errorgeneral") + ": "+e.Message);
			}
			finally {
				xr.Close();
			}
		}

		public static NewRef ParseNewRef(string verses, string rf, string desc, string type, string k)
		{
			try {
			Regex reg=new Regex(@"(?:Gn|Ex|Lv|Nm|Dt|Jos|Jdc|Rt|1Rg|2Rg|3Rg|4Rg|1Par|2Par|Esr|Neh|Tob|Jdt|Est|Job|Ps|Pr|Ecl|Ct|Sap|Sir|Is|Jr|Lam|Bar|Ez|Dn|Os|Joel|Am|Abd|Jon|Mch|Nah|Hab|Soph|Agg|Zach|Mal|1Mcc|2Mcc|Mt|Mc|Lc|Jo|Act|Rom|1Cor|2Cor|Gal|Eph|Phlp|Col|1Thes|2Thes|1Tim|2Tim|Tit|Phlm|Hbr|Jac|1Ptr|2Ptr|1Jo|2Jo|3Jo|Jud|Apc) \d{1,3}:{0,1}");
			Match m=reg.Match(rf);
			if (m.Success)
			{
				string u=m.Value;
				if (!u.EndsWith(":"))
					u+=":";
				hashReverseRef[u.Substring(0,u.Length-1)]=
					(hashReverseRef[u.Substring(0,u.Length-1)] as string ) + ";" + k;
			}
		} catch { }
			return new NewRef(verses,rf,desc,type);
		}

		
		private static void ReadBookmarkXML(ref XmlTextReader xr, TreeNode n)
		{
			try {
			while (!xr.EOF)
			{

				try
				{
					xr.ReadStartElement("Node");
					TreeNode m=new TreeNode(xr.ReadElementString("NodeName"));
					if (haveReadRootBookmark)
					{
						n.Nodes.Add(m);
						n=m;
					}
					else
					{
						haveReadRootBookmark=true;
					}
					BookmarksForm.hashRef[n.GetHashCode()]=xr.ReadElementString("Ref");
					BookmarksForm.hashText[n.GetHashCode()]=xr.ReadElementString("Text");
				}
				catch
				{
					//no more sub-nodes
					xr.ReadEndElement();
					n=n.Parent;
				}
			}
		} catch {}
		}
		
		private static void WriteBookmarkXML(ref XmlTextWriter xr, TreeNode n)
		{
			try {
			xr.WriteStartElement("Node");
			xr.WriteElementString("NodeName",n.Text);
			xr.WriteElementString("Ref",(string) BookmarksForm.hashRef[n.GetHashCode()]);
			xr.WriteElementString("Text",(string) BookmarksForm.hashText[n.GetHashCode()]);
			foreach (TreeNode m in n.Nodes)
			{
				WriteBookmarkXML(ref xr,m);
			}
			xr.WriteEndElement();
		} catch {}
		}

		public static void WriteSettings()
		{
			try {
                if (troubleshootingModeDontWriteSettings)
                    return;
			RegistryKey key = Registry.CurrentUser.OpenSubKey(
				"Software\\VulSearch4\\VulSearch4.2",true); 
			key.SetValue("Path",MainForm.AppPath().ToLower());
			key.SetValue("CurTxtVer",CurTxtVer.ToString());

            MainForm.restore_all_forms();

			key.SetValue("MainNewTop",(MainForm.Top).ToString());
			key.SetValue("MainNewLeft",(MainForm.Left).ToString());
			key.SetValue("MainNewHeight",(MainForm.Height).ToString());
			key.SetValue("MainNewWidth",(MainForm.Width).ToString());

            key.SetValue("BookmarksTop", (BookmarksForm.Top).ToString());
            key.SetValue("BookmarksLeft", (BookmarksForm.Left).ToString());
            key.SetValue("BookmarksHeight", (BookmarksForm.Height).ToString());
            key.SetValue("BookmarksWidth", (BookmarksForm.Width).ToString());
            key.SetValue("BookmarksVisible", (BookmarksForm.Visible).ToString());

            key.SetValue("SearchTop", (SearchForm.Top).ToString());
            key.SetValue("SearchLeft", (SearchForm.Left).ToString());
            key.SetValue("SearchHeight", (SearchForm.Height).ToString());
            key.SetValue("SearchWidth", (SearchForm.Width).ToString());
            key.SetValue("SearchVisible", (SearchForm.Visible).ToString());

            key.SetValue("RefsTop", (RefsForm.Top).ToString());
            key.SetValue("RefsLeft", (RefsForm.Left).ToString());
            key.SetValue("RefsHeight", (RefsForm.Height).ToString());
            key.SetValue("RefsWidth", (RefsForm.Width).ToString());
            key.SetValue("RefsVisible", (RefsForm.Visible).ToString());

            key.SetValue("HistoryTop", (HistoryForm.Top).ToString());
            key.SetValue("HistoryLeft", (HistoryForm.Left).ToString());
            key.SetValue("HistoryHeight", (HistoryForm.Height).ToString());
            key.SetValue("HistoryWidth", (HistoryForm.Width).ToString());
            key.SetValue("HistoryVisible", (HistoryForm.Visible).ToString());

            key.SetValue("NotesTop", (NotesForm.Top).ToString());
            key.SetValue("NotesLeft", (NotesForm.Left).ToString());
            key.SetValue("NotesHeight", (NotesForm.Height).ToString());
            key.SetValue("NotesWidth", (NotesForm.Width).ToString());
            key.SetValue("NotesVisible", (NotesForm.Visible).ToString());

            key.SetValue("ResultsTop", (ResultsForm.Top).ToString());
            key.SetValue("ResultsLeft", (ResultsForm.Left).ToString());
            key.SetValue("ResultsHeight", (ResultsForm.Height).ToString());
            key.SetValue("ResultsWidth", (ResultsForm.Width).ToString());
            key.SetValue("ResultsVisible", (ResultsForm.Visible).ToString());

            key.SetValue("WordsTop", (WordsForm.Top).ToString());
            key.SetValue("WordsLeft", (WordsForm.Left).ToString());
            key.SetValue("WordsHeight", (WordsForm.Height).ToString());
            key.SetValue("WordsWidth", (WordsForm.Width).ToString());
            key.SetValue("WordsVisible", (WordsForm.Visible).ToString());

            key.SetValue("WindowsLocked", (MainForm.toolLock.Checked).ToString());

			key.SetValue("LeftBible",MainForm.LeftBible);
			key.SetValue("RightBible",MainForm.RightBible);
			int i=MainForm.LastBook;
			key.SetValue("LastBook",i.ToString() );
			i=MainForm.LastChapter;
			key.SetValue("LastChapter",i.ToString() );
			i=MainForm.LastVerse;
			key.SetValue("LastVerse",i.ToString() );

			key.SetValue("SearchBMTop",(SearchBMForm.Top).ToString());
			key.SetValue("SearchBMLeft",(SearchBMForm.Left).ToString());
			key.SetValue("SearchBMHeight",(SearchBMForm.Height).ToString());
			key.SetValue("SearchBMWidth",(SearchBMForm.Width).ToString());
			key.SetValue("SearchBMCol0", SearchBMForm.lvres.Columns[0].Width.ToString());
			key.SetValue("SearchBMCol1", SearchBMForm.lvres.Columns[1].Width.ToString());
			key.SetValue("SearchBMCol2", SearchBMForm.lvres.Columns[2].Width.ToString());

			key.SetValue("RefsByVerse", RefsForm.radVerse.Checked.ToString());
			key.SetValue("RefsType", RefsForm.cmbType.SelectedIndex.ToString());

			key.SetValue("MainFontName",OptionsForm.fontMain.Font.Name );
			key.SetValue("MainFontStyle",((int) OptionsForm.fontMain.Font.Style ).ToString());
			key.SetValue("MainFontSize",OptionsForm.fontMain.Font.Size.ToString());

			key.SetValue("VerseFontName",OptionsForm.fontNumbers.Font.Name );
			key.SetValue("VerseFontStyle",((int) OptionsForm.fontNumbers.Font.Style ).ToString());
			key.SetValue("VerseFontSize",OptionsForm.fontNumbers.Font.Size.ToString());

			key.SetValue("VerseSuper",OptionsForm.chkVerseSuper.Checked.ToString());
			key.SetValue("Poetry", OptionsForm.chkPoetry.Checked.ToString());
			key.SetValue("Paragraphs", OptionsForm.radParagraphs.Checked.ToString());

			key.SetValue("Missals",  OptionsForm.radMissals.Checked.ToString());
			key.SetValue("Printed",OptionsForm.radPrinted.Checked.ToString());
			key.SetValue("Classical",OptionsForm.radClassical.Checked.ToString());
			key.SetValue("Rules",OptionsForm.radRules.Checked.ToString());
			key.SetValue("IJ",OptionsForm.chkIJ.Checked.ToString());
			key.SetValue("UV",OptionsForm.chkUV.Checked.ToString());
			key.SetValue("AE",OptionsForm.chkAE.Checked.ToString());
			key.SetValue("Accent",OptionsForm.chkAccent.Checked.ToString());
			key.SetValue("WordsTooltip",OptionsForm.chkWordsTooltip.Checked.ToString());
			key.SetValue("NoWords",OptionsForm.chkNoWords.Checked.ToString());

			key.SetValue("StoreSearch",OptionsForm.chkStoreSearch.Checked.ToString());
			key.SetValue("SaveSearch",OptionsForm.chkSaveSearch.Checked.ToString());
			key.SetValue("SaveHistory", OptionsForm.chkSaveHistory.Checked.ToString());
			key.SetValue("ParallelScroll", OptionsForm.chkParallelScroll.Checked.ToString());
			key.SetValue("Internet", OptionsForm.chkInternet.Checked.ToString());
			key.SetValue("nudHistory",((int) OptionsForm.nudHistory.Value).ToString());

			
			for (int j=1; j<=50 ; j++)
			{
				key.SetValue("History"+j.ToString(),"");
			}
			if (OptionsForm.chkSaveHistory.Checked)
			{
				for (int j=0; j<HistoryForm.HistoryBox.Items.Count;j++)
				{
					key.SetValue("History"+(j+1).ToString(),
						HistoryForm.HistoryBox.Items[j].ToString());
				}
			}
			for (int j=1; j<=10 ; j++)
			{
				key.SetValue("Search"+j.ToString(),"");
			}
			if (OptionsForm.chkSaveSearch.Checked)
			{
				for (int j=0; j<10;j++)
				{
					key.SetValue("Search"+(j+1).ToString(),
						(SearchForm.cmbSearch.Items.Count>j) ?
						SearchForm.cmbSearch.Items[j].ToString() : "");
				}
			}
			key.SetValue("BibleSearching", (Bibles[SearchForm.cmbBible.SelectedIndex] as Bible).Extension);

			// save bookmarks
			TreeNode n=BookmarksForm.tvwbm.SelectedNode;
			ArrayList a=new ArrayList();
			
			while (n!=null && n!=BookmarksForm.tvwbm.Nodes[0])
			{
				a.Add(n.Index.ToString());
				n=n.Parent;
			}
			string lastBM="";
			for (int j=a.Count-1; j>=0; j--)
			{
				lastBM += ((string) a[j])+"\\";
			}
			key.SetValue("LastBM",lastBM);
			key.SetValue("FirstRun","false");
			key.SetValue("LastRefType",RefsForm.cmbType.SelectedIndex.ToString());
			

			/* New in 4.1.6: make backups before overwriting config files */
			File.Copy(Startup.DataPath+"\\bookmarks.xml",Startup.DataPath+"\\bookmarks.xml.bak",true);
			XmlTextWriter xr=new XmlTextWriter(Startup.DataPath+"\\bookmarks.xml",System.Text.Encoding.UTF8);
			xr.Formatting=Formatting.Indented;
			xr.Indentation=4;
			xr.WriteStartDocument();
			WriteBookmarkXML(ref xr,BookmarksForm.tvwbm.Nodes[0]);
			xr.WriteEndDocument();
			xr.Flush();
			xr.Close();

			File.Copy(Startup.DataPath+"\\notes.xml",Startup.DataPath+"\\notes.xml.bak",true);
			xr=new XmlTextWriter(Startup.DataPath+"\\notes.xml",System.Text.Encoding.UTF8);
			xr.Formatting=Formatting.Indented;
			xr.Indentation=4;
			xr.WriteStartDocument();
			xr.WriteStartElement("Notes");
			foreach (DictionaryEntry o in hashNotes)
			{
				if ((string) o.Value != "")
				{
					xr.WriteStartElement("Note");
					xr.WriteElementString("Key",(string) o.Key);
					xr.WriteElementString("Value",(string) o.Value);
					xr.WriteEndElement();
				}
			}
			xr.WriteEndElement();
			xr.WriteEndDocument();
			xr.Flush();
			xr.Close();

			//Write references
			File.Copy(Startup.DataPath+"\\refs.xml",Startup.DataPath+"\\refs.xml.bak",true);
			ExportRefs(Startup.DataPath+"\\refs.xml");

			key.SetValue("ImportedOldRefs","true");

			key.Close();
		} catch {}
		}

		public static void ExportRefs(string filename)
		{
			try {
			try
			{
				XmlTextWriter xr=new XmlTextWriter(filename,System.Text.Encoding.UTF8);
				xr.Formatting=Formatting.Indented;
				xr.Indentation=4;
				xr.WriteStartDocument();
				xr.WriteStartElement("References");
				xr.WriteStartElement("Types");
/*
				foreach (RefType t in Startup.RefTypes)
				{
					if (t.Name !=rm.GetString("crossreference") && t.Name != rm.GetString("footnote"))
					{
						xr.WriteStartElement("Type");
						xr.WriteElementString("Name",t.Name);
						xr.WriteElementString("Red",t.Colour.R.ToString());
						xr.WriteElementString("Green",t.Colour.G.ToString());
						xr.WriteElementString("Blue",t.Colour.B.ToString());
						xr.WriteEndElement();
					}
				}
*/
				xr.WriteEndElement();


				foreach (DictionaryEntry o in hashXref)
				{
					if (o.Value!=null)
					{
						xr.WriteStartElement("ChapterRefs");
						xr.WriteElementString("BaseBookChap",(string) o.Key);
						ArrayList x=o.Value as ArrayList;
						if (x==null) continue;
						foreach (object p in x)
						{
							NewRef y=(NewRef) p;
							xr.WriteStartElement("Crossref");
							xr.WriteElementString("Verses",(string) y.Verses);
							xr.WriteElementString("Ref",(string) y.Ref);
							xr.WriteElementString("Desc",(string) y.Desc);
							string v=(string) y.Type;
							if (v==rm.GetString("crossreference")) v="Cross reference";
							if (v==rm.GetString("footnote")) v="Footnote";
							xr.WriteElementString("Type",v);
							xr.WriteEndElement();
						}
						xr.WriteEndElement();
					}
				}
				xr.WriteEndElement();
				xr.WriteEndDocument();
				xr.Flush();
				xr.Close();
			}
			catch (Exception e)
			{
				MessageBox.Show(rm.GetString("errorgeneral") + ": "+e.Message);
			}
		} catch {}
		}

		private static void ReadOldRefs()
		{
			try {
			if (File.Exists(DataPath+"\\xrefs.xml"))
			{
				XmlTextReader xr=new XmlTextReader(DataPath+"\\xrefs.xml");
				xr.Read();
				xr.ReadStartElement("Crossrefs");
				while (xr.IsStartElement("Chapter"))
				{
					xr.ReadStartElement("Chapter");
					string k= xr.ReadElementString("Key");
					ArrayList a=new ArrayList();
					if (hashXref[k]!=null) a=(ArrayList) hashXref[k];
					while (xr.IsStartElement("Crossref"))
					{
						xr.ReadStartElement("Crossref");
						string r=xr.ReadElementString("Ref");
						string d=xr.ReadElementString("Desc");
						a.Add(ParseNewRef("1",r,d,"Cross reference",k));
						xr.ReadEndElement();
					}
					hashXref[k]=a;
					xr.ReadEndElement();
				}
				xr.Close();
			}
		} catch {}
		}


    public static void CheckFormPositionsAreSensible()
{
     Form[] frms = {	SearchForm, BookmarksForm, MainForm, RefsForm, NotesForm, ResultsForm, HistoryForm, WordsForm };
     int sw = Screen.PrimaryScreen.Bounds.Width;
     int sh = Screen.PrimaryScreen.Bounds.Height;
     foreach (Form f in frms)
     {
         if (f.WindowState == FormWindowState.Normal)
         {
             if (f.Bottom < 0)
             {
                 f.Top = 0;
             }
             if (f.Top > sh)
             {
                 f.Top = sh - 100;
             }
             if (f.Height < 25)
                 f.Height = 25;

             if (f.Right < 0)
             {
                 f.Left = 0;
             }
             if (f.Left > sw)
             {
                 f.Left = sw - 100;
             }
             if (f.Width < 25)
                 f.Width = 25;

         }
     }
}
        	}

}
