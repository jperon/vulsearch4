﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace VulSearch4
{
    public partial class WordsForm : Form
    {
        public WordsForm()
        {
		try {
            InitializeComponent();
	} catch {}
        }

        private void WordsForm_VisibleChanged(object sender, EventArgs e)
        {
		try {
            Startup.MainForm.toolWords.Checked = this.Visible;
	} catch {}
        }

        private void WordsForm_FormClosing(object sender, FormClosingEventArgs e)
        {
		try {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                this.Hide();
                e.Cancel = true;
            }
	} catch {}
        }

    }
}
