﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace VulSearch4
{
    public partial class HistoryForm : Form
    {
        public HistoryForm()
        {
		try {
            InitializeComponent();
	} catch {}
        }

        private void HistoryForm_VisibleChanged(object sender, EventArgs e)
        {
		try {
            Startup.MainForm.toolHistory.Checked = this.Visible;
	} catch {}
        }

        private void HistoryForm_FormClosing(object sender, FormClosingEventArgs e)
        {
		try {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                this.Hide();
                e.Cancel = true;
            }
	} catch {}
        }

        private void HistoryBox_DoubleClick(object sender, EventArgs e)
        {
		try {
            Startup.MainForm.Box_DoubleClick(sender, e);
	} catch {}
        }

        private void HistoryBox_MouseDown(object sender, MouseEventArgs e)
        {
		try {
            Startup.MainForm.Box_MouseDown(sender, e);

	} catch {}
        }

        private void HistoryBox_MouseMove(object sender, MouseEventArgs e)
        {
		try {
            Startup.MainForm.Box_MouseMove(sender, e);

	} catch {}
        }

        private void HistoryBox_MouseUp(object sender, MouseEventArgs e)
        {
		try {
            Startup.MainForm.Box_MouseUp(sender, e);

	} catch {}
        }

    }
}
