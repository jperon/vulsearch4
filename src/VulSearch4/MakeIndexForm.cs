using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.Xml;

namespace VulSearch4
{
	/// <summary>
	/// Summary description for MakeIndexForm.
	/// </summary>
	public class MakeIndexForm : System.Windows.Forms.Form
	{
		private System.Windows.Forms.FolderBrowserDialog fldBrows;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnNext;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ProgressBar prg1;
		private System.Windows.Forms.Button btnRetry;
		private System.Windows.Forms.Label lblIndexQuery;
		private System.Windows.Forms.Button btnBuild;
		private System.Windows.Forms.Button btnSkipBuild;
		private System.Windows.Forms.Label lblProg;
		private System.Windows.Forms.ProgressBar prg2;
		private System.Windows.Forms.Label lblDone;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		private string path,ext,desc,name;
		private System.Windows.Forms.Label lblFirst;
		private bool lat,cancelFlag=false;
		private System.Windows.Forms.Label lblDoneNoIndex;

		public MakeIndexForm(string Path,string Ext, bool Lat, string Desc,string Name)
		{
			try {
			path=Path;
			ext=Ext;
			if (ext.StartsWith(".")) ext=ext.Substring(1);
			lat=Lat;
			desc=Desc;
			name=Name;

			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		} catch {}
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			try {
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		} catch {}
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MakeIndexForm));
            this.fldBrows = new System.Windows.Forms.FolderBrowserDialog();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnNext = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.lblFirst = new System.Windows.Forms.Label();
            this.prg1 = new System.Windows.Forms.ProgressBar();
            this.lblIndexQuery = new System.Windows.Forms.Label();
            this.btnRetry = new System.Windows.Forms.Button();
            this.btnBuild = new System.Windows.Forms.Button();
            this.btnSkipBuild = new System.Windows.Forms.Button();
            this.lblProg = new System.Windows.Forms.Label();
            this.prg2 = new System.Windows.Forms.ProgressBar();
            this.lblDone = new System.Windows.Forms.Label();
            this.lblDoneNoIndex = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // fldBrows
            // 
            this.fldBrows.ShowNewFolderButton = false;
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            resources.ApplyResources(this.btnCancel, "btnCancel");
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnNext
            // 
            resources.ApplyResources(this.btnNext, "btnNext");
            this.btnNext.Name = "btnNext";
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.label1);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // lblFirst
            // 
            resources.ApplyResources(this.lblFirst, "lblFirst");
            this.lblFirst.Name = "lblFirst";
            // 
            // prg1
            // 
            resources.ApplyResources(this.prg1, "prg1");
            this.prg1.Maximum = 1328;
            this.prg1.Name = "prg1";
            // 
            // lblIndexQuery
            // 
            resources.ApplyResources(this.lblIndexQuery, "lblIndexQuery");
            this.lblIndexQuery.Name = "lblIndexQuery";
            // 
            // btnRetry
            // 
            resources.ApplyResources(this.btnRetry, "btnRetry");
            this.btnRetry.Name = "btnRetry";
            this.btnRetry.Click += new System.EventHandler(this.btnRetry_Click);
            // 
            // btnBuild
            // 
            resources.ApplyResources(this.btnBuild, "btnBuild");
            this.btnBuild.Name = "btnBuild";
            this.btnBuild.Click += new System.EventHandler(this.btnBuild_Click);
            // 
            // btnSkipBuild
            // 
            resources.ApplyResources(this.btnSkipBuild, "btnSkipBuild");
            this.btnSkipBuild.Name = "btnSkipBuild";
            this.btnSkipBuild.Click += new System.EventHandler(this.btnSkipBuild_Click);
            // 
            // lblProg
            // 
            resources.ApplyResources(this.lblProg, "lblProg");
            this.lblProg.Name = "lblProg";
            // 
            // prg2
            // 
            resources.ApplyResources(this.prg2, "prg2");
            this.prg2.Maximum = 1328;
            this.prg2.Name = "prg2";
            this.prg2.Value = 1328;
            // 
            // lblDone
            // 
            resources.ApplyResources(this.lblDone, "lblDone");
            this.lblDone.Name = "lblDone";
            // 
            // lblDoneNoIndex
            // 
            resources.ApplyResources(this.lblDoneNoIndex, "lblDoneNoIndex");
            this.lblDoneNoIndex.Name = "lblDoneNoIndex";
            // 
            // MakeIndexForm
            // 
            this.AcceptButton = this.btnNext;
            resources.ApplyResources(this, "$this");
            this.ControlBox = false;
            this.Controls.Add(this.lblDone);
            this.Controls.Add(this.prg2);
            this.Controls.Add(this.lblProg);
            this.Controls.Add(this.btnSkipBuild);
            this.Controls.Add(this.btnBuild);
            this.Controls.Add(this.btnRetry);
            this.Controls.Add(this.prg1);
            this.Controls.Add(this.lblFirst);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lblIndexQuery);
            this.Controls.Add(this.lblDoneNoIndex);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "MakeIndexForm";
            this.ShowInTaskbar = false;
            this.Load += new System.EventHandler(this.MakeIndexForm_Load);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

		}
		#endregion

		private void btnRetry_Click(object sender, System.EventArgs e)
		{
			try {
			CheckFiles();
		} catch {}
		}

		private void CheckFiles()
		{
			try {
			prg1.Value=0;
			Application.DoEvents();
			bool error=false;
			Regex reg=new Regex(@"^\d{1,3}:\d{1,3} \S");
			for (byte index=0; index<73; index++)
			{
				if (!File.Exists(path+"\\"
					+ Bible.Abbrev[index] + "." + ext))
				{
					MessageBox.Show(Startup.rm.GetString("filenotexist") + path+"\\"
						+ Bible.Abbrev[index] + "." + ext +
						Startup.rm.GetString("filenotexist2"), "VulSearch 4",
						MessageBoxButtons.OK );
					btnRetry.Visible=true;
					error=true;
					break;
				}
				byte chapter=1;
				string x,y=null;
				try
				{
					StreamReader reader;
					FileStream file = new FileStream(path+"\\"
						+ Bible.Abbrev[index] + "." + ext,
						FileMode.Open,FileAccess.Read);
					reader = new StreamReader(file, 
						System.Text.Encoding.GetEncoding(1252));

					x=reader.ReadLine();
					while (x != null)
					{
						if (!reg.Match(x).Success)
						{
							if (y!=null)
							{
								MessageBox.Show(Startup.rm.GetString("linenotvalid")+
									Bible.Abbrev[index] + "." + ext +
									Startup.rm.GetString("linenotvalid2")+ y +
									Startup.rm.GetString("linenotvalid3"), "VulSearch 4",MessageBoxButtons.OK);
							}
							else
							{
								MessageBox.Show(Startup.rm.GetString("firstlinenotvalid")+
									Bible.Abbrev[index] + "." + ext +
									Startup.rm.GetString("firstlinenotvalid2"), "VulSearch 4",MessageBoxButtons.OK);
							}
							error=true;
							break;
						}

						if (x.StartsWith(chapter.ToString()))
						{
						}
						else if (x.StartsWith((chapter+1).ToString()))
						{
							chapter++;
							prg1.Value++;
							Application.DoEvents();
						}
						else
						{
							MessageBox.Show(Startup.rm.GetString("chapterjump")+
								Bible.Abbrev[index] + "." + ext +
								Startup.rm.GetString("chapterjump2")+x, "VulSearch 4",MessageBoxButtons.OK);
							error=true;
							break;
						}
						y=x;
						x=reader.ReadLine();
					}
					reader.Close();
					if (error) break;
					if (path.ToLower() != (Startup.DataPath+"\\text").ToLower())
					{
						File.Copy(path+"\\"
							+ Bible.Abbrev[index] + "." + ext,
							Startup.DataPath+"\\text\\"+
							Bible.Abbrev[index] + "." + ext,true);
					}
				}

				catch (Exception e)
				{
					MessageBox.Show(Startup.rm.GetString("adminerror")+
						Bible.Abbrev[index] + "." + ext +":\n" +
						e.Message+Startup.rm.GetString("adminerror2"),
						"VulSearch 4",MessageBoxButtons.OK);
					error=true;
					break;
				}
			}
			if (error)
			{
				prg1.Value=0;
				btnRetry.Visible=true;
			}
			else
			{
				btnRetry.Visible=false;
				lblFirst.Enabled=false;
				prg1.Visible=false;
#if DEBUG
                string idxpath = MainForm.AppPath() + "\\src\\swish\\" + ext + ".idx";
#else
            string idxpath = MainForm.AppPath()+"\\swish\\" + ext +".idx";
#endif
				if (File.Exists(idxpath))
				{
					// index is already present
					lblDoneNoIndex.Visible=true;
					btnBuild.Text=Startup.rm.GetString("rebuildindex");
					btnBuild.Visible=true;
					btnNext.Enabled=true;
					btnNext.Focus();
				}
				else
				{
					lblIndexQuery.Visible=true;
					btnBuild.Visible=true;
					btnSkipBuild.Visible=true;
				}
			}

		} catch {}
		}

		private void MakeIndexForm_Load(object sender, System.EventArgs e)
		{
			try {
			this.Show();
			this.Focus();
			Application.DoEvents();
			CheckFiles();
		} catch {}
		}

		private void btnNext_Click(object sender, System.EventArgs e)
		{
			try {
			
			// write the new Bible info to an xml file
			XmlTextWriter xr=new XmlTextWriter(Startup.DataPath+"\\text\\" + ext+".xml",System.Text.Encoding.UTF8);
			xr.Formatting=Formatting.Indented;
			xr.Indentation=4;
			xr.WriteStartDocument();

			xr.WriteStartElement("Bible");
			xr.WriteElementString("Name",name);
			xr.WriteElementString("IsLatin",lat.ToString());
			xr.WriteElementString("Description",desc);
			xr.WriteEndElement();

			xr.WriteEndDocument();
			xr.Flush();
			xr.Close();
			
			// add in new Bible
			bool b=false;
			foreach (Bible c in Startup.Bibles)
			{
				if (c.Extension==ext)
				{
					b=true;
					break;
				}
			}
			if (!b)
			{
				Startup.Bibles.Add(new Bible(name,ext,desc,lat));
				Startup.MainForm.cmbLeft.Items.Add(name);
				Startup.MainForm.cmbRight.Items.Add(name);
				Startup.SearchForm.cmbBible.Items.Add(name);
			}

			this.Hide();
		} catch {}
		}

		private void btnSkipBuild_Click(object sender, System.EventArgs e)
		{
			try {
			lblDone.Visible=true;
			lblIndexQuery.Enabled=false;
			btnBuild.Enabled=false;
			btnSkipBuild.Enabled=false;
			btnNext.Enabled=true;
		} catch {}
		}

		private void btnBuild_Click(object sender, System.EventArgs e)
		{
			try {
			lblProg.Visible=true;
			prg2.Visible=true;
			btnBuild.Visible=false;
			btnSkipBuild.Visible=false;
			btnNext.Enabled=false;
			prg2.Value=0;

			// start by finding a temporary directory for the files
			// to be indexed. Must end in "\text"
			string dr=path.Substring(0,1),temp;
			if (Directory.Exists(dr+@":\text"))
			{
				int i=0;
				while (Directory.Exists(dr+@":\"+i.ToString()+"\\text"))
				{
					i++;
				}
				temp=dr+@":\"+i.ToString()+"\\text\\";
			}
			else
			{
				temp=dr+@":\text\";
			}
			Directory.CreateDirectory(temp);

			for (byte index=0; index<73; index++)
			{
				string cd=temp +(index+1).ToString("000");
				Directory.CreateDirectory(cd);
				byte chapter=1;
				lblProg.Text=Bible.ShortName[index]+" 1";
				Application.DoEvents();
				string x,y= "\\001-";
				try
				{
					StreamReader reader;
					FileStream file = new FileStream(path+"\\"
						+ Bible.Abbrev[index] + "." + ext,
						FileMode.Open,FileAccess.Read);
					reader = new StreamReader(file, 
						System.Text.Encoding.GetEncoding(1252));

					x=reader.ReadLine();
					while (x != null)
					{

						if (x.StartsWith(chapter.ToString()))
						{
						}
						else
						{
							chapter++;
							prg2.Value++;
							lblProg.Text=Bible.ShortName[index]+" "+
								chapter.ToString();
							Application.DoEvents();
							y="\\"+ chapter.ToString("000")+"-";
						}
						string z=x.Substring(0,x.IndexOf(" "));
						z=z.Substring(z.IndexOf(":")+1);
						x=x.Substring(x.IndexOf(" ")+1);
						if (lat)
						{
							x=x.Replace("j","i");
							x=x.Replace("J","I");
							x=x.Replace("v","u");
							x=x.Replace("V","U");
							x=x.Replace("�","e");
							x=x.Replace("�","ae");
							x=x.Replace("�","oe");
							x=x.Replace("�","Ae");
							x=x.Replace("�","Oe");
						}
						x=x.Replace(",","");
						x=x.Replace(".","");
						x=x.Replace("(","");
						x=x.Replace(")","");
						x=x.Replace(";","");
						x=x.Replace(":","");
						x=x.Replace("!","");
						x=x.Replace("?","");
						x=x.Replace("\\","");
						x=x.Replace("/","");
						x=x.Replace("[","");
						x=x.Replace("]","");
						x=x.Replace("<","");
						x=x.Replace(">","");

						StreamWriter sw=new StreamWriter(cd + y+(int.Parse(z)).ToString("000"),false,System.Text.Encoding.GetEncoding(1252));
						sw.WriteLine(x);
						sw.Close();
						if (cancelFlag)
						{
							reader.Close();
							Directory.Delete(temp,true);
							this.Hide();
							break;
						}

						x=reader.ReadLine();
					}
					reader.Close();
				}

				catch (Exception ex)
				{
					MessageBox.Show(Startup.rm.GetString("errorprocessing")+
						Bible.Abbrev[index] + "." + ext +":\n" +
						ex.Message, "VulSearch 4",MessageBoxButtons.OK);
					break;
				}
				if (cancelFlag) break;
			}

			lblProg.Text=Startup.rm.GetString("runningswish");
			prg2.Visible=false;
			Process swish = new Process();
#if DEBUG
			swish.StartInfo.WorkingDirectory = MainForm.AppPath() + "\\src\\Swish";
			swish.StartInfo.FileName = MainForm.AppPath() + "\\src\\Swish\\swish-e" ;
#else
			swish.StartInfo.WorkingDirectory = MainForm.AppPath() + "\\Swish";
			swish.StartInfo.FileName = MainForm.AppPath() + "\\Swish\\swish-e" ;
#endif
			swish.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Normal ;
			swish.StartInfo.Arguments = "-i "+temp+" -f " + ext +".idx -v 2" ;
			swish.StartInfo.UseShellExecute = true;
			swish.Start();
			Application.DoEvents();
			//swish.WaitForExit();
			while (!swish.HasExited)
				Application.DoEvents();

            lblProg.Text=Startup.rm.GetString("deltemp");
			Application.DoEvents();
			try
			{
				Directory.Delete(temp,true);
			}
			catch
			{
				//TO DO - do something!!
			}

			if (btnBuild.Text==Startup.rm.GetString("rebuildindex")) lblDone.Text=Startup.rm.GetString("successrebuild");
			lblDone.Visible=true;
			lblIndexQuery.Enabled=false;
			lblDoneNoIndex.Enabled=false;
			lblProg.Visible=false;
			btnBuild.Visible=true;
			btnSkipBuild.Visible=true;
			btnBuild.Enabled=false;
			btnSkipBuild.Enabled=false;
			btnNext.Enabled=true;
			btnNext.Focus();
		} catch {}
		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			try {
			cancelFlag=true;
		} catch {}
		}

	}
}
