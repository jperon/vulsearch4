﻿namespace VulSearch4
{
    partial class WordsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rtbWords = new VulSearch4.XRichTextBox();
            this.SuspendLayout();
            // 
            // rtbWords
            // 
            this.rtbWords.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.rtbWords.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtbWords.Location = new System.Drawing.Point(0, 0);
            this.rtbWords.Name = "rtbWords";
            this.rtbWords.ReadOnly = true;
            this.rtbWords.Size = new System.Drawing.Size(284, 261);
            this.rtbWords.TabIndex = 0;
            this.rtbWords.Text = "";
            // 
            // WordsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.rtbWords);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "WordsForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Whitaker\'s Words";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.WordsForm_FormClosing);
            this.VisibleChanged += new System.EventHandler(this.WordsForm_VisibleChanged);
            this.ResumeLayout(false);

        }

        #endregion

        public XRichTextBox rtbWords;
    }
}