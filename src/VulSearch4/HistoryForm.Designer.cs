﻿namespace VulSearch4
{
    partial class HistoryForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.HistoryBox = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // HistoryBox
            // 
            this.HistoryBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.HistoryBox.FormattingEnabled = true;
            this.HistoryBox.Location = new System.Drawing.Point(0, 0);
            this.HistoryBox.Name = "HistoryBox";
            this.HistoryBox.Size = new System.Drawing.Size(284, 261);
            this.HistoryBox.TabIndex = 0;
            this.HistoryBox.DoubleClick += new System.EventHandler(this.HistoryBox_DoubleClick);
            this.HistoryBox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.HistoryBox_MouseDown);
            this.HistoryBox.MouseMove += new System.Windows.Forms.MouseEventHandler(this.HistoryBox_MouseMove);
            this.HistoryBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.HistoryBox_MouseUp);
            // 
            // HistoryForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.HistoryBox);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "HistoryForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "History";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.HistoryForm_FormClosing);
            this.VisibleChanged += new System.EventHandler(this.HistoryForm_VisibleChanged);
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.ListBox HistoryBox;


    }
}