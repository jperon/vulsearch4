1:1 RAB., expos. in lib. Esther, tom. 3. Liber Esther quem Hebraei inter hagiographa numerant, Christi et Ecclesiae continet sacramenta, etc., usque ad qui regnavit post Darium patruum cognomento Nochum annis quadraginta.
1:2 <Susan civitas,>etc. Susan metropolis est in Perside, quam aiunt historici Memnonis fratrem constituisse, et a Susi fluvio nomen accepisse. Ibi est regia domus Cyri lapide candido et vario, columnis aureis, et laquearibus gemmisque distincta: coeli continens simulacrum stellis micantibus insignitum, et incredibilia multa. Ibi Assuerus convivium maximum divitiis, et copiosum deliciis celebravit. Virtus namque sacri eloquii, sic aliquando transacta narrat, ut ventura exprimat: sic factorem approbat, ut ei in ministerio contradicat: sic gesta damnat, ut haec mystice gerenda suadeat.
1:3 <Tertio igitur anno.>Tempore, scilicet, istius saeculi incarnationis suae sacramentum patefecit, et spirituales epulas praedicationis, et corporis et sanguinis sui abundantissime ministravit. Primum tempus ante legem, secundum sub lege, tertium sub gratia. <Grande convivium cunctis principibus.><Simile est regnum coelorum homini regi qui fecit nuptias filio suo Matth. 22..>Et alibi: <Homo quidam fecit coenam magnam: et vocavit multos Luc. 14..>Hujus convivii historia, pompam divitiarum, et luxum regis ostendit: sed Christi spirituales delicias quas unicuique dispensat, allegorice significat. Christus enim est ille ditissimus rex, qui uxoris suae, id est Ecclesiae, precibus exoratus, Judaeos, id est confessores suos, de manu inimicorum liberat, atque ipsos juste condemnat. Neque enim necesse est, ut si aliquorum bona Christum significant, eorum quoque mala ipsi, scilicet Christo conveniant. Moyses enim in multis Christum significavit; sed non in hoc, quod ad aquas contradictionis dubitavit. Sed nec Aaron factura vituli. Nec Salomon in sorde libidinis. Sic Assuerus in isto judicio, ut in liberali convivio Christum significat, sicut Isaias in Cyro rege Persarum Christum signat, de quo postea subjungit: <Accinxi te et non cognovisti me Isa. 45.,>etc. Si enim reges iniqui in malefactis diabolum significant; cur non reges justi in benefactis, Christum demonstrant? Nabuchodonosor jussit populos audita symphoniarum et musicorum voce prostratos statuam adorare. Et diabolus saeculari dulcedine genus humanum inflectit a mentis rectitudine ad sequendam avaritiam quae est simulacrorum servitus.
1:4 .
1:5 <Dies convivii.>Magnatum vel primitivorum fidelium primi refecti sunt, secundum illud: <Non sum missus nisi ad oves, quae perierunt domus Israel Matth. 15..>In circumcisionis sacramento Dominicae resurrectionis, et Novi Testamenti et verae circumcisionis: et octo sunt beatitudines, ad quas istud convivium perducit, quod fit tantum diebus; mali autem convivabantur in noctibus.
1:6 <Et pendebant.>Byssus mortificationem carnis significat. Purpura sanguinem martyrii. Columnae marmoreae, firmitatem doctorum. Bene ergo dicitur quod tentoria diversi coloris byssinis et purpureis funibus per circulos eburneos in columnis marmoreis suspendebantur; quia decor Ecclesiae in sapientiae meditatione et in virtutum ascensione per carnis mortificationem et castitatem cum martyrii dignitate in doctoribus debet effulgere; et ipsorum verbo et exemplo ad aliorum notitiam pervenire: ut ab his instructi et confortati, aulam paradisi mereantur introire. <Et columnis marmoreis fulciebantur.>Id est, doctoribus de quibus dicitur: <Ego confirmavi columnas ejus Psal. 74..>Ex alibi: <Columnas fecit argenteas, reclinatorium aureum Cant. 3..><Super pavimentum.>Smaragdus a nimia viriditate sui sic vocatur. Parium, genus marmoris candidissimum. Per virorem enim fidei et candorem castitatis construitur fundamentum humilitatis. <Beati enim pauperes spiritu, quoniam ipsorum est regnum coelorum. Matth. 5..>Et: <Qui se humiliat exaltabitur Luc. 10.. Et alibi: <Discite a me, quia mitis sum Matth. 11.,>etc.
1:7 <Vinum.>Secundum illud: <Charitas Dei diffusa est in cordibus nostris Rom. 5.,>etc. Alii datur sermo scientiae, alii sapientiae, alii fides, alii gratia sanitatum. <Nolentes cogeret ad bibendum,>etc. Nemo cogitur spirituale donum accipere: filios vult Deus, non servos: voluntatem vult, non necessitatem, secundum illud: <Qui potest capere capiat Matth. 19..>Item. <Si vis ad vitam ingredi, serva mandata Ibid..>Et: <Si vis perfectus esse, vade et vende omnia quae habes Ibid.,>etc.
1:8 Unusquisque quod vellet,>etc. Secundum velocitatem et capacitatem et utilitatem singulorum. Sic enim temperanda est praedicatio, ut in omnibus utilis fiat, nulli noceat, et inter omnium vitia, quasi gladius anceps transeat. Sic per superbiam rescindens auferat, ut non augeat timiditatem. Sic otiosis et torpentibus sollicitudinem operis imponens, ut inquietis et curiosis non augeat importunam actionem, et sic de caeteris.
1:9 <In palatio, ubi rex Assuerus,>etc. Hierusalem, ubi templum et sancta sanctorum, vel in sanctae Scripturae meditatione; in qua divinitas, potentiae suae tribuit notitiam.
1:10 .
1:11 <Ut ostenderet,>etc. <Ubi venit plenitudo temporis, misit Deus Filium Gal. 4.,>etc. Tunc enim Dei Filius incarnatus legis mysteria quibus ante fideles paverat, abundantiori gratia manifestavit.
1:12 Quae renuit.>Non solum legatores despiciens, sed et regis imperium; unde in Evangelio, senior filius a patre rogatus ad convivium noluit introire: et qui ad coenam vocati, aliis rebus occupati, noluerunt venire, juxta indignationem patrisfamilias, rejecti sunt et alii loco illorum constituti.
1:13 .
1:14 .
1:15 .
1:16 <Responditque Mamuchan audiente rege atque principibus: Non solum regem laesit regina,>etc. Hic novissimus in ordine septem sapientum numeratus, sententiam promulgat, et Paulum apostolum significat qui dicit: <Ego sum minimus apostolorum, qui non sum dignus vocari apostolus I Cor. 15..>Et contradicentibus Judaeis et zelo repletis ait: <Quia vos indignos judicatis vitae aeternae, ecce convertimur ad gentes Act. 13..>
1:17 .
1:18 .
1:19 .
1:20 .
1:21 .
1:22 .
2:1 .
2:2 .
2:3 .
2:4 .
2:5 .
2:6 .
2:7 <Qui fuit.>Esther dicitur absconsa, Edissa, misericordiam consecuta. Haec est gentium Ecclesia, quae in abscondito cordis nutriens castitatem fidei, misericordiam et gratiam coram oculis Domini invenit, repudiata synagoga, quae in Osee vocabatur absque misericordia. Hanc nutrit Mardochaeus spiritualis, et adoptat in filiam qui est doctor gentium in fide et veritate, et est de stirpe Jemini, hoc est de stirpe Benjamin.
2:8 <Esther.>Hanc Nabuchodonosor spiritualis et rex confusionis a naturali lege et cultu unius Dei in confusionem idololatriae transtulit, sed pietas divina ad viam veritatis per praedicatores revocavit.
2:9 <Et septem puellas.>Id est, fideles animas Spiritu sancti gratia regeneratas atque dedicatas quae ejus sequuntur vestigia, fide, doctrina, et operatione, de quibus dicitur: <Adolescentulae dilexerunt te nimis.><Quae noluit.>Quia peccatorum labe et sordibus idololatriae per baptismum mundata, opprobrium pristinae iniquitatis non sustinuit ultra.
2:10 .
2:11 .
2:12 .
2:13 <Ingredientesque.>Quaecunque anima certat ad thalamum Christi properare, condignum a suis doctoribus accipit ornamentum, et quo magis se devotam ad agnitionem fidei, et exercitium operis praeparat, eo amplius a doctoribus instruitur, ut de competentium numero, ad sanctae Trinitatis integram fidem et confessionem ac perceptionem baptismi et unctionem chrismatis, ut coelesti sponso veraciter jungatur, accedat.
2:14 <Atque inde.>Qui se ab Ecclesia per vitiorum sordes separaverit, merito in secundas aedes retruditur ubi concubinae regis morantur: quia sanae fidei casus descensum honoris meretur, nec habet ultra potestatem ad regem redire, nisi superna gratia visitatus, in pristinae statum dignitatis restituatur.
2:15 <Evoluto autem.>Id est, transactis quinque aetatibus mundi, quibus patrum propago processerat, instabat sexta aetas veniente Redemptore, in qua gentium multitudo per Evangelium vocanda erat.
2:16 <Tebeeth. Apud Hebraeos; Xudimos, apud Graecos; Januarius est apud Latinos. In quo Dominus incarnatus octavo die circumcisus a magis adoratus, et mysticis muneribus est honoratus, in quo etiam a Joanne baptizatus esse praedicatur.
2:17 .
2:18 .
2:19 <Cumque et secundo.>Non sufficit Christo primitivam sibi sociare Ecclesiam, sed per praedicatores de gentibus multitudinem acquisivit copiosam; unde: <Alias oves habeo quae non, etc. Joan. 10..>
2:20 .
2:21 <Duo eunuchi regis,>etc. Possunt in duobus eunuchis schismatici et haeretici notari, qui fraudis et malitiae venenum corde gestantes, contra veritatem consiliantur ut eam credentibus auferant, et Christum, id est fidem Christi, in ipsis fidelibus interficiant: sed eorum iniquitatem sancti doctores manifestant, ut innocentes salventur, et illi justa ultione puniantur.
2:22 .
2:23 <Historiis et annalibus traditum coram rege,>etc. Duobus Testamentis, in quibus scriptum est, quae justis praemia, quae injustis debeantur supplicia; unde: <Ibunt hi in supplicium aeternum: justi autem in vitam aeternam Matth. 25..>Et in Ezech.: <Justitiae justi super eum, et impietas impii erit super eum Ezech. 18..>
3:1 <Aman filium.>Potentes saeculi, qui beneficiis divina potestate collatis abutentes, quos consortes habent naturae dedignantur habere consortes gratiae: et honorem et reverentiam quam soli Deo debuerunt, in sese transferre contendunt. Eos autem qui consentire nolunt, odiis et cruciatibus persequuntur: sed justo Dei judicio in insidiis suis capiuntur iniqui. Unde dicitur: <Justus angustia liberatur, et traditur impius pro eo Prov. 11..>Potest per Aman quem Josephus de stirpe Amalec esse commemorat Judaeorum populus figurari: qui prophetas occiderunt, et Dominum prophetarum et apostolos, ejus generis nobilitatem propter incredulitatem et duritiam cordis perdentes, qui per Isaiam principes Sodomorum et populus Gomorrhae dicuntur. Unde Ezechiel: <Pater tuus Amorrhaeus, et mater tua Cethaea.>
3:2 .
3:3 .
3:4 .
3:5 .
3:6 .
3:7 <Missa est sors.>Qui ad sortem Domini pertinent, occiduntur, quos significat ille hircus, qui secundum sortem Domini in lege occidebatur.
3:8 .
3:9 .
3:10 <Tulit ergo.>Sicut Aman epistolas dirigens, regis signaculo eas munire conatur: ut facilius votum expleatur, sic Judaei libros divinae legis in quibus est signaculum summi regis, id est, gratia Spiritus sancti ad comprehendendam haeresim suam assumunt in testimonium, reprobantes societatem gentium et Christi Evangelium, quasi divinis praeceptis contrarium.
3:11 .
3:12 .
3:13 .
3:14 .
3:15 .
4:1 .
4:2 <Usque ad fores.>Sic magister Ecclesiae intercessione reginae, de qua dicitur: <Astitit regina a dextris tuis Psal. 14.,>etc., quae partim peregrinatur in mundo, et partim regnat cum Domino, a rege omnium saeculorum desiderat exaudiri. <Non enim.>Nullus cum corruptione praesentis vitae potest aulam coelestem intrare, sed ante obitum suum debet quisque corporis castigatione et cordis  compunctione regni pulsare introitum, ut post laetabundus supernum intret palatium. Si quis quaerat quomodo rex justissimus tormenta inferat innocentibus, sciat hoc non esse ex voto malitiae, sed ex summi consilii nutu procedere. <Sapientia enim vincit malitiam Sap. 7.,><attingit a fine usque ad finem fortiter: et disponit omnia suaviter Ibid., 8..><Omnia quaecunque voluit fecit in coelo et omnibus abyssis Psal. 113..>Cujus justo judicio agitur, ut fideles in manu persecutorum tradantur vel ad purgationem, vel ad correptionem, vel ad meritorum augmentationem: sic Job Satanae traditur; Paulus ab angelo Satanae colaphizatur. <Rursumque.>Orant pro se invicem Mardochaeus et Esther, id est magistri et discipuli, sicut Paulus pro discipulis orat, et pro se orari obsecrat, ut in Actibus apostolorum ab Ecclesia fiebat oratio sine intermissione pro Petro.
4:3 .
4:4 .
4:5 .
4:6 .
4:7 .
4:8 .
4:9 .
4:10 .
4:11 .
4:12 .
4:13 .
4:14 .
4:15 .
4:16 .
4:17 .
5:1 .
5:2 <Virgam auream.>Regiminis potentiam vel passionis crucem, per quam acquisivit potestatem, de qua dicitur: <Data est mihi omnis potestas in coelo et in terra Matth. 28..>Et alibi: <Propter quod exaltavit illum, et dedit illi nomen quod est super omne nomen Philip. 2..>
5:3 .
5:4 .
5:5 .
5:6 .
5:7 .
5:8 <Si inveni in conspectu.>Dilatio petendi non deputatur segnitiei, sed patientiae. Non enim in praesenti sed in futuro bonis et malis retribuetur merces: cras enim pro futuro ponitur, unde: <Nolite solliciti esse de crastino;>et Jacob in Genesi ait: <Apparebit mihi cras justitia mea Matth. 6..>Et de agno dicitur: <Non remanebit ex eo quidquam usque mane Gen. 30.>Cum judicium venerit, revelabitur quali veste quisquam ad convivium intraverit.
5:9 .
5:10 .
5:11 .
5:12 .
5:13 .
5:14 .
6:1 <Noctem illam duxit rex insomnem,>etc. In se mobilis permanens cursus temporum et actus omnium contemplatur, et nulla eum latet cogitatio cui omnia praesentia; unde Apostolus: <Non est in illo, est et fuit, sed est in illo semper est II Cor. 1..>
6:2 <Ventum est.>Gesta Mardochaei memorantur coram rege; quia bona opera sanctorum doctorum nunquam oblivioni tradentur, sed in memoria aeterna erit justus.
6:3 .
6:4 .
6:5 .
6:6 .
6:7 .
6:8 .
6:9 .
6:10 <Festina.>Magistri Ecclesiae omnium virtutum cultu et decore sapientiae illustrati honorantur diademate regiae dignitatis, tanquam membra summi regis. Ascendunt super equum regium, id est, super populum fidelium in quorum cordibus residet rex angelorum; unde Habacuc propheta: <Ascendens super equos tuos, et equitatus tuus sanitas.>His Aman specialis hostis populi Dei, licet etiam invitus praebet obsequium cum persecutores Ecclesiae coguntur eidem Ecclesiae reddere testimonium, non valentes occultare quod manifestum est. <Tulit itaque.>Haec mutatio dexterae Excelsi, qui si videbatur gloriosior, infra alios apparet vilior et inferior, secundum illud: <Deposuit potentes de sede, et exaltavit humiles.>Similiter Isaias ait: <Convertetur Libanus in Chermel, et Chermel in saltum reputabitur.>Sic Synagogae compressa est superbia, Ecclesiae humilitas exaltata. Sic persecutores fidei ad nihilum sunt redacti: confessores Christi in toto orbe exaltati. Caput in caudam, et cauda in caput versa est: <Quia omnis qui se exaltat, humiliabitur Luc. 24..>
6:11 .
6:12 .
6:13 .
6:14 .
7:1 <Secunda die,>etc. Sed ubi Esther petitionem suam aperuit, damnatus ad poenam secessit. In Evangelio enim prandium et coena memorantur. Prandium praesens tempus Ecclesiae designat, coena autem aeternum et ultimum convivium. Unde malis separatis in perpetuum laetantur boni. Aman, namque quia vestem dignam convivio non habuit, extra projici meruit.
7:2 .
7:3 .
7:4 .
7:5 .
7:6 .
7:7 <Intravit in hortum.>Rege in hortum properante, id est, secum electos ad delicias paradisi invitante, reginam pro salute sua deprecari contendit, sed tempus opportunum non habuit. Sero enim quaeruntur salutis remedia, cum mortis imminent pericula; unde Salomon: <Tunc invocabunt, et non exaudiam eos: mane consurgent, et non invenient Prov. 1..>
7:8 <Et statim.>Deprecatio Aman, oppressio dicitur, quia in die judicii iniquorum oratio irritatio est: unde: <Cum judicatur, exeat condemnatus>Psal. 108., etc. Tunc oppressio qua humiles opprimebant, ipsis improperabitur.
7:9 <Harbona.>Doctores legis significat, qui scelera et dolos Judaeorum praedixerunt, unde Moyses: <Novi quod post mortem meam inique agetis, et declinabitis de via quam ostendi vobis Deut. 31..>Et in Evangelio: <Est qui accusat vos, Moyses, in quo speratis Joan. 5..><In domo Aman.>Iniquos tumultuantes contra Christum, significat Aman et ejus Ecclesiam persequentes. Christus autem in lege erat eis promissus; lex itaque, quae ad custodiam vitae data est, ipsis in mortem versa est, per quam Christum delere, et ejus confessores interficere moliebantur: et qui per legem innoxios opprimere volebant; ipsa accusante, justi judicii accepere sententiam. Qui in lege peccaverunt, per legem vindicabuntur.
7:10 .
8:1 <Die illo.>Tenet Esther domum Aman, hostis Judaeorum, cum Ecclesia possidet mundum, quem possederat diabolus, hostis Christianorum; unde: <Cum fortis armatus custodit atrium suum Luc. 11.,>etc. Ingreditur ante faciem regis Mardochaeus, cum quotidie sanctorum animae de incolatu praesentis vitae ingrediuntur ad contemplationem divinae majestatis. <Domum Aman.>Dignitatem et honorem quem Judaei scientia legis, et prophetarum, et cultu religionis habuerunt; unde in Proverbiis: <Custoditur justo substantia peccantis Prov. 13..>Et in Evangelio: <Auferetur a vobis regnum, et dabitur genti facienti fructus ejus Matth. 21..>
8:2 <Tulitque rex.>Laudante et praedicante instantiam magistrorum, a Domino pro gratia gratiam accipiunt in conspectu conditoris omnium; unde: <Nutrivit corda fidelium.><Tradidit Mardochaeo.>Praedicatoribus gentium ut sibi haererent, et gentibus ministrarent.
8:3 <Procidit ad pedes,>etc. Quia sancta Ecclesia, pro ereptione filiorum suorum, quotidie Deum omnipotentem per fidem et mysteria incarnationis exorat, ut hostium comprimatur audacia, et fidelium liberetur innocentia.
8:4 .
8:5 <Obsecro ut.>Quia evangelica doctrina, quae Christi nomine in toto orbe praedicatur, Spiritus sancti signaculo confirmata declaratur, cujus dono repleti ipsi praedicatores permanserunt insuperabiles, et hostium suorum gloriosi triumphatores.
8:6 .
8:7 .
8:8 .
8:9 <Accitisque scribis,>etc. Quia Evangelii doctrina ita condita est per scriptores Novi Testamenti, Domino mediante, ut sanctae Trinitatis fides plenissime in ea contineretur, et totius decalogi summa perfectio in duobus praeceptis charitatis demonstraretur. <Qui centum viginti.>In quibus totus mundus signatur. Nam denarius numerus per duodenarium multiplicatus centum et viginti facit, quibus septenarius sociatus totius summae plenitudinem concludit: ita decalogi custodia apostolica traditione per totum orbem diffusa, septiformis Spiritus gratia in cordibus fidelium infunditur vel est consummata.
8:10 .
8:11 <Interficerent,>etc. Ne mali germinis pullularent rediviva plantaria. Similiter septem gentes quae habitabant in terra promissionis Dominus interfici jussit, et postmodum Amalechitas omnimode deleri, ut omnem occasionem scandali auferret eis; unde David ait: <In matutino interficiebam>Psal. 100., etc.
8:12 .
8:13 .
8:14 <In Susan,>etc. In superbia mundi debellanda. Nam Susis equitatio vel evertens interpretatur; unde Salvator ait: <Confidite, ego vici mundum Joan. 16..>
8:15 .
8:16 .
8:17 .
9:1 .
9:2 .
9:3 .
9:4 <Et plurimum posse cognoverant.>Quia doctorum actio laudabilis, et potentia virtutum, honorem et reverentiam tribuit multitudini fidelium. Unde in Actibus apostolorum: <Fiebant prodigia et signa multa Act. 5.,>unde legitur alibi: <Fiebat enim omni animae timor Ibid. 2.. Item: <Nemo audebat se conjungere illis, et magnificabat eos populus Ibid. 5..>
9:5 <Itaque percusserunt Judaei.>Non solum alios operarios iniquitatis, qui perseverant in peccatis, nec curant percipere remissionem peccatorum, Spiritus sancti divina sententia damnandos judicat, sed etiam carnales Judaeos decalogi transgressores, crucis Christi reatum, quem ex perfidia contraxerunt, in inferni civitatibus sensuros.
9:6 <Quorum ista sunt nomina.>Horum nominum interpretationes non multo opere exquirimus, nec allegoriae servire cogimus. Typus enim in parte est, non in toto. Si enim in toto est typus, non est historia. Historicum oportet rei gestae veram seriem exponere, nec citra subsistere, nec ultra procedere: allegoricum interpretem, egregias partes, in spirituali sensu convenientes eligere.
9:7 .
9:8 .
9:9 .
9:10 .
9:11 .
9:12 .
9:13 .
9:14 .
9:15 .
9:16 <In tantum.>Omnes, qui quinque sensibus corporeis legem carnaliter suscipiunt, et cum per septiformem Spiritus sancti gratiam specialiter plena et perfecta sit, spiritualiter intelligere respuunt, quasi duobus diebus vincuntur atque prosternuntur.
9:17 <Dies autem.>Notandum quod quidam Judaeorum tertia decima die interficiunt, et decima quarta desinunt, et solemnitatem agunt, quidam per duos dies, id est, tertiam decimam et decimam quartam diem ejusdem mensis occidunt, quinta decima solemniter quiescunt: quia quidam sanctorum, post completos labores quibus Deo bene credendo et bene vivendo serviunt, modo ante tempus judicii in animabus sabbatum aeternae quietis habent: alii, usque ad ultimum resurrectionis diem in carne perdurantes superatis hostibus universis subito immutati, quietem animarum et corporum percipiunt; unde Apostolus: <Mortui qui in Christo sunt, resurgent primi. Deinde nos qui vivimus, qui residui sumus, resurgemus cum illis obviam Christo in aera I Thes. 4.,>etc. Nam quarta decima, quae bis septem continet quietem animarum perfectam significat, quinta decima vero septenarium et octonarium complectens, animarum et corporum quietem significat.
9:18 .
9:19 .
9:20 .
9:21 .
9:22 .
9:23 .
9:24 <Et misit Phur,>etc. Sors in urnam missa, dispositionem cujuslibet rei in hominis mente significat, cujus eventum non humanum, sed divinum regit arbitrium, unde Salomon: <Sortes mittuntur in sinum: sed a Domino temperantur Prov. 16..>
9:25 .
9:26 .
9:27 .
9:28 <Isti sunt dies.>Quia quies animarum et resurrectio corporum merito firmiter a fidelibus custoditur et celebratur: vel, quia quietis animarum et resurrectionis corporum merito feriatio a fidelibus custoditur et celebratur.
9:29 .
9:30 .
9:31 .
9:32 .
10:1 <Rex vero Assuerus omnem.>Christus, qui excisus de monte sine manibus crevit in montem magnum et implevit totum mundum: historialiter, enim non omnem terram tributariam fecit, quia nec habuit. In antiquis Glossae ordinariae exemplaribus nulla, hinc usque ad finem libri, textui sacro expositio adjacet. Ideo capita 11-16 praetermittimus.
10:2 .
10:3 .
