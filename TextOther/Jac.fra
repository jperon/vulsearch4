1:1 Jacques, serviteur de Dieu et du Seigneur J�sus Christ, aux douze tribus qui sont dans la dispersion, salut!\
1:2 Mes fr�res, regardez comme un sujet de joie compl�te les diverses �preuves auxquelles vous pouvez �tre expos�s,
1:3 sachant que l'�preuve de votre foi produit la patience.
1:4 Mais il faut que la patience accomplisse parfaitement son oeuvre, afin que vous soyez parfaits et accomplis, sans faillir en rien.
1:5 Si quelqu'un d'entre vous manque de sagesse, qu'il l'a demande � Dieu, qui donne � tous simplement et sans reproche, et elle lui sera donn�e.
1:6 Mais qu'il l'a demande avec foi, sans douter; car celui qui doute est semblable au flot de la mer, agit� par le vent et pouss� de c�t� et d'autre.
1:7 Qu'un tel homme ne s'imagine pas qu'il recevra quelque chose du Seigneur:
1:8 c'est un homme irr�solu, inconstant dans toutes ses voies.
1:9 Que le fr�re de condition humble se glorifie de son �l�vation.
1:10 Que le riche, au contraire, se glorifie de son humiliation; car il passera comme la fleur de l'herbe.
1:11 Le soleil s'est lev� avec sa chaleur ardente, il a dess�ch� l'herbe, sa fleur est tomb�e, et la beaut� de son aspect a disparu: ainsi le riche se fl�trira dans ses entreprises.
1:12 Heureux l'homme qui supporte patiemment la tentation; car, apr�s avoir �t� �prouv�, il recevra la couronne de vie, que le Seigneur a promise � ceux qui l'aiment.
1:13 Que personne, lorsqu'il est tent�, ne dise: C'est Dieu qui me tente. Car Dieu ne peut �tre tent� par le mal, et il ne tente lui-m�me personne.
1:14 Mais chacun est tent� quand il est attir� et amorc� par sa propre convoitise.
1:15 Puis la convoitise, lorsqu'elle a con�u, enfante le p�ch�; et le p�ch�, �tant consomm�, produit la mort.
1:16 Nous vous y trompez pas, mes fr�res bien-aim�s:
1:17 toute gr�ce excellente et tout don parfait descendent d'en haut, du P�re des lumi�res, chez lequel il n'y a ni changement ni ombre de variation.
1:18 Il nous a engendr�s selon sa volont�, par la parole de v�rit�, afin que nous soyons en quelque sorte les pr�mices de ses cr�atures.\
1:19 Sachez-le, mes fr�res bien-aim�s. Ainsi, que tout homme soit prompt � �couter, lent � parler, lent � se mettre en col�re;
1:20 car la col�re de l'homme n'accomplit pas la justice de Dieu.
1:21 C'est pourquoi, rejetant toute souillure et tout exc�s de malice, recevez avec douceur la parole qui a �t� plant� en vous, et qui peut sauver vos �mes.
1:22 Mettez en pratique la parole, et ne vous bornez pas � l'�couter, en vous trompant vous-m�mes par de faux raisonnements.
1:23 Car, si quelqu'un �coute la parole et ne la met pas en pratique, il est semblable � un homme qui regarde dans un miroir son visage naturel,
1:24 et qui, apr�s s'�tre regard�, s'en va, et oublie aussit�t quel il �tait.
1:25 Mais celui qui aura plong� les regards dans la loi parfaite, la loi de la libert�, et qui aura pers�v�r�, n'�tant pas un auditeur oublieux, mais se mettant � l'oeuvre, celui-l� sera heureux dans son activit�.
1:26 Si quelqu'un croit �tre religieux, sans tenir sa langue en bride, mais en trompant son coeur, la religion de cet homme est vaine.
1:27 La religion pure et sans tache, devant Dieu notre P�re, consiste � visiter les orphelins et les veuves dans leurs afflictions, et � se pr�server des souillures du monde.
2:1 Mes fr�res, que votre foi en notre glorieux Seigneur J�sus Christ soit exempte de toute acception de personnes.
2:2 Supposez, en effet, qu'il entre dans votre assembl�e un homme avec un anneau d'or et un habit magnifique, et qu'il y entre aussi un pauvre mis�rablement v�tu;
2:3 si, tournant vos regards vers celui qui porte l'habit magnifique, vous lui dites: Toi, assieds-toi ici � cette place d'honneur! et si vous dites au pauvre: Toi, tiens-toi l� debout! ou bien: Assieds-toi au-dessous de mon marche-pied,
2:4 ne faites vous pas en vous-m�mes une distinction, et ne jugez-vous pas sous l'inspiration de pens�es mauvaises?
2:5 �coutez, mes fr�res bien-aim�s: Dieu n'a-t-il pas choisi les pauvres aux yeux du monde, pour qu'ils soient riches en la foi, et h�ritiers du royaume qu'il a promis � ceux qui l'aiment?
2:6 Et vous, vous avilissez le pauvre! Ne sont-ce pas les riches qui vous oppriment, et qui vous tra�nent devant les tribunaux?
2:7 Ne sont-ce pas eux qui outragent le beau nom que vous portez?
2:8 Si vous accomplissez la loi royale, selon l'�criture: Tu aimeras ton prochain comme toi-m�me, vous faites bien.
2:9 Mais si vous faites acception de personnes, vous commettez un p�ch�, vous �tes condamn�s par la loi comme des transgresseurs.
2:10 Car quiconque observe toute la loi, mais p�che contre un seul commandement, devient coupable de tous.
2:11 En effet, celui qui a dit: Tu ne commettras point d'adult�re, a dit aussi: Tu ne tueras point. Or, si tu ne commets point d'adult�re, mais que tu commettes un meurtre, tu deviens transgresseur de la loi.
2:12 Parlez et agissez comme devant �tre jug�s par une loi de libert�,
2:13 car le jugement est sans mis�ricorde pour qui n'a pas fait mis�ricorde. La mis�ricorde triomphe du jugement.\
2:14 Mes fr�re, que sert-il � quelqu'un de dire qu'il a la foi, s'il n'a pas les oeuvres? La foi peut-elle le sauver?
2:15 Si un fr�re ou une soeur sont nus et manquent de la nourriture de chaque jour,
2:16 et que l'un d'entre vous leur dise: Allez en paix, chauffez-vous et vous rassasiez! et que vous ne leur donniez pas ce qui est n�cessaire au corps, � quoi cela sert-il?
2:17 Il en est ainsi de la foi: si elle n'a pas les oeuvres, elle est morte en elle-m�me.
2:18 Mais quelqu'un dira: Toi, tu as la foi; et moi, j'ai les oeuvres. Montre-moi ta foi sans les oeuvres, et moi, je te montrerai la foi par mes oeuvres.
2:19 Tu crois qu'il y a un seul Dieu, tu fais bien; les d�mons le croient aussi, et ils tremblent.
2:20 Veux-tu savoir, � homme vain, que la foi sans les oeuvres est inutile?
2:21 Abraham, notre p�re, ne fut-il pas justifi� par les oeuvres, lorsqu'il offrit son fils Isaac sur l'autel?
2:22 Tu vois que la foi agissait avec ses oeuvres, et que par les oeuvres la foi fut rendue parfaite.
2:23 Ainsi s'accomplit ce que dit l'�criture: Abraham crut � Dieu, et cela lui fut imput� � justice; et il fut appel� ami de Dieu.
2:24 Vous voyez que l'homme est justifi� par les oeuvres, et non par la foi seulement.
2:25 Rahab la prostitu�e ne fut-elle pas �galement justifi�e par les oeuvres, lorsqu'elle re�ut les messagers et qu'elle les fit partir par un autre chemin?
2:26 Comme le corps sans �me est mort, de m�me la foi sans les oeuvres est morte.
3:1 Mes fr�res, qu'il n'y ait pas parmi vous un grand nombre de personnes qui se mettent � enseigner, car vous savez que nous serons jug�s plus s�v�rement.
3:2 Nous bronchons tous de plusieurs mani�res. Si quelqu'un ne bronche point en paroles, c'est un homme parfait, capable de tenir tout son corps en bride.
3:3 Si nous mettons le mors dans la bouche des chevaux pour qu'ils nous ob�issent, nous dirigeons aussi leur corps tout entier.
3:4 Voici, m�me les navires, qui sont si grands et que poussent des vents imp�tueux, sont dirig�s par un tr�s petit gouvernail, au gr� du pilote.
3:5 De m�me, la langue est un petit membre, et elle se vante de grandes choses. Voici, comme un petit feu peut embraser une grande for�t.
3:6 La langue aussi est un feu; c'est le monde de l'iniquit�. La langue est plac�e parmi nos membres, souillant tout le corps, et enflammant le cours de la vie, �tant elle-m�me enflamm�e par la g�henne.
3:7 Toutes les esp�ces de b�tes et d'oiseaux, de reptiles et d'animaux marins, sont dompt�s et ont �t� dompt�s par la nature humaine;
3:8 mais la langue, aucun homme ne peut la dompter; c'est un mal qu'on ne peut r�primer; elle est pleine d'un venin mortel.
3:9 Par elle nous b�nissons le Seigneur notre P�re, et par elle nous maudissons les hommes faits � l'image de Dieu.
3:10 De la m�me bouche sortent la b�n�diction et la mal�diction. Il ne faut pas, mes fr�res, qu'il en soit ainsi.
3:11 La source fait-elle jaillir par la m�me ouverture l'eau douce et l'eau am�re?
3:12 Un figuier, mes fr�res, peut-il produire des olives, ou une vigne des figues? De l'eau sal�e ne peut pas non plus produire de l'eau douce.\
3:13 Lequel d'entre vous est sage et intelligent? Qu'il montre ses oeuvres par une bonne conduite avec la douceur de la sagesse.
3:14 Mais si vous avez dans votre coeur un z�le amer et un esprit de dispute, ne vous glorifiez pas et ne mentez pas contre la v�rit�.
3:15 Cette sagesse n'est point celle qui vient d'en haut; mais elle est terrestre, charnelle, diabolique.
3:16 Car l� o� il y a un z�le amer et un esprit de dispute, il y a du d�sordre et toutes sortes de mauvaises actions.
3:17 La sagesse d'en haut est premi�rement pure, ensuite pacifique, mod�r�e, conciliante, pleine de mis�ricorde et de bons fruits, exempte de duplicit�, d'hypocrisie.
3:18 Le fruit de la justice est sem� dans la paix par ceux qui recherchent la paix.
4:1 D'o� viennent les luttes, et d'ou viennent les querelles parmi vous? N'est-ce pas de vos passions qui combattent dans vos membres?
4:2 Vous convoitez, et vous ne poss�dez pas; vous �tes meurtriers et envieux, et vous ne pouvez pas obtenir; vous avez des querelles et des luttes, et vous ne poss�dez pas, parce que vous ne demandez pas.
4:3 Vous demandez, et vous ne recevez pas, parce que vous demandez mal, dans le but de satisfaire vos passions.
4:4 Adult�res que vous �tes! ne savez-vous pas que l'amour du monde est inimiti� contre Dieu? Celui donc qui veut �tre ami du monde se rend ennemi de Dieu.
4:5 Croyez-vous que l'�criture parle en vain? C'est avec jalousie que Dieu ch�rit l'esprit qu'il a fait habiter en nous.
4:6 Il accorde, au contraire, une gr�ce plus excellente; c'est pourquoi l'�criture dit: Dieu r�siste aux l'orgueilleux, Mais il fait gr�ce aux humbles.
4:7 Soumettez-vous donc � Dieu; r�sistez au diable, et il fuira loin de vous.
4:8 Approchez-vous de Dieu, et il s'approchera de vous. Nettoyez vos mains, p�cheurs; purifiez vos coeurs, hommes irr�solus.
4:9 Sentez votre mis�re; soyez dans le deuil et dans les larmes; que votre rire se change en deuil, et votre joie en tristesse.
4:10 Humiliez-vous devant le Seigneur, et il vous �l�vera.
4:11 Ne parlez point mal les uns des autres, fr�res. Celui qui parle mal d'un fr�re, ou qui juge son fr�re, parle mal de la loi et juge la loi. Or, si tu juges la loi, tu n'es pas observateur de la loi, mais tu en es juge.
4:12 Un seul est l�gislateur et juge, c'est celui qui peut sauver et perdre; mais toi, qui es-tu, qui juges le prochain?\
4:13 A vous maintenant, qui dites: Aujourd'hui ou demain nous irons dans telle ville, nous y passerons une ann�e, nous trafiquerons, et nous gagnerons!
4:14 Vous qui ne savez pas ce qui arrivera demain! car, qu'est-ce votre vie? Vous �tes une vapeur qui para�t pour un peu de temps, et qui ensuite dispara�t.
4:15 Vous devriez dire, au contraire: Si Dieu le veut, nous vivrons, et nous ferons ceci ou cela.
4:16 Mais maintenant vous vous glorifiez dans vos pens�es orgueilleuses. C'est chose mauvaise que de se glorifier de la sorte.
4:17 Celui donc qui sait faire ce qui est bien, et qui ne le fait pas, commet un p�ch�.
5:1 A vous maintenant, riches! Pleurez et g�missez, � cause des malheurs qui viendront sur vous.
5:2 Vos richesses sont pourries, et vos v�tements sont rong�s par les teignes.
5:3 Votre or et votre argent sont rouill�s; et leur rouille s'�l�vera en t�moignage contre vous, et d�vorera vos chairs comme un feu. Vous avez amass� des tr�sors dans les derniers jours!
5:4 Voici, le salaire des ouvriers qui ont moissonn� vos champs, et dont vous les avez frustr�s, crie, et les cris des moissonneurs sont parvenus jusqu'aux oreilles du Seigneur des arm�es.
5:5 Vous avez v�cu sur la terre dans les volupt�s et dans les d�lices, vous avez rassasiez vos coeurs au jour du carnage.
5:6 Vous avez condamn�, vous avez tu� le juste, qui ne vous a pas r�sist�.\
5:7 Soyez donc patients, fr�res jusqu'� l'av�nement du Seigneur. Voici, le laboureur attend le pr�cieux fruit de la terre, prenant patience � son �gard, jusqu'� ce qu'il ait re�u les pluies de la premi�re et de l'arri�re-saison.
5:8 Vous aussi, soyez patients, affermissez vos coeurs, car l'av�nement du Seigneur est proche.
5:9 Ne vous plaignez pas les uns des autres, fr�res, afin que vous ne soyez pas jug�s: voici, le juge est � la porte.
5:10 Prenez, mes fr�res, pour mod�les de souffrance et de patience les proph�tes qui ont parl� au nom du Seigneur.
5:11 Voici, nous disons bienheureux ceux qui ont souffert patiemment. Vous avez entendu parler de la patience de Job, et vous avez vu la fin que le Seigneur lui accorda, car le Seigneur est plein de mis�ricorde et de compassion.\
5:12 Avant toutes choses, mes fr�res, ne jurez ni par le ciel, ni par la terre, ni par aucun autre serment. Mais que votre oui soit oui, et que votre non soit non, afin que vous ne tombiez pas sous le jugement.\
5:13 Quelqu'un parmi vous est-il dans la souffrance? Qu'il prie. Quelqu'un est-il dans la joie? Qu'il chante des cantiques.
5:14 Quelqu'un parmi vous est-il malade? Qu'il appelle les anciens de l'�glise, et que les anciens prient pour lui, en l'oignant d'huile au nom du Seigneur;
5:15 la pri�re de la foi sauvera le malade, et le Seigneur le rel�vera; et s'il a commis des p�ch�s, il lui sera pardonn�.
5:16 Confessez donc vos p�ch�s les uns aux autres, et priez les uns pour les autres, afin que vous soyez gu�ris. La pri�re fervente du juste a une grande efficace.
5:17 �lie �tait un homme de la m�me nature que nous: il pria avec instance pour qu'il ne pl�t point, et il ne tomba point de pluie sur la terre pendant trois ans et six mois.
5:18 Puis il pria de nouveau, et le ciel donna de la pluie, et la terre produisit son fruit.\
5:19 Mes fr�res, si quelqu'un parmi vous s'est �gar� loin de la v�rit�, et qu'un autre l'y ram�ne,
5:20 qu'il sache que celui qui ram�nera un p�cheur de la voie o� il s'�tait �gar� sauvera une �me de la mort et couvrira une multitude de p�ch�s.
